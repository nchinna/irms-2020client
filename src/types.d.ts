// import {
//     AccountType, PeriodDropDownRange, Gender, MaritalStatus, ParticipationDay, ParticipationTime, PeriodFilterRange, RoleType, Status,
//     TimeFilterRange,
//     TimePeriodFilter
//   } from './app/constants/constants';
  import {Observable} from 'rxjs/Observable';
  import { IOffset } from 'selenium-webdriver';
  
  interface EnumItem<E> { value: E; name: keyof E; }
  
  interface IUserAuth {
    email: string;
    password: string;
  }
  
  interface IUser {
    id: string;
    email: string;
    //role: RoleType;
    fullName: string;
    avatarId?: string;
  }
  
  interface IResponseModel {
    items: any[];
    totalCount: number;
  }
  
  interface IResponseSimpleEntity {
    id: string;
    value: string;
    name?: string;
    type?: string;
  }
  
  interface IActionButton {
    title: string;
    icon?: string;
    svgIcon?: string;
    handler: Function;
    visible?: Function;
    disabled?: boolean;
  }
  
  interface IActionButtons {
    leftSide?: Array<IActionButton>;
    rightSide?: Array<IActionButton>;
  }
  
  interface IList {
    data: Observable<IResponseModel>;
    childComponent: Object;
    selectedIds: string[];
    handleRowCheckChange(selectedId: string[]);
    changePagination(paginationFilter: IListPager);
  }
  
  interface IListPager {
    pageIndex: number;
    pageSize: number;
  }
  
  interface IPage {
    pageNo: number;
    pageSize: number;
    searchText?: string | null;
  }
  
  interface IGuestPage extends IPage {
    listId: string;
    eventId: string;
  }
  interface IRFIResponsesPage extends IPage {
    formId: string;
  }

  interface IEventList extends IPage  {
    // timeFilter: PeriodDropDownRange;
  }
  
  interface ISystemActivityList extends IPage {
    from: Date;
    to: Date;
    userId?: string;
    evenTypes?: Array<string>;
  }

  
  interface IAttendanceFilter extends IPage {
    volunteerId: string;
  }
  