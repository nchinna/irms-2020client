import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Token} from '../models/token';

@Injectable()
export class TokenService {

  private _token: BehaviorSubject<Token>;
  private _refreshToken: BehaviorSubject<string>;

  constructor() {
    this._token = <BehaviorSubject<Token>>new BehaviorSubject(new Token(sessionStorage.getItem('auth_token')));
    this._refreshToken = <BehaviorSubject<string>>new BehaviorSubject(sessionStorage.getItem('refresh_token'));
  }

  /**
   * Get the current token.
   */
  getToken(): Token {
    const token = this._token.getValue();
    return (token && token.token) ? token : null;
  }

  /**
   * Get the current token.
   */
  getRefreshToken(): string {
    return this._refreshToken.getValue();
  }
  getOtpStatus() {
    return sessionStorage.getItem('auth_otp');
  }
  /**
   * Returns an stream of tokens.
   */
  getTokenStream(): Observable<Token> {
    return this._token.asObservable();
  }

    /**
   * Update the current token.
   */
  setOTPStatus(val) {
    sessionStorage.setItem('auth_otp', val);
  }
  /**
   * Update the current token.
   */
  setToken(token: string) {
    this._token.next(new Token(token));
    sessionStorage.setItem('auth_token', token);
  }

 /**
   * Update the refresh token.
   */
  setRefreshToken(token: string) {
    this._refreshToken.next(token);
    sessionStorage.setItem('refresh_token', token);
  }

  /**
   * Remove the current token.
   */
  removeTokens() {
    this._refreshToken.next(null);
    sessionStorage.removeItem('refresh_token');
    this._token.next(null);
    sessionStorage.removeItem('auth_token');
    sessionStorage.removeItem('auth_otp');
  }

}
