import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { TokenService } from './token.service';
import { Token } from '../models/token';
import { AuthConfig } from '../auth.config';
import { AuthenticationService } from './authentication.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';
import { Toast } from 'angular5-toaster/dist/angular5-toaster';
import { BodyOutputType } from 'angular5-toaster/dist/src/bodyOutputType';
import { ErrorToastComponent } from './error-toast.component';
import { GenericLoaderService } from 'app/main/content/components/shared/generic-loader/generic-loader.service';

@Injectable()
export class JwtHttpInterceptor implements HttpInterceptor {
  constructor(private config: AuthConfig,
    private _tokenService: TokenService,
    private loaderService: GenericLoaderService,
    private injector: Injector,
    private toasterService: ToasterService) {
    this.config = new AuthConfig(config);
  }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: Token = this._tokenService.getToken();
    const isRepeated = (req as any).isRepeated;
    if (token && !token.isExpired()) {
      const isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
      let headers = req.headers.set(this.config.headerName, this.config.headerPrefix + ' ' + token.token);
      if (isIEOrEdge) {
        headers = req.headers.set(this.config.headerName, this.config.headerPrefix + ' ' + token.token).set('Cache-Control', 'no-cache')
          .set('Pragma', 'no-cache')
          .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT')
          .set('If-Modified-Since', '0');
      };
      req = req.clone({ headers: headers });
    }


    if (req.url.indexOf('token') < 0) {
      const returnSubject = new Subject<HttpEvent<any>>();
      next.handle(req).subscribe((event: HttpEvent<any>) => {
        returnSubject.next(event);
      },
        (err: any) => {
          console.log(err);
          if (!isRepeated) {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401 || err.status === 403) {
                // Circular reference if directly injected
                const auth = this.injector.get(AuthenticationService);
                auth.collectFailedRequest(req, returnSubject);
                auth.refreshToken().pipe(map(result => {
                  auth.retryFailedRequests();
                })).subscribe(a => {
                });
              }
              else {
                if (err.status === 400 || err.status === 500 || err.status === 0) {
                  this.loaderService.hide();

                  if (err.error && err.error.code !== 503) {
                    const toast: Toast =
                    {
                      type: 'error',
                      title: (err.error && err.error.message) ? err.error.message : 'Something went wrong',
                      body: ErrorToastComponent,
                      bodyOutputType: BodyOutputType.Component,
                      data: err.error
                    };
                    this.toasterService.pop(toast);
                    console.log(err);
                  }
                }
                returnSubject.error(err);
              }
            }
          }
        });
      return returnSubject;
    }
    else {
      return next.handle(req);
    }
  }
}
