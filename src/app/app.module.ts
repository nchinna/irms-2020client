import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { fuseConfig } from 'app/fuse-config';
import { AppComponent } from './app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { AuthModule } from './auth/auth.module';
import { ContentModule } from './layout/components/content/content.module';
import { appRoutingModule } from './app.routing';
import { SelectivePreloadingStrategy } from './auth/selective-preloading-strategy';
import { MainModule } from './main/main.module';
import { BASE_URL } from './constants/constants';
import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { GenericLoaderService } from './main/content/components/shared/generic-loader/generic-loader.service';
import { ToasterModule, ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { SharedModule } from './core/modules/shared.module';
import { FuseDirectivesModule } from '@fuse/directives/directives';
import { SmsDesignerPreviewComponent } from './main/content/sms-designer/sms-designer-preview/sms-designer-preview.component';
import { SmsDesignerModule } from './main/content/sms-designer/sms-designer.module';

@NgModule({
    declarations: [
        AppComponent,
        
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        appRoutingModule,        
        ToasterModule,
        ContentModule,
        TranslateModule.forRoot(),
        // ToasterModule.forRoot(),
        // Material moment date module
        MatMomentDateModule,
        // Material
        // MaterialModule,
        MatButtonModule,
        MatIconModule,
        // Fuse modules
        FuseDirectivesModule,
        MainModule.forRoot(),
        FuseModule.forRoot(fuseConfig),
        SharedModule.forRoot(),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        // App modules
        LayoutModule,
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyCA3PCbtkzsoeNqgaDMMrq-gftv3s3jA9w',
          libraries: ['places'],
          apiVersion: '3.25',
          language: 'en_US'
        }),
        AuthModule.forRoot({
            loginEndPoint: `${BASE_URL}connect/token`,
            guards: {loggedInGuard: {redirectUrl: '/account/login'}, loggedOutGuard: {redirectUrl: '/'}}
          })
    ],
    providers: [
      FuseNavigationService,
      SelectivePreloadingStrategy,
      ToasterService,
      GenericLoaderService
    ],    
    exports: [ ToasterModule ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
