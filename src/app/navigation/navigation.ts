import { FuseNavigation } from '@fuse/types';
import {Section} from '../constants/constants';

export const navigation: FuseNavigation[] = [
    {
        id       : 'admin_functions',
        title    : 'ADMIN FUNCTIONS',
        translate: 'NAV.ADMIN_FUNCTIONS',
        type     : 'group',
        children : [
            
            {
                id: 'dashboard',
                title: 'Dashboard',        
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: `/${Section.Dashboard}`
            },
            {
                id: 'event',
                title: 'Event',        
                translate: 'NAV.EVENTS.TITLE',
                type: 'item',
                icon: 'date_range',
                url: `/${Section.Events}`
            },
            {
                id: 'globalContact',
                title: 'Global Contacts',        
                translate: 'NAV.GLOBAL_CONTACTS.TITLE',
                type: 'item',
                icon: 'contacts',
                url: `/${Section.GlobalContact}`
            },
            {
                id: 'globalGuest',
                title: 'Global Guests',        
                translate: 'NAV.GLOBAL_GUESTS.TITLE',
                type: 'item',
                icon: 'people',
                url: `/${Section.GlobalGuest}`
            }
        ]
    }
];
