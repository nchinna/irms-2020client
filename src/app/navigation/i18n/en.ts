export const locale = {
    lang: 'en',
    data: {
        NAV: {
            ADMIN_FUNCTIONS: 'ADMIN FUNCTIONS',
            DASHBOARD        : {
                TITLE: 'Dashboard'
            },
            EVENTS : {
                TITLE: 'Events'
            },
            GLOBAL_CONTACTS : {
                TITLE: 'Global Contacts'
            },
            GLOBAL_GUESTS : {
                TITLE: 'Global Guests'
            },
        }
    }
};
