import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { GlobalGuestModel } from '../../global-guest.model';
import { IPage, IResponseModel } from 'app/core/types/types';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/constants/constants';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GlobalGuestDetailsEventHistoryDataService implements SectionDataService<GlobalGuestModel>{
    private url = `${BASE_URL}api/contact`;
    private dataUrl = `${BASE_URL}api/dataModule`;
    private contactDetailUrl = `${BASE_URL}api/contactDetail`;

    constructor(private http: HttpClient) { }

    getList(filter): Observable<any> {
        return this.http.post(`${this.dataUrl}/guest-event-admission`, filter);
    }
    get(id: any): Observable<GlobalGuestModel> {
        throw new Error("Method not implemented.");
    }
    create(model: any): Observable<string> {
        throw new Error("Method not implemented.");
    }
    update(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }
    delete(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }

  }