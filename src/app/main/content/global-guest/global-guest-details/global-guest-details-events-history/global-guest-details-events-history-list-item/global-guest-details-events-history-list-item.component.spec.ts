import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsEventsHistoryListItemComponent } from './global-guest-details-events-history-list-item.component';

describe('GlobalGuestDetailsEventsHistoryListItemComponent', () => {
  let component: GlobalGuestDetailsEventsHistoryListItemComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsEventsHistoryListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsEventsHistoryListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsEventsHistoryListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
