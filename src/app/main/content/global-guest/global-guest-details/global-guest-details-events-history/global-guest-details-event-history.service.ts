import { Injectable } from '@angular/core';
import { GlobalGuestModel } from '../../global-guest.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalGuestDataService } from '../../global-guest-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { GlobalGuestDetailsEventHistoryDataService } from './global-guest-details-event-history-data.service';

@Injectable({
    providedIn: 'root'
})
export class GlobalGuestDetailsEventHistoryService extends SectionService<GlobalGuestModel> {

    constructor(
        protected dataService:GlobalGuestDetailsEventHistoryDataService,
        queryService:QueryService,
        protected router: Router
    ) {
        super(dataService, queryService, router);
    }
}