import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsEventsHistoryDialogComponent } from './global-guest-details-events-history-dialog.component';

describe('GlobalGuestDetailsEventsHistoryDialogComponent', () => {
  let component: GlobalGuestDetailsEventsHistoryDialogComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsEventsHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsEventsHistoryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsEventsHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
