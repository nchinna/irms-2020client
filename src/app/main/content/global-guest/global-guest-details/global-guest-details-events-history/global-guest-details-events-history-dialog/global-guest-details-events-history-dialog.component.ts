import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { GlobalGuestDataService } from '../../../global-guest-data.service';
import { transparentSource } from 'app/constants/constants';

@Component({
  selector: 'irms-global-guest-details-events-history-dialog',
  templateUrl: './global-guest-details-events-history-dialog.component.html',
  styleUrls: ['./global-guest-details-events-history-dialog.component.scss']
})
export class GlobalGuestDetailsEventsHistoryDialogComponent implements OnInit {
  public eId;
  public gId;
  public model;
  transparentSource = transparentSource;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dataService:GlobalGuestDataService,
    private dialogRef: MatDialogRef<GlobalGuestDetailsEventsHistoryDialogComponent>) {
    this.eId = data.eId;
    this.gId = data.guest.id;
    this.model = data.guest;
   }

   doPathUrl(path): any {
    return `url(${path})`;
  }
  ngOnInit() {
    this.dataService.getGuestEventInfo({eventId:this.eId,
    guestId: this.gId}).subscribe(model => {
      this.model = model;
    })
  }
  close(){
    this.dialogRef.close();
  }

}
