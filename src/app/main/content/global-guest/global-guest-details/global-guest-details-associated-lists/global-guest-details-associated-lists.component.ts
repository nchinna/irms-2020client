import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GlobalGuestService } from '../../global-guest.service';
import { GlobalGuestDataService } from '../../global-guest-data.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { GlobalContactDataService } from 'app/main/content/global-contact/global-contact-data.service';
import { GlobalContactService } from 'app/main/content/global-contact/global-contact.service';

@Component({
  selector: 'irms-global-guest-details-associated-lists',
  templateUrl: './global-guest-details-associated-lists.component.html',
  styleUrls: ['./global-guest-details-associated-lists.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsAssociatedListsComponent implements OnInit {
  assocList = [];
  id = 1;
  constructor(public sectionService: GlobalContactService,
    protected dataService: GlobalContactDataService,
    protected router: Router,
    protected route: ActivatedRoute) {
     }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
            this.id = params['gid'];
            this.dataService.getContactAssociatedLists(this.id).subscribe(data => {
              this.assocList = data;
            });
          });
  }

}
