import { TestBed } from '@angular/core/testing';

import { GlobalGuestService } from './global-guest.service';

describe('GlobalGuestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalGuestService = TestBed.get(GlobalGuestService);
    expect(service).toBeTruthy();
  });
});
