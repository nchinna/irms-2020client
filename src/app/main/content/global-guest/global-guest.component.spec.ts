import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestComponent } from './global-guest.component';

describe('GlobalGuestComponent', () => {
  let component: GlobalGuestComponent;
  let fixture: ComponentFixture<GlobalGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
