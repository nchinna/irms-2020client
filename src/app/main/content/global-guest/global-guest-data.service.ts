import { Injectable } from '@angular/core';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { IPage, IResponseModel } from 'types';
import { Observable, of } from 'rxjs';
import { GlobalGuestModel } from './global-guest.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalGuestDataService implements SectionDataService<GlobalGuestModel>{

  private url = `${BASE_URL}api/contact`;
  private dataUrl = `${BASE_URL}api/dataModule`;
  private contactDetailUrl = `${BASE_URL}api/contactDetail`;

  constructor(private http: HttpClient) { }
  getGuestEventInfo(params): Observable<any>{
    return of({
      eventName: 'Spark',
      status: 'Accepted',
      seat: 'S4 R2',
      parking: 'P4 16',
      badgeLink: 'https://picsum.photos/200/300'
    });
  }
  getEventsHistoryByGuestId(id: number): Observable<any> {
    return this.http.get(`${this.dataUrl}/guest-event-admission/${id}`);
  }
  getGuestOrganizationInfo(id): Observable<any> {
    return of({
      id: '720a040a-cbca-423f-b97e-f37b6f24b370',
      organization: 'Escenta',
      position: 'Programmer'
    });
  }
  checkEmailUniqueness(email): Observable<any> {
    return this.http.post(`${this.url}/CheckEmailUniqueness`, email);
  }

  checkPhoneUniqueness(phone): Observable<any> {
    return this.http.post(`${this.url}/CheckPhoneUniqueness`, phone);
  }
  getGuestBasicInfo(id, listId): Observable<any> {
    return this.http.post<any>(`${this.contactDetailUrl}/personal-details`, {
      guestListId: listId,
      contactId: id
    });
  }
  getGlobalGuestListByListId(data: any): Observable<any>{
    return this.http.post<any>(`${this.dataUrl}/global-guest-list`, data);
  }
  
  getListName(data): Observable<any> {
    return this.http.post(`${this.url}/list-name`, data, { responseType: 'text' });
  }
  
  getGlobalGuestsCount(): Observable<any> {
    return this.http.get<any>(`${this.dataUrl}/global-guests-count/`);
  }

  getGlobalGuestsList(filterParam: any): Observable<any> {
    return this.http.post<any>(`${this.dataUrl}/load-global-guests`, filterParam);
  }
  getList(filterParam: IPage): Observable<IResponseModel> {
    return this.http.post<any>(`${this.dataUrl}/load-global-guests`, filterParam);
  }

  get(id): Observable<GlobalGuestModel> {
    return of({
      id: '720a040a-cbca-423f-b97e-f37b6f24b370',
      title: 'yellow',
      fullName: 'Janna Daugherty',
      preferredName: 'Deirdre Cain',
      gender: 'female',
      documentNumber: 1234567890,
      expirationDate: '2020-06-16T09:24:21.513Z',
      email: 'deirdrecain@escenta.com',
      alternativeEmail: 'deirdrecain@escenta.com',
      mobileNumber: '+966547687678',
      secondaryMobileNumber: '',
      workNumber: '',
      nationalityId: 'e40f40d3-4969-164e-1d00-3e574a8a5738',
      documentTypeId: 1,
      issuingCountryId: 'e40f40d3-4969-164e-1d00-3e574a8a5738'
    });
  }

  getGuestAssociatedLists(param): Observable<any> {
    return of({items: [
      {
        id: '0d167019-f510-41f0-beb7-96c37b7a32q2',
        listName: 'List 1'
      },
      {
        id: '0d167019-f510-41f0-beb7-96c37b7a32d2',
        listName: 'List 2'
      }
    ]});
  }

  getGuestCustomFields(filterParam: any): Observable<any> {
    return of({
      fields: [
        {
          id: '145778455426',
          title: 'Food Prefference',
          value: 'Vegan'
        },
        {
          id: '145778455416',
          title: 'Visa Required',
          value: 'Yes'
        }
      ]
    });
  }
  create(model: any): Observable<string> {
    throw new Error('Method not implemented.');
  }
  update(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
  delete(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
}
