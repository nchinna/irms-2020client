import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {debounceTime} from 'rxjs/operators';
import {startWith} from 'rxjs/operators/startWith';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'irms-autocomplete-server-side',
  templateUrl: './autocomplete-server-side.component.html',
  styleUrls: ['./autocomplete-server-side.component.scss']
})
export class AutocompleteServerSideComponent implements OnInit {
  @Input() public data: Observable<any[]>;
  private inputSubsc: Subscription;
  @Input() public inputControl: FormControl;
  @Input() public placeholder: string;
  @Input() public empty: boolean;
  @Output() public onChangeObject: EventEmitter<Event> = new EventEmitter();
  @Output() public onChangeString: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.createAuto();
  }

  createAuto() {
    this.inputSubsc = this.inputControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300)
      )
      .subscribe((value: any) => {
        if (typeof value === 'string') {
          this.onChangeString.emit(value);
        }
        if (typeof value === 'object' && value != null) {
          this.onChangeObject.emit(value);
        }
      }
    );
  }

  public displayName(val): string {
    if (val) {
      if (typeof val === 'string') {
        return val;
      }
      if (val.hasOwnProperty('value')) {
        return val.value;
      }
      if (val.hasOwnProperty('name')) {
        return val.name;
      }
    }
    return undefined;
  }
}
