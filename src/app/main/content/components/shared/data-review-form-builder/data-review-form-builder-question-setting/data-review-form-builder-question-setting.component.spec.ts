import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderQuestionSettingComponent } from './data-review-form-builder-question-setting.component';

describe('DataReviewFormBuilderQuestionSettingComponent', () => {
  let component: DataReviewFormBuilderQuestionSettingComponent;
  let fixture: ComponentFixture<DataReviewFormBuilderQuestionSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormBuilderQuestionSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormBuilderQuestionSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
