import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderSettingsPaneComponent } from './data-review-form-builder-settings-pane.component';

describe('DataReviewFormBuilderSettingsPaneComponent', () => {
  let component: DataReviewFormBuilderSettingsPaneComponent;
  let fixture: ComponentFixture<DataReviewFormBuilderSettingsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormBuilderSettingsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormBuilderSettingsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
