import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataReviewFormBuilderComponent } from './data-review-form-builder.component';
import { MainModule } from 'app/main/main.module';
import { DataReviewFormBuilderContentPaneComponent } from './data-review-form-builder-content-pane/data-review-form-builder-content-pane.component';
import { DataReviewFormBuilderQuestionSettingComponent } from './data-review-form-builder-question-setting/data-review-form-builder-question-setting.component';
import { DataReviewFormBuilderSettingsPaneComponent } from './data-review-form-builder-settings-pane/data-review-form-builder-settings-pane.component';
import { DataReviewFormBuilderThemePaneComponent } from './data-review-form-builder-theme-pane/data-review-form-builder-theme-pane.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FormModule } from '../form/form.module';
import { GenericColorPickerModule } from '../generic-color-picker/generic-color-picker.module';



@NgModule({
  declarations: [
    DataReviewFormBuilderComponent,
    DataReviewFormBuilderContentPaneComponent,
    DataReviewFormBuilderQuestionSettingComponent,
    DataReviewFormBuilderSettingsPaneComponent,
    DataReviewFormBuilderThemePaneComponent],
  imports: [
    CommonModule,
    MainModule,
    EditorModule,
    FormModule,
    GenericColorPickerModule,
  ],
  exports: [DataReviewFormBuilderComponent,
  DataReviewFormBuilderContentPaneComponent,
  DataReviewFormBuilderSettingsPaneComponent,
  DataReviewFormBuilderThemePaneComponent,
  DataReviewFormBuilderQuestionSettingComponent
  ]
})
export class DataReviewFormBuilderModule { }
