import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderComponent } from './data-review-form-builder.component';

describe('DataReviewFormBuilderComponent', () => {
  let component: DataReviewFormBuilderComponent;
  let fixture: ComponentFixture<DataReviewFormBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
