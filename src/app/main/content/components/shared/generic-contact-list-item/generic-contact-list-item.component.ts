import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-generic-contact-list-item',
  templateUrl: './generic-contact-list-item.component.html',
  styleUrls: ['./generic-contact-list-item.component.scss']
})
export class GenericContactListItemComponent implements OnInit {
  
  
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
