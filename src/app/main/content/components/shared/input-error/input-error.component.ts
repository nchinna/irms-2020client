import {Component, EventEmitter, Input, Output} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'irms-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.scss']
})

export class InputErrorComponent {
  @Input() public inputControl: FormControl;
  @Input() public placeholder: string;
  @Input() public required: boolean;
  @Input() public flexWidth: string;
  @Input() public charactersCount: number;
  @Input() public errorPatternText: string;
  @Input() public errorRangeText: string;
  @Input() public fieldLabel: string;
  @Input() public disabled = false;
  @Input() public readonly = false;
  @Input() public showCount = true;
  @Input() public id = '';
  @Input() public sideLabel = false;
  @Input() public appearance = 'outline';  // 'legacy' | 'standard' | 'fill' | 'outline'

  Validators = Validators;
  @Output() onBlur: EventEmitter<Event> = new EventEmitter();


  handleBlur(): void {
    this.onBlur.emit(event);
  }
  public isRequired(): boolean { 
    const validator: any = this.inputControl.validator && this.inputControl.validator(this.inputControl);
    return validator && validator.required;
  }
  
}
