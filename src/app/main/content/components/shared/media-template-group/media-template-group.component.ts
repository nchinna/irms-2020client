import { Component, OnInit, Input, Output, EventEmitter, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { invitationWorkflowStep, MediaType, sideDialogConfig } from 'app/constants/constants';
import { fuseAnimations } from 'app/core/animations';
import { EmailDesignerDataService } from 'app/main/content/email-designer/email-designer-data.service';
import { EmailDesignerService } from 'app/main/content/email-designer/email-designer.service';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { SmsDesignerDataService } from 'app/main/content/sms-designer/sms-designer-data.service';
import { WhatsappDesignerDataService } from 'app/main/content/whatsapp-designer/whatsapp-designer-data.service';
import { EmailLandingDesignComponent } from './email-preview/email-landing-design/email-landing-design.component';
import { EmailPreviewComponent } from './email-preview/email-preview.component';
import { SmsLandingDesignComponent } from './sms-preview/sms-landing-design/sms-landing-design.component';
import { SmsPreviewComponent } from './sms-preview/sms-preview.component';
import { WhatsappBotConfigModalComponent } from './whatsapp-bot-config-modal/whatsapp-bot-config-modal.component';
import { WhatsappOptionConfigModalComponent } from './whatsapp-option-config-modal/whatsapp-option-config-modal.component';
import { WhatsappPreviewModalComponent } from './whatsapp-preview-modal/whatsapp-preview-modal.component';

@Component({
  selector: 'irms-media-template-group',
  templateUrl: './media-template-group.component.html',
  styleUrls: ['./media-template-group.component.scss'],
  animations: fuseAnimations
})
export class MediaTemplateGroupComponent implements OnInit, OnDestroy {
  mediaType = MediaType;
  @Input() showRsvp;
  @Input() disabled;
  @Input() emailTemplate;
  @Input() smsTemplate;
  @Input() whatsappTemplate;
  @Input() onlyEmitOnCreate;
  @Input() isReview = false;
  @Input() showWhatsApp = true;
  @Input() isLanding = false;
  @Input() activeMedia: any;
  @Input() step;
  @Output() create: EventEmitter<any> = new EventEmitter();

  sms: any = {};
  email: any = {};
  id: any;
  whatsapp: any = {};
  
  /// Edit Mode Flags
  editEmail = false;
  editSms = false;
  editWhatsapp = false;
  @ViewChild('wrapper', {static: false}) template: TemplateRef<any>;
  isDialogOpen: boolean;
  emailDialogRef: MatDialogRef<any>;
  invitationStep = invitationWorkflowStep;

  constructor(public animDialog: DialogAnimationService,
              public smsDataService: SmsDesignerDataService,
              public emailDataService: EmailDesignerDataService,
              public emailDesignerService: EmailDesignerService,
              public whatsAppDataService: WhatsappDesignerDataService,
              public route: ActivatedRoute,
              public router: Router,
              private dialog: MatDialog) {

  }

  ngOnInit(): void {
    
  }
  ngOnDestroy(): void {
    this.handleExpand('none');
  }

 

  handleExpand(media): void {
    if (media === 'email') {
      this.editEmail = true;
      this.editSms = false;
      this.editWhatsapp = false;

    } else if (media === 'sms') {

      this.editEmail = false;
      this.editSms = true;
      this.editWhatsapp = false;

    } else if (media === 'whatsapp') {
      this.editEmail = false;
      this.editSms = false;
      this.editWhatsapp = true;

    } else {
      this.editEmail = false;
      this.editSms = false;
      this.editWhatsapp = false;

    }
    // this.create.emit(media);
  }

  smsPreview(): void {
    this.smsDataService.get(this.smsTemplate.id).subscribe(result => {
      if (result) {
        this.sms.id = result.id;
        this.sms.sender = result.senderName;
        this.sms.message = result.body;
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          id: this.sms.id,
          messages: this.sms.message,
          senderName: this.sms.sender
        };
        const dialogRef = this.animDialog.open(SmsPreviewComponent, dialogConfig);
      }
    });
  }

  whatsAppPreview(): void {

    this.whatsAppDataService.get(this.whatsappTemplate.id).subscribe(result => {

      if (result) {
        this.whatsapp.id = result.id;
        this.whatsapp.sender = result.senderName;
        this.whatsapp.message = result.body;
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          id: this.whatsapp.id,
          messages: this.whatsapp.message,
          senderName: this.whatsapp.sender
        };

        const dialogRef = this.animDialog.open(WhatsappPreviewModalComponent, dialogConfig);
      }
    });

  }

  emailPreview(): void {
    this.emailDataService.get(this.emailTemplate.id).subscribe(x => {
      this.email.id = x.id;
      if (x) {
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          id: this.emailTemplate.id,
          tempId: this.email.id
        };

        const dialogRef = this.animDialog.open(EmailPreviewComponent, dialogConfig);
      }
    });

  }
  whatsappOptionConfig(): void {
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.minWidth = '60vw';
    dialogConfig.data = {
      id: this.emailTemplate.id,
      tempId: this.whatsappTemplate.id,
    };
    if (this.step === this.invitationStep.Invitation || this.step === this.invitationStep.Pending) {
      const dialogRef = this.animDialog.open(WhatsappOptionConfigModalComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'bot') {
          setTimeout(() => {
            this.whatsappBotConfig();
          }, 500);
        } else if (result === 'template') {
          this.router.navigate([`/whatsapp-designer/${this.whatsappTemplate.id}/design`]);
        }
      });
    } else {
      this.router.navigate([`/whatsapp-designer/${this.whatsappTemplate.id}/design`]);
    }
  }

whatsappBotConfig(): void {
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.minWidth = '60vw';
    dialogConfig.data = {
      id: this.emailTemplate.id,
      tempId: this.email.id,
      step: this.step,
    };

    const dialogRef = this.animDialog.open(WhatsappBotConfigModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

landingEmail(): void {
    this.emailDataService.get(this.emailTemplate.id).subscribe(x => {
      this.email.id = x.id;
      if (x) {
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          id: this.emailTemplate.id,
          tempId: this.email.id,
          formAccept: x.acceptHtml,
          formReject: x.rejectHtml,
          formTheme: JSON.parse(x.themeJson),
          step: this.step,
          backgroundImagePath: x.backgroundImagePath
        };

        const dialogRef = this.animDialog.open(EmailLandingDesignComponent, dialogConfig);
      }
    });
  }

landingSms(): void {
    this.smsDataService.get(this.smsTemplate.id).subscribe(x => {
      this.sms.id = x.id;
      let themeJson =  {
        answer: 'rgb(1, 32, 66)',
        background: '#fff',
        backgroundStyle: 'cover',
        buttons: 'rgb(1, 32, 66)',
        buttonsText: '#fff',
        font: 'ithra',
        question: 'rgb(1, 32, 66)'
      }
      if (x) {
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          acceptButtonText: x.acceptButtonText,
          backgroundImagePath: x.backgroundImagePath,
          body: x.body,
          senderName: x.senderName,
          proceedButtonText: x.proceedButtonText,
          rejectButtonText: x.rejectButtonText,
          id: x.id,
          campaignInvitationId: x.campaignInvitationId,
          rsvpHtml: x.rsvpHtml,
          acceptHtml: x.acceptHtml,
          rejectHtml: x.rejectHtml,
          welcomeHtml: x.welcomeHtml,
          themeJson: x.themeJson == '[object Object]' ? themeJson : JSON.parse(x.themeJson),
          type: 'SMS',
          step: this.step,
          copyTemplate: x.copyTemplate
        };

        const dialogRef = this.animDialog.open(SmsLandingDesignComponent, dialogConfig);
      }
    });
  }

landingWhatsApp(): void {
    this.whatsAppDataService.get(this.whatsappTemplate.id).subscribe(x => {
      this.whatsapp.id = x.id;
      if (x) {
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
          acceptButtonText: x.acceptButtonText,
          backgroundImagePath: x.backgroundImagePath,
          body: x.body,
          senderName: x.senderName,
          proceedButtonText: x.proceedButtonText,
          rejectButtonText: x.rejectButtonText,
          id: x.id,
          campaignInvitationId: x.campaignInvitationId,
          rsvpHtml: x.rsvphtml,
          acceptHtml: x.acceptHtml,
          rejectHtml: x.rejectHtml,
          welcomeHtml: x.welcomeHtml,
          themeJson: JSON.parse(x.themeJson),
          type: 'WhatsApp',
          step: this.step,
          copyTemplate: x.copyTemplate
        };

        const dialogRef = this.animDialog.open(SmsLandingDesignComponent, dialogConfig);
      }
    });
  }

emailSave(): void {
    this.emailDesignerService.triggerSave();
    }

}
