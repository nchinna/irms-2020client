import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappTemplateCardComponent } from './whatsapp-template-card.component';

describe('WhatsappTemplateCardComponent', () => {
  let component: WhatsappTemplateCardComponent;
  let fixture: ComponentFixture<WhatsappTemplateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappTemplateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappTemplateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
