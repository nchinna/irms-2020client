import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { invitationWorkflowStep, MediaType } from 'app/constants/constants';
import { fuseAnimations } from 'app/core/animations';
import { SmsDesignerService } from 'app/main/content/sms-designer/sms-designer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'irms-sms-template-card',
  templateUrl: './sms-template-card.component.html',
  styleUrls: ['./sms-template-card.component.scss'],
  animations: fuseAnimations
})
export class SmsTemplateCardComponent implements OnInit , OnDestroy {
  @Input() editSms = false;
  @Input() smsTemplate;
  @Input() showRsvp;
  @Input() disabled;
  @Input() isDialog;
  @Input() step;
  @Input() isReview = false;
  @Output() editSmsEvent: EventEmitter<any> = new EventEmitter();
  @Output() smsPreview: EventEmitter<any> = new EventEmitter();
  @Output() landingSms: EventEmitter<any> = new EventEmitter();
  invitationWorkflowStep = invitationWorkflowStep;
  
  subscriptions: Subscription[] = [];
  mediaType = MediaType;
  private dialogRef = null;
  private dialogData;
  constructor(
    private dialog: MatDialog,
    private injector: Injector,
    private smsDesignerService: SmsDesignerService) {
      this.dialogRef = this.injector.get(MatDialogRef, null);
      this.dialogData = this.injector.get(MAT_DIALOG_DATA, null);
  }
  ngOnInit(): void {
    this.subscriptions.push(this.smsDesignerService.triggerPreviewObs().subscribe(data => {
      this.smsPreview.emit();
      this.closeDialogView();
    }));
    this.subscriptions.push(this.smsDesignerService.triggerLandingPageObs().subscribe(data => {
      this.landingSms.emit();
      this.closeDialogView();
    }));
    
    if (this.dialogData) {
      this.isDialog = this.dialogData.isDialog;
    }
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  expandForEdit(): void {
    this.editSmsEvent.emit();
  }

  openDialogView(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.minWidth = '70vw';
    dialogConfig.data = {
      isDialog: true
    };
    const dialogRef = this.dialog.open(SmsTemplateCardComponent, dialogConfig);
    dialogRef.componentInstance.editSms = this.editSms;
    dialogRef.componentInstance.smsTemplate = this.smsTemplate;
    dialogRef.componentInstance.showRsvp = this.showRsvp;
    dialogRef.componentInstance.disabled = this.disabled;
    dialogRef.componentInstance.step = this.step;
    dialogRef.componentInstance.isDialog = true;
    dialogRef.afterOpened().subscribe(() => {
      this.smsDesignerService.setCurrentComponentState('dialog');
    });

    dialogRef.afterClosed().subscribe( ex => {
      this.smsDesignerService.setCurrentComponentState('component');
      this.smsDesignerService.triggerRefresh();
    });

  }

  closeDialogView(): void {
    if (this.dialogRef) {
      this.dialogRef.close(false);
    }
  }

  smsSave(): void {
    this.smsDesignerService.triggerSave();
  }
  smsPreviewMethod(): void {
    this.smsDesignerService.triggerPreview();
  }

}
