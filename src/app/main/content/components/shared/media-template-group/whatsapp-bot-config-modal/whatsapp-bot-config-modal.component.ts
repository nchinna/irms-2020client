import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MediaTemplateDataService } from '../../media-template-design/media-template-data.service';

@Component({
  selector: 'irms-whatsapp-bot-config-modal',
  templateUrl: './whatsapp-bot-config-modal.component.html',
  styleUrls: ['./whatsapp-bot-config-modal.component.scss']
})
export class WhatsappBotConfigModalComponent implements OnInit {

  public tabGroup = [
    {
      label: 'Accepted Response',
      link: './config'
    },
    {
      label: 'Rejected Response',
      link: './invitation'
    },
    {
      label: 'Default Response',
      link: './invitation-pending'
    }
  ];
  selectedTab = 0;

  public configForm = this.fb.group({
    invitation: [''],
    acceptCode: ['AC1'],
    rejectCode: ['RJ1'],
    acceptedResponse: ['Thank you for accepting the invitation.\nWe are looking forward to seeing you at {{eventname}}.'],
    rejectedResponse: ['We are sorry that you won\'t be able to attend the {{eventname}}.\nWe hope to see you in future events.'],
    defaultResponse: ['']
  });

  previewData = {
    senderName: 'see-u',
    messages: [
      {
        type: 'received',
        content: this.configForm.controls.invitation.value
      },
      {
        type: 'sent',
        content: `${this.configForm.controls.acceptCode.value}`
      },
      {
        type: 'received',
        content: this.configForm.controls.acceptedResponse.value
      }
    ]
  };
  isSaving: boolean;
  constructor(protected fb: FormBuilder, public dialogRef: MatDialogRef<WhatsappBotConfigModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: MediaTemplateDataService) { }

  ngOnInit(): void {
    
    // Get template Call
    this.dataService.getWhatsAppBotTemplate(this.data.id).subscribe(data => {
      if (data) {
        this.configForm.controls.invitation.setValue(data.approvedTemplate);
        if (data.defaultResponse) {
          this.configForm.patchValue(data);
         
        } else if (data.acceptedResponse != null) {
          this.configForm.patchValue(data);
          if (data.acceptedResponse === ''){
          this.configForm.controls.acceptedResponse.setValue(data.acceptedResponse === '' ? 'Thank you for accepting the invitation.\nWe are looking forward to seeing you at {{eventname}}.' : data.acceptedResponse);
          this.configForm.controls.rejectedResponse.setValue(data.rejectedResponse === '' ? 'We are sorry that you won\'t be able to attend the {{eventname}}.\nWe hope to see you in future events.' : data.rejectedResponse);
          }
        } else {
          this.configForm.controls.acceptCode.setValue(data.acceptCode);
          this.configForm.controls.rejectCode.setValue(data.rejectCode);
        }
        if (this.data.step === 'Pending'){
          this.configForm.controls.invitation.setValue(`Dear {{guestname}},\n\nWe would like to remind you that {{tenantname}} is waiting for you to confirm your attendance to {{eventname}} that will be held on {{eventdatetime}} at {{event location}}. To respond to the invitation, please type the corresponding code:\n\nTo Accept type: ${this.configForm.controls.acceptCode.value}\n\nTo Reject type: ${this.configForm.controls.rejectCode.value}`);
          this.previewData.messages[0].content = this.configForm.controls.invitation.value;
    
        }

        this.updatePreview();
      }
    });
    this.configForm.get('acceptedResponse').valueChanges.subscribe(x => {
      this.previewData.messages[2].content = this.configForm.controls.acceptedResponse.value;
    });

    this.configForm.get('rejectedResponse').valueChanges.subscribe(x => {
      this.previewData.messages[2].content = this.configForm.controls.rejectedResponse.value;
    });
  }

  tabChange(index): void {
    this.selectedTab = index;
    this.updatePreview();
  }

  next(): void {
    if (this.selectedTab < this.tabGroup.length - 1) {
      this.selectedTab++;
      this.updatePreview();
    } else {
      this.isSaving = true;
      const formData = {
        campaignInvitationId: this.data.id,
        acceptedResponse: this.configForm.controls.acceptedResponse.value,
        rejectedResponse: this.configForm.controls.rejectedResponse.value,
        defaultResponse: this.configForm.controls.defaultResponse.value,
        body: this.configForm.controls.invitation.value
      };
      // update template call with formData 
      this.dataService.updateWhatsAppBotTemplate(formData).subscribe(data => {
        this.closeDialog(null);
        this.isSaving = false;
      }, error => this.isSaving = false);
    }
  }

  updatePreview(): void {
    if (this.selectedTab === 0) {
      this.previewData.messages[0].content = this.configForm.controls.invitation.value;
      this.previewData.messages[1].content = this.configForm.controls.acceptCode.value;
      this.previewData.messages[2].content = this.configForm.controls.acceptedResponse.value;
    } else if (this.selectedTab === 1) {
      this.previewData.messages[1].content = this.configForm.controls.rejectCode.value;
      this.previewData.messages[2].content = this.configForm.controls.rejectedResponse.value;
    } else if (this.selectedTab === 2) {
      this.previewData.messages[1].content = 'Hello World';
      this.previewData.messages[2].content = `Sorry... ${this.previewData.messages[1].content} is an invalid response to the invitation.\nPlease reply back with the corresponding RSVP code. (You can find the RSVP code by scrolling to the top of the invitation message)`;
      this.configForm.controls.defaultResponse.setValue(`Sorry... {{user_input}} is an invalid response to the invitation.\nPlease reply back with the corresponding RSVP code. (You can find the RSVP code by scrolling to the top of the invitation message)`);

    }
  }

  closeDialog(exitData = null): void {
    this.dialogRef.close(exitData);
  }
}
