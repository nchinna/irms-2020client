import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsLandingDesignComponent } from './sms-landing-design.component';

describe('SmsLandingDesignComponent', () => {
  let component: SmsLandingDesignComponent;
  let fixture: ComponentFixture<SmsLandingDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsLandingDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsLandingDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
