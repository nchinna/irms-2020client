import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsTemplateExpandedViewComponent } from './sms-template-expanded-view.component';

describe('SmsTemplateExpandedViewComponent', () => {
  let component: SmsTemplateExpandedViewComponent;
  let fixture: ComponentFixture<SmsTemplateExpandedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsTemplateExpandedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsTemplateExpandedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
