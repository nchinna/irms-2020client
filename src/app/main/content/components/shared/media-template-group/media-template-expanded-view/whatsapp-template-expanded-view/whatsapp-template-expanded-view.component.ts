import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'irms-whatsapp-template-expanded-view',
  templateUrl: './whatsapp-template-expanded-view.component.html',
  styleUrls: ['./whatsapp-template-expanded-view.component.scss']
})
export class WhatsappTemplateExpandedViewComponent implements OnInit {
  @Input() invitationId: string;
  @Input() isDialog;
  @Input() isLanding;
  constructor() { }

  ngOnInit() {
    
  }

}
