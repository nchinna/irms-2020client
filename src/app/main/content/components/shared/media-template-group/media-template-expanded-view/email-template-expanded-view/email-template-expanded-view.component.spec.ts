import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTemplateExpandedViewComponent } from './email-template-expanded-view.component';

describe('EmailTemplateExpandedViewComponent', () => {
  let component: EmailTemplateExpandedViewComponent;
  let fixture: ComponentFixture<EmailTemplateExpandedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTemplateExpandedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTemplateExpandedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
