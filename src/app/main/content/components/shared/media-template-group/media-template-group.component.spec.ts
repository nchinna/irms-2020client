import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaTemplateGroupComponent } from './media-template-group.component';

describe('MediaTemplateGroupComponent', () => {
  let component: MediaTemplateGroupComponent;
  let fixture: ComponentFixture<MediaTemplateGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaTemplateGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaTemplateGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
