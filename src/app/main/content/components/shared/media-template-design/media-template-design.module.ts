import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RsvpFormEditorModule } from '../rsvp-form-editor/rsvp-form-editor.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { MediaTemplateGroupComponent } from '../media-template-group/media-template-group.component';
import { MainModule } from 'app/main/main.module';
import { EmailLandingDesignComponent } from '../media-template-group/email-preview/email-landing-design/email-landing-design.component';
import { SmsLandingDesignComponent } from '../media-template-group/sms-preview/sms-landing-design/sms-landing-design.component';
import { WhatsappOptionConfigModalComponent } from '../media-template-group/whatsapp-option-config-modal/whatsapp-option-config-modal.component';
import { WhatsappBotConfigModalComponent } from '../media-template-group/whatsapp-bot-config-modal/whatsapp-bot-config-modal.component';
import { MediaTemplateDataService } from './media-template-data.service';
import { EmailTemplateExpandedViewComponent } from '../media-template-group/media-template-expanded-view/email-template-expanded-view/email-template-expanded-view.component';
import { SmsTemplateExpandedViewComponent } from '../media-template-group/media-template-expanded-view/sms-template-expanded-view/sms-template-expanded-view.component';
import { WhatsappTemplateExpandedViewComponent } from '../media-template-group/media-template-expanded-view/whatsapp-template-expanded-view/whatsapp-template-expanded-view.component';
import { EmailDesignerModule } from 'app/main/content/email-designer/email-designer.module';
import { EmailTemplateCardComponent } from '../media-template-group/media-template-card/email-template-card/email-template-card.component';
import { SmsTemplateCardComponent } from '../media-template-group/media-template-card/sms-template-card/sms-template-card.component';
import { WhatsappTemplateCardComponent } from '../media-template-group/media-template-card/whatsapp-template-card/whatsapp-template-card.component';
import { SmsDesignerModule } from 'app/main/content/sms-designer/sms-designer.module';
import { WhatsappDesignerModule } from 'app/main/content/whatsapp-designer/whatsapp-designer.module';



@NgModule({
  declarations: [
    MediaTemplateGroupComponent,
    EmailLandingDesignComponent,
    WhatsappOptionConfigModalComponent,
    SmsLandingDesignComponent,
    WhatsappBotConfigModalComponent,
    EmailTemplateExpandedViewComponent,
    SmsTemplateExpandedViewComponent,
    WhatsappTemplateExpandedViewComponent,    
    SmsTemplateCardComponent,
    EmailTemplateCardComponent,
    WhatsappTemplateCardComponent
  ],
  imports: [
    CommonModule,
    MainModule,
    RsvpFormEditorModule,    
    EmailDesignerModule, 
    SmsDesignerModule,   
    EditorModule,
    WhatsappDesignerModule
    // RsvpFormModule
  ],
  exports: [
    RsvpFormEditorModule,
    EmailDesignerModule, 
    SmsDesignerModule,   
    EditorModule,
    MediaTemplateGroupComponent,
    EmailLandingDesignComponent,
    WhatsappOptionConfigModalComponent,
    SmsLandingDesignComponent,
    WhatsappBotConfigModalComponent,    
    EmailTemplateExpandedViewComponent,
    SmsTemplateExpandedViewComponent,
    WhatsappTemplateExpandedViewComponent,    
    SmsTemplateCardComponent,
    EmailTemplateCardComponent

    // RsvpFormModule
  ],
  providers: [MediaTemplateDataService],
  entryComponents: [EmailLandingDesignComponent, WhatsappOptionConfigModalComponent,
    WhatsappBotConfigModalComponent, SmsLandingDesignComponent, EmailTemplateCardComponent,WhatsappTemplateCardComponent, SmsTemplateCardComponent]
})
export class MediaTemplateDesignModule { }
