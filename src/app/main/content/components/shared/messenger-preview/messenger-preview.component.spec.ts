import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerPreviewComponent } from './messenger-preview.component';

describe('MessengerPreviewComponent', () => {
  let component: MessengerPreviewComponent;
  let fixture: ComponentFixture<MessengerPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessengerPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
