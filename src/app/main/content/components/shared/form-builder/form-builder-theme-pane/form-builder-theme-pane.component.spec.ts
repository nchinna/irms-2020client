import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderThemePaneComponent } from './form-builder-theme-pane.component';

describe('FormBuilderThemePaneComponent', () => {
  let component: FormBuilderThemePaneComponent;
  let fixture: ComponentFixture<FormBuilderThemePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBuilderThemePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderThemePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
