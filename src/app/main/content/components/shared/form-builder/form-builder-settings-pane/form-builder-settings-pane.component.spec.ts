import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderSettingsPaneComponent } from './form-builder-settings-pane.component';

describe('FormBuilderSettingsPaneComponent', () => {
  let component: FormBuilderSettingsPaneComponent;
  let fixture: ComponentFixture<FormBuilderSettingsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBuilderSettingsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderSettingsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
