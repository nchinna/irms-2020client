import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilderComponent } from './form-builder.component';
import { FormBuilderContentPaneComponent } from './form-builder-content-pane/form-builder-content-pane.component';
import { FormBuilderSettingsPaneComponent } from './form-builder-settings-pane/form-builder-settings-pane.component';
import { FormBuilderThemePaneComponent } from './form-builder-theme-pane/form-builder-theme-pane.component';
import { MainModule } from 'app/main/main.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FormModule } from '../form/form.module';
import { GenericColorPickerModule } from '../generic-color-picker/generic-color-picker.module';
import { FormBuilderQuestionSettingsComponent } from './form-builder-question-settings/form-builder-question-settings.component';



@NgModule({
  declarations: [FormBuilderComponent, FormBuilderContentPaneComponent, FormBuilderSettingsPaneComponent, FormBuilderThemePaneComponent, FormBuilderQuestionSettingsComponent],
  imports: [
    CommonModule,
    MainModule,
    EditorModule,
    FormModule,
    GenericColorPickerModule,
  ],
  exports: [FormBuilderComponent, FormBuilderContentPaneComponent, FormBuilderSettingsPaneComponent, FormBuilderThemePaneComponent, FormBuilderQuestionSettingsComponent]
})
export class FormBuilderModule { }
