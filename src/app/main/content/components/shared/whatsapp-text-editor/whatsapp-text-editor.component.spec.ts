import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappTextEditorComponent } from './whatsapp-text-editor.component';

describe('WhatsappTextEditorComponent', () => {
  let component: WhatsappTextEditorComponent;
  let fixture: ComponentFixture<WhatsappTextEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappTextEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappTextEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
