import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'irms-continue-button',
  templateUrl: './continue-button.component.html',
  styleUrls: ['./continue-button.component.scss']
})
export class ContinueButtonComponent implements OnInit {
  @Input()
  theme: any;
  @Input()
  isArabic: boolean;
  @Output()
  next = new EventEmitter<any>();
  @Input()
  buttonText = 'OK';
  @Input()
  hideNextHint = false;

  constructor() { 
   
  }

  ngOnInit(): void{
    
  }

  gotoNext(): void{
    this.next.emit();
  }
}
