import { Directive, Input, OnChanges, SimpleChanges, ElementRef } from '@angular/core';

@Directive({
  selector: '[progressBarColor]'
})
export class ProgressBarColor implements OnChanges {
  static counter = 0;

  @Input() progressBarColor;
  styleEl: HTMLStyleElement = document.createElement('style');
  uniqueAttr = `irms-progress-bar-color-${ProgressBarColor.counter++}`;

  constructor(private el: ElementRef) {
    const nativeEl: HTMLElement = this.el.nativeElement;
    nativeEl.setAttribute(this.uniqueAttr, '');
    nativeEl.appendChild(this.styleEl);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateColor();
  }

  updateColor(): void {
    // update dynamic style with the uniqueAttr
    this.styleEl.innerText = `
      [${this.uniqueAttr}] .mat-progress-bar-fill::after {
        background-color: ${this.progressBarColor.foreground} !important;
      }
      [${this.uniqueAttr}] .mat-progress-bar-buffer {
        background-color: ${this.progressBarColor.background} !important;
      }
    `;
  }

}
