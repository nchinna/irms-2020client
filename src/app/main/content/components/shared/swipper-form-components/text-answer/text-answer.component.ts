import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
// import { Question } from 'src/app/question.model';
// import { QuestionSwipperService } from '../questionSwipper.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-text-answer',
  templateUrl: './text-answer.component.html',
  styleUrls: ['./text-answer.component.scss']
})
export class TextAnswerComponent implements OnInit {

  @Input() theme: any;
  @Input() isArabic: boolean;
  @Input() question;
  @Input() reviewMode = false;
  @Input() public inputControl: FormControl;
  @Output() scrollNext = new EventEmitter<any>();
  @Output() fieldUpdate = new EventEmitter<string>();
  placeholder = 'Type your answer here...';
  placeholderArabic = 'اكتب إجابتك هنا...';
  constructor() { }

  ngOnInit(): void{
  }

  /**
   * On Enter key press
   * @event
   */
  onPressEnter(event): void{
    if (event.keyCode === 13 || event.keyCode === 9) {
      event.preventDefault();
      this.gotoNext();
    }
  }

  /**
   * Go to next
   */
  gotoNext(): void {
    if (this.reviewMode) {
      return;
    }
    this.fieldUpdate.next();
    this.scrollNext.emit();
  }

}
