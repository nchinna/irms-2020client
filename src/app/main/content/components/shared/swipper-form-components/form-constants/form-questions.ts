export enum QuestionColorCode {
  singleLineText = 'rgb(32, 140, 255)',
  multilineText = 'rgb(158, 0, 111)',
  date = 'rgb(219, 90, 4)',
  phone = 'rgb(26, 160, 2)',
  select = 'rgb(112, 108, 99)',
  selectSearch = 'rgb(255,186,73)',
  email = 'rgb(191,57,91)',
  rating = 'rgb(79,169,179)',
  welcome = 'rgb(187, 211, 170)',
  thanks = 'rgb(150, 206, 220)',
  submit = 'rgb(191, 150, 119)',
  // fileUpload = 'rgb(7, 221, 177)',
  // picture = 'rgb(255, 0, 128)'
}
export enum QuestionIcon {
  singleLineText = 'text_format',
  multilineText = 'notes',
  date = 'event',
  phone = 'phone',
  select = 'check_box',
  selectSearch = 'search',
  email = 'alternate_email',
  rating = 'star',
  welcome = 'exit_to_app',
  thanks = 'done_all',
  submit = 'how_to_vote',
  // fileUpload = 'folder',
  // picture = 'insert_photo'
}
export enum QuestionTypeTitle {
  singleLineText = 'Single Line Answer',
  multilineText = 'Multi Line Answer',
  date = 'Date',
  phone = 'Phone Number',
  select = 'Select',
  selectSearch = 'Select Search',
  email = 'Email',
  rating = 'Rating',
  welcome = 'Welcome',
  thanks = 'Thanks',
  submit = 'Submit',
  // fileUpload = 'folder',
  // picture = 'insert_photo'
}
export const QuestionTypeList = [
  {
    type: 'singleLineText',
    typeTitle: QuestionTypeTitle.singleLineText,
    icon: QuestionIcon.singleLineText,
    colorCode: QuestionColorCode.singleLineText
  },
  {
    type: 'multilineText',
    typeTitle: QuestionTypeTitle.multilineText,
    icon: QuestionIcon.multilineText,
    colorCode: QuestionColorCode.multilineText
  },
  {
    type: 'date',
    typeTitle: QuestionTypeTitle.date,
    icon: QuestionIcon.date,
    colorCode: QuestionColorCode.date
  },
  {
    type: 'phone',
    typeTitle: QuestionTypeTitle.phone,
    icon: QuestionIcon.phone,
    colorCode: QuestionColorCode.phone
  },
  {
    type: 'select',
    typeTitle: QuestionTypeTitle.select,
    icon: QuestionIcon.select,
    colorCode: QuestionColorCode.select
  },
  {
    type: 'selectSearch',
    typeTitle: QuestionTypeTitle.selectSearch,
    icon: QuestionIcon.selectSearch,
    colorCode: QuestionColorCode.selectSearch
  },
  {
    type: 'email',
    typeTitle: QuestionTypeTitle.email,
    icon: QuestionIcon.email,
    colorCode: QuestionColorCode.email
  },
  {
    type: 'rating',
    typeTitle: QuestionTypeTitle.rating,
    icon: QuestionIcon.rating,
    colorCode: QuestionColorCode.rating
  },
  // {
  //   type: 'fileUpload',
  //   typeTitle: 'File Upload',
  //   icon: QuestionIcon.fileUpload,
  //   colorCode: QuestionColorCode.fileUpload
  // },
  // {
  //   type: 'picture',
  //   typeTitle: 'Picture',
  //   icon: QuestionIcon.picture,
  //   colorCode: QuestionColorCode.picture
  // }
];
export const questionnaire = {
    id: '',
    title: '',
    formUrl: '',
    isActive: false,
    formJson: {
        languageOption: false,
        defaultLanguage: 'en',
        welcomeScreen: {
          type: 'welcome',
          welcomeMessage: 'Welcome to Form Builder!',
          buttonText: 'Register',
          welcomeMessageArabic: 'مرحبا بك إلى فورم بلدر!',
          buttonTextArabic: 'تسجيل'
        },
        questions: [],
        submitAreaJson: {
          type: 'submit',
          submitMessage: `Everything seems good,\nYou can submit now!`,
          submitMessageArabic: `جميع البيانات صحيحة،\n قم بتقديم الطلب الآن!`,
          submitButton: 'Submit',
          submitButtonArabic: 'تقدم'
        },
        thankYouJson: {
          type: 'thanks',
          thankyouMessage: `Thank you!`,
          thankyouMessageArabic: `شكرا لك!`
        }
      },
    styleJson: {
        backgroundImagePath: '',
        themeJson: {
          answer: 'rgb(1, 32, 66)',
          background: '#fff',
          backgroundStyle: 'cover',
          buttons: 'rgb(1, 32, 66)',
          buttonsText: '#fff',
          font: 'ithra',
          question: 'rgb(1, 32, 66)'
        }
      }
};
export const questionObj = {
    singleLineText: {
        type: 'singleLineText',
        id: null,
        headlineArabic: '',
        headline: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        multiline: false,
        minLength: 3,
        maxLength: 20,
        pattern: '',
        mapping: false,
        mappingField: '',
        isNumber: false,
        jumps: []
    },
    multilineText: {
        type: 'multilineText',
        id: null,
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        multiline: true,
        minLength: 1,
        maxLength: 50,
        mapping: false,
        mappingField: '',
        jumps: []
    },
    date: {
        type: 'date',
        id: '',
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        minDate: null,
        maxDate: null,
        mapping: false,
        mappingField: '',
        jumps: []
    },
    phone: {
        type: 'phone',
        id: '',
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        mapping: false,
        mappingField: '',
        jumps: []
    },
    select: {
        type: 'select',
        id: '',
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        multiple: false,
        choices: [],
        mapping: false,
        mappingField: '',
        jumps: []
    },
    selectSearch: {
        type: 'selectSearch',
        id: '',
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        multiple: true,
        choices: [],
        mapping: false,
        mappingField: '',
        jumps: []
    },
    email: {
        type: 'email',
        id: '',
        headline: ``,
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        minLength: 3,
        pattern: '[^@]+@[^@]+\.[a-zA-Z]{2,6}',
        mapping: false,
        mappingField: '',
        jumps: []
    },
    rating: {
        type: 'rating',
        id: '',
        headline: '',
        headlineArabic: '',
        isDescription: false,
        description: null,
        descriptionArabic: null,
        required: false,
        mapping: false,
        mappingField: '',
        jumps: []
    },
//     fileUpload: {
//       type: 'fileUpload',
//       id: '',
//       headline: '',
//       headlineArabic: '',
//       isDescription: false,
//       description: null,
//       descriptionArabic: null,
//       required: false,
//       maxFileSize: 50,
//       fileTypes: '',
//       mapping: false,
//       mappingField: '',
//       jumps: []
//   },
//   picture: {
//     type: 'picture',
//     id: '',
//     headline: '',
//     headlineArabic: '',
//     isDescription: false,
//     description: null,
//     descriptionArabic: null,
//     pictureLink: 'http://unsplash.it/256?random',
//     multiline: false,
//     required: false,
//     mapping: false,
//     mappingField: '',
//     jumps: []
// }
};

