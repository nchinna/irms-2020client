import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IResponseModel } from 'types';
import { ContactsListItemComponent } from './contacts-list-item/contacts-list-item.component';
import { EventGuestDataService } from 'app/main/content/event/event-layout/event-guest/event-guest-data.service';
import { EventGuestService } from 'app/main/content/event/event-layout/event-guest/event-guest.service';

@Component({
  selector: 'irms-contact-guest-list-search',
  templateUrl: './contact-guest-list-search.component.html',
  styleUrls: ['./contact-guest-list-search.component.scss']
})
export class ContactGuestListSearchComponent implements OnInit {
  public listItemComponent: Object = ContactsListItemComponent;

  @Input() selectedIds = {
    contacts: [],
    guests: []
  };
  @Output() selectedListsEvent: EventEmitter<any> = new EventEmitter();

  contacts: Observable<IResponseModel> = of({
    items: [],
    totalCount: 0
  });

  isSearching = false;

  guests: Observable<IResponseModel> = of({
    items: [],
    totalCount: 0
  });

  loadedData = {
    contacts: [],
    guests: []
  };

  importFromFilters = {
    contacts: true,
    guests: true
  };

  selectedContactLists;
  selectedGuestLists;

  constructor(
    protected dataService: EventGuestDataService,
    public sectionService: EventGuestService,
   ) {
      this.dataService.getGuestLists().subscribe(res => {
        this.guests = of({
          items: res,
          totalCount: res.length
        });
        this.loadedData.guests = res;
      });

      this.dataService.getContactLists().subscribe(res => {
        res.push(this.sectionService.unassignedList);
        this.contacts = of({
          items: res,
          totalCount: res.length
        });
        this.loadedData.contacts = res;
      });
    }

  ngOnInit(): void {  }

  // Handle filter change
  filterChange(event, listType): void {
    if (listType === 'contacts') {
      this.selectedIds.contacts = event;
    } else {
      this.selectedIds.guests = event;
    }
  }

  /// Refresh lists with filters method
  refreshLists(): void {

  }

  searchQueryChange(event) {
    const searchString = event.toUpperCase();
    
    if (!event) {
      this.isSearching = false;
    } else {

      const contacts = [];
      const guests = [];

      this.loadedData.contacts.forEach(contact => {
        if (contact.name.toUpperCase().includes(searchString)) {
          contacts.push(contact);
        }
      });

      this.loadedData.guests.forEach(guest => {
        if (guest.name.toUpperCase().includes(searchString)) {
          guests.push(guest);
        }
      });
      
      this.selectedContactLists = of({
        items: contacts,
        totalCount: contacts.length
      });

      this.selectedGuestLists = of({
        items: guests,
        totalCount: guests.length
      });

      this.isSearching = true;
    }
  }

  /// On dialog confirm
  confirm(): void {
    if (!this.importFromFilters.contacts) {
      this.selectedIds.contacts = Array<any>();

    }
    if (!this.importFromFilters.guests) {
      this.selectedIds.guests = Array<any>();
    }

    this.selectedListsEvent.emit({
      selectedListsIds: this.selectedIds,
      loadedData: {
        contacts : this.loadedData.contacts
          .filter(x => 
            this.selectedIds.contacts.includes(x.id)
          ),
        guests: this.loadedData.guests
          .filter(x => 
            this.selectedIds.guests.includes(x.id)
          )
      }
    });
  }

  /// On dialog close
  close(): void {
    this.selectedListsEvent.emit();
  }

}
