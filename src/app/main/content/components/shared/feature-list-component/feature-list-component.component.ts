import { Component, OnInit, Input, EventEmitter, Output, ViewChild, OnDestroy, DoCheck, Type, IterableDiffers } from '@angular/core';
import { BASE_PAGE_SIZE } from 'app/constants/constants';
import { Observable, Subscription } from 'rxjs';
import { IResponseModel } from 'types';
import { MatSelectionList } from '@angular/material';
import { fuseAnimations } from 'app/core/animations';
import { FeatureListService } from './feature-list-service.service';

@Component({
  selector: 'irms-feature-list',
  templateUrl: './feature-list-component.component.html',
  styleUrls: ['./feature-list-component.component.scss'],
  providers: [FeatureListService],
  animations: fuseAnimations
})
export class FeatureListComponent implements OnInit, DoCheck {
  public pageSize = BASE_PAGE_SIZE;
  public pageIndex = 0;
  @Input() selectedIds: string[] = [];
  @Input() currentId: string;
  @Input() selectable: boolean;
  @Input() readonly: boolean = false;
  @Input() public dataList: [];
  @Input() public dataListItemBody: Type<any>;
  @Input() public selectedIdsList: any[];

  @Output() public onCheckChange: EventEmitter<string[]> = new EventEmitter();
  @Output() public onReadItem: EventEmitter<string> = new EventEmitter();
  @Output() public onSelectAll: EventEmitter<string[]> = new EventEmitter();
  @Output() public onDeSelectAll: EventEmitter<string> = new EventEmitter();

  @Output() public onChangePagination: EventEmitter<Event> = new EventEmitter();

  @ViewChild('list', { read: MatSelectionList, static: false }) matSelectionList: MatSelectionList;
  differ: any;

  constructor(private featureListService: FeatureListService, differs: IterableDiffers) {
    this.differ = differs.find([]).create(null);
  }

  ngOnInit() {
    this.selectedIds = this.dataList.reduce(function (filtered, option) {
      if (option['isChecked']) {
        filtered.push(option['id']);
      }
      return filtered;
    }, []);
  }

  ngDoCheck() {
    const change = this.differ.diff(this.selectedIds);
    if (change) {
      this.featureListService.setSelectAllItems(change.collection);
    }
  }

  toggleSelectedId(newSelectedItem) {
    // First, check if we already have that todo as selected...
    const index = this.selectedIds.indexOf(newSelectedItem.id);
    index > -1 ? this.selectedIds.splice(index, 1) : this.selectedIds.push(newSelectedItem.id);
    this.onCheckChange.emit(this.selectedIds);
  }

  readItem(item) {
    this.onReadItem.emit(item);
  }

}
