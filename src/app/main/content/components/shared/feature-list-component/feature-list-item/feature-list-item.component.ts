import { Component, OnInit, OnChanges, OnDestroy, Input, HostBinding, Type, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subscription } from 'rxjs';
import { FeatureListService } from '../feature-list-service.service';

@Component({
  selector: 'irms-feature-list-item',
  templateUrl: './feature-list-item.component.html',
  styleUrls: ['./feature-list-item.component.scss'],
  animations: fuseAnimations
})
export class FeatureListItemComponent implements OnInit, OnChanges, OnDestroy {
  @Input() selectable: boolean;
  @Input() readonly: boolean;
  @Input() data;
  @HostBinding('class.selected') selected: boolean;
  @Input() public dataListItemBody: Type<any>;
  @Input() selectedIds;
  @Input() isSubscribed;
  onSelectedlistItemChanged: Subscription;
  @Output() public onSelectItem: EventEmitter<Event> = new EventEmitter();

  constructor(private featureListService: FeatureListService) {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnInit() {
    // Set the initial values
    // Subscribe to update on selected mail change
    this.onSelectedlistItemChanged =
      this.featureListService.onCurrentIdsSelectesChanged
        .subscribe(selectedIds => {
          this.selected = false;
          if (selectedIds.length > 0) {
            for (const id of selectedIds) {
              if (id === this.data.id) {
                this.selected = true;
                break;
              }
            }
          }
          if (this.data.isChecked) {
            this.selected = true;
          }
        });
  }

  ngOnDestroy() {
    this.onSelectedlistItemChanged.unsubscribe();
  }

  onSelectedChange(item) {
    // this.featureListService.toggleSelectedId(this.data.id);
    this.onSelectItem.emit(item);
  }


}
