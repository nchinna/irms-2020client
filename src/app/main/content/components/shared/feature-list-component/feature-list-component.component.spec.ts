import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureListComponentComponent } from './feature-list-component.component';

describe('FeatureListComponentComponent', () => {
  let component: FeatureListComponentComponent;
  let fixture: ComponentFixture<FeatureListComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureListComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureListComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
