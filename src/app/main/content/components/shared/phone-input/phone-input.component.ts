import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-phone-input',
  templateUrl: './phone-input.component.html',
  styleUrls: ['./phone-input.component.scss']
})
export class PhoneInputComponent implements OnInit {
  @Input() public inputControl: FormControl;
  @Input() public required: boolean;
  @Input() public flexWidth: string;
  @Input() public requiredErrorText = 'This input field cannot be empty';
  @Input() public validErrorText = 'Please enter a valid phone number.';
  @Input() public existsErrorText = 'This phone number already exists';
  @Input() public fieldLabel: string;
  @Output() onBlur: EventEmitter<Event> = new EventEmitter();
  @ViewChild('mobileSelect', { static: false }) mobileSelect;
  
  public phoneConfig = {
    initialCountry: 'sa',
    preferredCountries: ['sa', 'ae', 'bh', 'kw', 'om', 'ps', 'lb', 'jo', 'sy', 'eg', 'us'],
    excludeCountries: ['il', 'ir', 'kp']
  };

  constructor() { }

  ngOnInit() {
  }
  
  handleBlur(): void {
    this.onBlur.emit(event);
  }

  getNumber(e) {
    this.inputControl.setValue(e);
    this.onBlur.emit(e);
  }

  hasError(notError) {
    if (!notError && this.inputControl.value) {
      this.inputControl.setErrors({ phoneError: 'Please enter a valid phone number' });
    }
  }

}
