import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPreferredMediaDialogComponent } from './campaign-preferred-media-dialog.component';

describe('CampaignPreferredMediaDialogComponent', () => {
  let component: CampaignPreferredMediaDialogComponent;
  let fixture: ComponentFixture<CampaignPreferredMediaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignPreferredMediaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPreferredMediaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
