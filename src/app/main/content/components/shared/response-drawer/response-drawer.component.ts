import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-response-drawer',
  templateUrl: './response-drawer.component.html',
  styleUrls: ['./response-drawer.component.scss']
})
export class ResponseDrawerComponent implements OnInit {
  @Input() htmlData: string = '';

  constructor() { }

  ngOnInit() {
  }

}
