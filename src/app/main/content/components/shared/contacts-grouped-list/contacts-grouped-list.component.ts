import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { GenericContactListItemComponent } from '../generic-contact-list-item/generic-contact-list-item.component';
import { ContactsGroupedListService } from './contacts-grouped-list.service';
import { Subscription } from 'rxjs';
import { FixedSizeVirtualScrollStrategy } from '@angular/cdk/scrolling';

@Component({
  selector: 'irms-contacts-grouped-list',
  templateUrl: './contacts-grouped-list.component.html',
  styleUrls: ['./contacts-grouped-list.component.scss']
})

export class ContactsGroupedListComponent implements OnInit {
  listItemComponent: Object = GenericContactListItemComponent;
  @Input() data;
  @Input() selected = [
  ];
  @Input() svgPositionRelative = false;
  @Output() public onCheckChange: EventEmitter<any> = new EventEmitter();
  subscription: Subscription;
  constructor(protected listService: ContactsGroupedListService) {
    this.subscription = this.listService.getSelectToggle().subscribe(data => {
      this.selectAll(data);
    });
  }

  ngOnInit() {
    console.log(this.data)
  }

  /// get selected items of a given list Id
  getSelected(listId): any[] {
    const index = this.selected.findIndex(x => x.listId === listId);
    if (index !== -1) {
      return this.selected[index].selectedIds;
    } else {
      return [];
    }
  }
  selectionChange(event, listId): void {
    const dataIndex = this.data.findIndex(x => x.listId === listId);
    let index = 0;
    index = this.selected.findIndex(x => x.listId === listId);
    if (index === -1) {
      this.selected.push({
        listId: listId,
        selected: event.selectedItems,
        selectedIds: event.selectedIds
      });
      index = this.selected.length - 1;
    } else {
      this.selected[index].selected = event.selectedItems;
      this.selected[index].selectedIds = event.selectedIds;
    }
    // set group checkbox status
    if (event.selectedIds.length === 0) {
      this.data[dataIndex].checked = 'none';
    } else if (this.selected[index].selectedIds.length === this.data[dataIndex].items.length) {
      this.data[dataIndex].checked = 'all';
    } else if (this.selected[index].selectedIds.length > 0) {
      this.data[dataIndex].checked = 'indeterminate';
    }
    console.log(this.selected)
    this.onCheckChange.emit(this.selected);
  }
  selectGuest(event, listId, guestId, contact) {
    const i = this.data.findIndex(x => x.listId === listId);
    // let index = this.data.findIndex(x => x.listId === listId);
    let list = this.selected.find(x => x.listId == listId);
    if (event) {
      if (list) {
        if (!list.selected.find(x => x.item.id == guestId)) {
          this.selected.find(x => x.listId == listId).selected.push({
            item: contact,
            contactListId: listId
          })
          this.selected.find(x => x.listId == listId).selectedIds.push(guestId)
        }
      } else {
        this.selected.push({
          listId: listId,
          selected: [{
            item: contact,
            contactListId: listId
          }],
          selectedIds: [guestId]
        })
      }
    } else {
      this.selected[i].selectedIds = this.selected[i].selectedIds.filter(x=> x != guestId);
      this.selected[i].selected = this.selected[i].selected.splice(this.selected[i].selected.splice(this.selected[i].selected.findIndex(x => x.id == guestId), 1));

    }
  }

  /// toggle  Select a list
  groupSelect(event, listId): void {
    const dataIndex = this.data.findIndex(x => x.listId === listId);
    const ids = [];
    const items = [];
    if (event) {
      this.data[dataIndex].items.forEach(contact => {
        items.push(contact);
        ids.push(contact.id);
      });
      this.data[dataIndex].checked = 'all';
    } else {
      this.data[dataIndex].checked = 'none';
    }

    const eventt = {
      selectedItems: items,
      selectedIds: ids
    };
    this.selectionChange(eventt, listId);
  }
  /// ToggleSelectAll
  selectAll(event): void {
    this.data.forEach(list => {
      this.groupSelect(event, list.listId);
    });
  }

  debug() {
    console.log(this.data)
  }

}
