import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsGroupedListComponent } from './contacts-grouped-list.component';

describe('ContactsGroupedListComponent', () => {
  let component: ContactsGroupedListComponent;
  let fixture: ComponentFixture<ContactsGroupedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsGroupedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsGroupedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
