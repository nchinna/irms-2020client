import {Observable} from 'rxjs/Observable';
import {BaseModel} from './base.model';
import { IResponseModel, IPage } from 'app/core/types/types';

export interface SectionDataService<T extends BaseModel> {
  
  getList(filterParam: IPage): Observable<IResponseModel>;

  get(id): Observable<T>;

  create(model): Observable<string>; // returns id of created entity
  update(model): Observable<any>;

  delete(model): Observable<any>;
}
