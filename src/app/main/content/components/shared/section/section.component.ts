import {OnDestroy, OnInit} from '@angular/core';
import {SectionService} from './section.service';
import {SectionDataService} from './section-data.service';
import {BaseModel} from './base.model';
import {Router} from '@angular/router';
import {Section} from '../../../../../constants/constants';
import {Subscription} from 'rxjs/Subscription';

export class SectionComponent<T extends BaseModel> implements OnInit, OnDestroy {
  public hideCreateButton = false;

  subscriptions: Subscription[] = [];

  constructor(public sectionName: Section,
              public sectionService: SectionService<T>,
              protected dataService: SectionDataService<T>,
              protected router: Router) {
  }

  selectedItems: T[];

  ngOnInit(): void {
    this.subscriptions.push(this.sectionService.onCurrentChanged.subscribe(() => {
    }));
  }

  create(): void  {
    this.router.navigate([`${this.sectionName}/create`]);
  }

  afterDelete(): void  {
    if (this.sectionService.onCurrentChanged.value && this.sectionService.selectedIds.indexOf(this.sectionService.onCurrentChanged.value.id) >= 0) {
      this.router.navigate([`${this.sectionName}`]);
    }

    this.sectionService.resetSelection();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.sectionService.dispose();
  }
}
