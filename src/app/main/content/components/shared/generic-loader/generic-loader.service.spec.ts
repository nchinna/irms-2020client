import { TestBed, inject } from '@angular/core/testing';

import { GenericLoaderService } from './generic-loader.service';

describe('GenericLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenericLoaderService]
    });
  });

  it('should be created', inject([GenericLoaderService], (service: GenericLoaderService) => {
    expect(service).toBeTruthy();
  }));
});
