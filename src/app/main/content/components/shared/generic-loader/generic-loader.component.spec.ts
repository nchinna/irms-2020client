import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericLoaderComponent } from './generic-loader.component';

describe('GenericLoaderComponent', () => {
  let component: GenericLoaderComponent;
  let fixture: ComponentFixture<GenericLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
