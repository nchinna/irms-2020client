import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'irms-generic-content-loader',
  templateUrl: './generic-content-loader.component.html',
  styleUrls: ['./generic-content-loader.component.scss']
})
export class GenericContentLoaderComponent implements OnInit {
  @Input() loaderType = 'generic';
  @HostBinding('class.fullWidth') fullWidth = false;
  constructor() { }

  ngOnInit() {
    if (this.loaderType === 'section-list') {
      this.fullWidth = true;
    }
  }

}
