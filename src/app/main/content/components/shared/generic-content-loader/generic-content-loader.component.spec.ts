import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericContentLoaderComponent } from './generic-content-loader.component';

describe('GenericContentLoaderComponent', () => {
  let component: GenericContentLoaderComponent;
  let fixture: ComponentFixture<GenericContentLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericContentLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericContentLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
