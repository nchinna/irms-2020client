import {Component, Input} from '@angular/core';
import {BASE_URL} from '../../../../../constants/constants';

@Component({
  selector: 'irms-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent  {

  @Input() public image: string;
  @Input() public isAvatar = true;
  @Input() public isLarger = false;
  @Input() public base64;

  public baseUrl = BASE_URL;

  constructor() {
  }
}
