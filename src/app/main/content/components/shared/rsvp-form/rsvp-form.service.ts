import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { EmailDesignerDataService } from 'app/main/content/email-designer/email-designer-data.service';

@Injectable({
  providedIn: 'root'
})
export class RsvpFormService {
  rvspFormValue = {
    formContent: {
      welcome: {
        content: `<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
        <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
        <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`,
        button: 'Proceeded'
      },
      rsvp: {
        content: `<p><span style="font-family: tahoma, arial, helvetica, sans-serif;">Do you accept the Invitation to the&nbsp;</span></p>
        <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p><span style="color: #34495e; font-family: 'arial black', sans-serif;">Select your Response:&nbsp;</span></p>`,
        acceptButton: `Yes I'll be there`,
        rejectButton: `Nope I won't be there`,
      },
      accept: {
        content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
        <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
        <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: center;">&nbsp;</p>`
      },
      reject: {
        content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
        <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
        <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`
      },
    },
    formTheme: {
      buttons: {
        background: '#576268',
        text: '#fff',
      },
      background: {
        color: '#fff',
        image: '',
        style: '',
      }
    },
    formSettings: {
      title: ''
    }
  };

  public rsvpForm = new BehaviorSubject<any>(this.rvspFormValue);
  private refreshForm = new Subject<any>();
  private currentSection = new Subject<any>();
  private tags = new BehaviorSubject<any>(null);
  tempId: number;

  constructor(
    private dataService: EmailDesignerDataService
  ) { }

  getTags(id: any): Observable<any> {
    if (!this.tags.value) {
      return this.dataService.getPlaceholders({ pageNo: 1, pageSize: 100 }, id, this.tempId, true);
    } else {
      return this.tags.asObservable();
    }
  }

  setTags(tags: any): void {
    this.tags.next(tags);
  }

  getRsvpForm(): Observable<any> {
    return this.rsvpForm.asObservable();
  }
  setRsvpForm(questionnaire: any): void {
    this.rsvpForm.next(questionnaire);
  }

  getRefreshFormTrigger(): Observable<any> {
    return this.refreshForm.asObservable();
  }
  refreshFormTrigger(): void {
    this.refreshForm.next(true);
  }

  getCurrentSection(): Observable<any> {
    return this.currentSection.asObservable();
  }
  setCurrentSection(section): void {
    this.currentSection.next(section);
  }
}
