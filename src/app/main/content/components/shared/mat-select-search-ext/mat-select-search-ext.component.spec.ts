import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatSelectSearchExtComponent } from './mat-select-search-ext.component';

describe('MatSelectSearchExtComponent', () => {
  let component: MatSelectSearchExtComponent;
  let fixture: ComponentFixture<MatSelectSearchExtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatSelectSearchExtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatSelectSearchExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
