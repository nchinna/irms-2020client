import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericColorPickerComponent } from './generic-color-picker.component';

describe('GenericColorPickerComponent', () => {
  let component: GenericColorPickerComponent;
  let fixture: ComponentFixture<GenericColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
