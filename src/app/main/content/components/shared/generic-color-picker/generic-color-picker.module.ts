import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericColorPickerComponent } from './generic-color-picker.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { MainModule } from 'app/main/main.module';


@NgModule({
  declarations: [GenericColorPickerComponent],
  imports: [
    CommonModule,
    ColorPickerModule,
    MainModule
  ],
  exports: [GenericColorPickerComponent]
})
export class GenericColorPickerModule { }
