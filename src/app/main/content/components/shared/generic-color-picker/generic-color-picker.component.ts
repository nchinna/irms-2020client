import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-generic-color-picker',
  templateUrl: './generic-color-picker.component.html',
  styleUrls: ['./generic-color-picker.component.scss']
})
export class GenericColorPickerComponent {
  @Input() public color: FormControl;
  constructor() { }
  updateColor(event): void{
    this.color.setValue(event);
    if (!this.color.dirty) {
      this.color.markAsDirty();
    }
  }

}
