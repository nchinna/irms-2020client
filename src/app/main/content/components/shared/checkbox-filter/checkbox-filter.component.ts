import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PeriodCheckBoxRange } from 'app/constants/constants';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'irms-checkbox-filter',
  templateUrl: './checkbox-filter.component.html',
  styleUrls: ['./checkbox-filter.component.scss']
})
export class CheckboxFilterComponent implements OnInit{
  public filter = PeriodCheckBoxRange;
  @Input() selectedFilters = [];
  delayedFilter: any;
  @Input() vertical = false;
  @Input() filterType = 'timePeriod';
  @Input() filterArray: Array<any>;
  @Output() public onFilterChange: EventEmitter<Array<any>> = new EventEmitter();
  debouncer: Subject<any> = new Subject<any>();
  filterGroup: FormGroup;

  constructor(private fb: FormBuilder) { }
  ngOnInit(): void {
    this.filterGroup = this.fb.group({
      selected: this.fb.array([])
    });
    if (this.filterType === 'timePeriod') {
      this.filterArray = [
        {id: this.filter.Live, name: 'Live'},
        {id: this.filter.Upcoming, name: 'Upcoming'},
        {id: this.filter.Past, name: 'Past'},
      ];
    }
    this.setupFilterDebouncer();
  }
  
  private setupFilterDebouncer(): void {
    this.debouncer.pipe(
      debounceTime(300),
      distinctUntilChanged(),
    ).subscribe((data: any) => {
      this.delayedFilter = data;
      this.change();
    });
  }
  public onFilterChangeMethod(filter: any): void {
    this.debouncer.next(filter);
  }

  change(): void {
    const event = this.delayedFilter.event;
    const value = this.delayedFilter.value;
    const selected = this.filterGroup.controls.selected as FormArray;
    if (event) {
      const index = selected.controls.findIndex(x => x.value === value);
      if (index === -1) {
        selected.push(new FormControl(value));
      }
    } else {
      const index = selected.controls.findIndex(x => x.value === value);
      selected.removeAt(index);
    }
    this.onFilterChange.emit(selected.value);
  }


}
