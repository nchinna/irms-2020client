import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form.component';
import { MainModule } from 'app/main/main.module';
import { SwipperFormComponentsModule } from '../swipper-form-components/swipper-form-components.module';
import { FormService } from './form.service';
import { SwiperConfigInterface, SWIPER_CONFIG, SwiperModule } from 'ngx-swiper-wrapper';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'vertical',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [FormComponent],
  imports: [
    CommonModule,
    MainModule,
    SwiperModule,
    SwipperFormComponentsModule,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }],
  exports: [FormComponent]
})
export class FormModule { }
