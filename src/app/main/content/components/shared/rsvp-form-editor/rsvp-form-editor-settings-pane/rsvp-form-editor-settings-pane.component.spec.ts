import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvpFormEditorSettingsPaneComponent } from './rsvp-form-editor-settings-pane.component';

describe('RsvpFormEditorSettingsPaneComponent', () => {
  let component: RsvpFormEditorSettingsPaneComponent;
  let fixture: ComponentFixture<RsvpFormEditorSettingsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvpFormEditorSettingsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvpFormEditorSettingsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
