import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { Toast } from 'app/core/decorators/toast.decorator';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RsvpFormEditorDataService {
  private url = `${BASE_URL}api/campaign`;
  private urlInvitation = `${BASE_URL}api/campaignInvitation`;
  constructor(private http: HttpClient) { }

  @Toast('Successfully updated')
  copyTemplate(model: any): Observable<any>{
    return this.http.post<any>(`${this.urlInvitation}/copy-invitation-template`, model);
  }
}
