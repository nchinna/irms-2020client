import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { WhatsappDesignerService } from '../whatsapp-designer.service';
import { WhatsappDesignerDataService } from '../whatsapp-designer-data.service';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from '../../services/can-exit.guard';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { fuseAnimations } from 'app/core/animations';

@Component({
    selector: 'irms-whatsapp-designer-design',
    templateUrl: './whatsapp-designer-design.component.html',
    styleUrls: ['./whatsapp-designer-design.component.scss'],
    animations: fuseAnimations
})
export class WhatsappDesignerDesignComponent implements OnInit, OnDestroy, CanExit {

    public whatsappForm = this.fb.group({
        sender: ['', [Validators.required, Validators.maxLength(100)]],
        template: this.fb.group({
            id: '',
            name: '',
            content: ''
        }),
        method: ['bot', [Validators.required]],
        whatsappContent: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(320)]]
    });
    template = [];

    templateId = '';
    senders;
    tagSearchKeyword = '';
    tags;
    messageContent = '';
    body = '';
    listName = '';
    @Input() invitationId: string;
    @Input() isReview: boolean;
    @Input() isLanding = true;
    @Input() isDialog = false;

    hasUnstagedChanges = false;
    currentCursor;
    subscriptions: Subscription[] = [];
    isChangingMethod: boolean;

    isFormUpdating = false;

    constructor(private fb: FormBuilder,
                protected sectionService: WhatsappDesignerService,
                protected dataService: WhatsappDesignerDataService,
                protected rsvpService: RsvpFormService,
                protected designerService: WhatsappDesignerService,
                private dialog: MatDialog,
                private route: ActivatedRoute,
                protected router: Router
    ) {

        if (this.router.url.includes('data-review')) {
            this.isReview = true;
        }
        this.subscriptions.push(this.designerService.saveObs().subscribe(data => this.saveTemplate()));
        this.subscriptions.push(this.designerService.sendTestObs().subscribe(data => this.sendTestWhatsapp()));
        this.subscriptions.push(this.sectionService.getRefreshTrigger().subscribe(data => {
            if (this.sectionService.template) {
                this.whatsappForm.setValue(this.sectionService.template);
                this.listName = this.sectionService.listName;
            }
        }));
    }

    ngOnInit(): void {
        this.senders = [];
        if (this.isReview) {
            this.dataService.loadTemplates(this.invitationId).subscribe(template => { // Load Template Api Call
                this.template = template;
                if (this.templateId) {
                    this.whatsappForm.controls.template.setValue(
                        this.template.filter(x => x.id === this.templateId)[0]);

                }
                if (template) {
                    this.sectionService.rvspFormValue = template;
                } else {
                    this.sectionService.initDefaultForm();
                }
                this.formInit();
            });
        } else {
            if (!this.isDialog && this.invitationId) {
                this.dataService.loadTemplates(this.invitationId).subscribe(template => { // Load Template Api Call
                    this.template = template;
                    if (this.templateId) {
                        this.whatsappForm.controls.template.setValue(this.template.filter(x => x.id === this.templateId)[0]);

                        this.template.hasOwnProperty('listName') ? this.listName = this.template['listName'] : this.listName = '';
                    }
                    if (template) {
                        this.sectionService.rvspFormValue = template;
                    } else {
                        this.sectionService.initDefaultForm();
                    }

                    this.formInit();
                });
            } else if (this.isDialog) {
                this.sectionService.template.hasOwnProperty('listName') ? this.listName = this.template['listName'] : this.listName = '';
                this.whatsappForm.patchValue(this.sectionService.template);
                this.whatsappForm.get('template').patchValue(this.sectionService.template.template);
                this.body = this.sectionService.template.template.content;
                this.dataService.loadTemplates(this.invitationId).subscribe(template => { // Load Template Api Call
                    this.template = template;
                    if (this.templateId) {
                        this.whatsappForm.controls.template.setValue(
                            this.template.filter(x => x.id === this.templateId)[0]);
                    }
                    if (template) {
                        this.sectionService.rvspFormValue = template;
                    } else {
                        this.sectionService.initDefaultForm();
                    }

                    this.formInit();
                });

            }
        }

        if (this.isReview) {
            this.dataService.loadSendersDataReview()
                .subscribe(result => {
                    result.forEach(item => {
                        this.senders.push({
                            id: item,
                            value: item
                        });
                    });

                    const template = this.sectionService.template;
                    template.senderName = this.senders[0].value;
                    this.whatsappForm.patchValue({
                        sender: this.senders[0].id
                    });
                });
        } else {
            this.dataService.loadSenders()
                .subscribe(result => {
                    result.forEach(item => {
                        this.senders.push({
                            id: item,
                            value: item
                        });
                    });

                    const template = this.sectionService.template;
                    template.senderName = this.senders[0].value;
                    this.whatsappForm.patchValue({
                        sender: this.senders[0].id
                    });
                });
        }

        this.whatsappForm.get(['template', 'id']).valueChanges.subscribe(x => {
            if (x) {
                const temp = this.template.filter(y => y.id === x)[0];
                let body = temp.content;
                this.body = temp.content;
                this.whatsappForm.controls.whatsappContent.setValue(temp);
                body = body.replace(/\*(\S(.*?\S)?)\*/gm, '<strong>$1</strong>');
                body = body.replace(/\_(\S(.*?\S)?)\_/gm, '<em>$1</em>');
                body = body.replace(/\~(\S(.*?\S)?)\~/gm, '<strike>$1</strike>');
                body = body.replace(/\```(\S(.*?\S)?)\```/gm, '<code>$1</code>');
                body = body.replace(/\n/g, '<br>');
                this.messageContent = body;
            }
        });

        this.updateTags();
        this.onFormUpdate();
        this.whatsappForm.valueChanges.subscribe(x => this.saveTemplateInService());
    }

    canSwitch(event): any {
        if (this.whatsappForm.get(['template', 'id']).touched) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
            dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
            dialogRef.componentInstance.confirmButton = 'Leave page';
            dialogRef.componentInstance.cancelButton = 'Stay';
            dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
            return dialogRef.afterClosed().subscribe(canLeave => {
                if (canLeave) {
                    const temp = this.whatsappForm.controls.method.value;
                    this.whatsappForm.reset();
                    this.whatsappForm.controls.method.setValue(temp);
                    this.whatsappForm.controls.sender.setValue(this.senders[0].id);
                    // this.messageContent = this.body = '';
                }
                else {
                    const prevValue = this.whatsappForm.controls.method.value === 'template' ? 'bot' : 'template';
                    this.whatsappForm.controls.method.setValue(prevValue);
                }
            });
        }
    }

    saveTemplateInService(): void {
        this.sectionService.setTemplate(this.whatsappForm.value);
    }

    private updateTags(): void {
        const filter = this.sectionService.getFilter();
        this.dataService.getPlaceholders(filter, this.invitationId).subscribe(result => {
            const tags = [];
            result.forEach(item => {
                tags.push({
                    id: item.id,
                    label: item.title,
                    value: item.placeholder,
                });
                this.tags = tags;
            });
        });
    }

    private formInit(): void {

        if (this.isReview) {
            // load template Data Review
            this.dataService.getDataReview(this.invitationId)
                .subscribe(x => {
                    if (x) {
                        this.templateId = x.templateId;
                        const temp = this.template.filter(y => y.id === x.templateId)[0];
                        if (temp) {
                            this.whatsappForm.setValue({
                                sender: x.senderName,
                                template: {
                                    id: temp.id,
                                    name: temp.name,
                                    content: x.body
                                },
                                whatsappContent: x.body
                            });
                        }
                    }
                });
        } else {
            // load template
            const method = '';
            this.dataService.get(this.invitationId)
                .subscribe(x => {
                    if (x) {
                        let method = '';
                        this.templateId = x.templateId;
                        const temp = this.template.filter(y => y.id === x.templateId)[0];
                        if (temp) {
                            if (temp.isBot) {
                                method = 'bot';
                            } else {
                                method = 'template';
                            }

                            this.whatsappForm.setValue({
                                sender: x.senderName,
                                template: {
                                    id: temp.id,
                                    name: temp.name,
                                    content: x.body
                                },
                                method: method,
                                whatsappContent: x.body
                            });
                        }
                        this.listName = x.listName;
                    }
                });
        }

        const template = this.sectionService.template;
        this.whatsappForm.patchValue({
            sender: template.senderName,
            whatsappContent: template.body
        });
    }

    resetMethod(event): any {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
        dialogRef.componentInstance.confirmHeading = 'Confirm';
        dialogRef.componentInstance.confirmButton = 'Confirm';
        dialogRef.componentInstance.cancelButton = 'Cancel';
        dialogRef.componentInstance.confirmMessage = `Are you sure you want to change invitation type?\nClicking "Confirm" will reset your changes and all content will be lost.`;
        return dialogRef.afterClosed().subscribe(x => {
            const method = this.whatsappForm.controls['method'].value;
            this.isChangingMethod = true;
            if (x) {
                const model = { invitationId: this.invitationId };
                this.dataService.resetWhatsapp(model).subscribe(data => {
                    if (data) {
                        if (event === 'bot') {
                            this.whatsappForm.controls['template'].get('id').setValue(this.template[this.template.findIndex(i => i.isBot === true)].id);
                        } else {
                            this.whatsappForm.controls['template'].get('id').setValue(this.template[this.template.findIndex(i => i.isBot == null)].id);
                        }
                    } else {
                        this.whatsappForm.controls['method'].setValue(event === 'bot' ? 'template' : 'bot');
                    }
                    this.isChangingMethod = false;
                });
            } else {
                this.whatsappForm.controls['method'].setValue(event === 'bot' ? 'template' : 'bot');
                this.isChangingMethod = false;
            }
        }, error => {
            this.whatsappForm.controls['method'].setValue(event === 'bot' ? 'template' : 'bot');
            this.isChangingMethod = false;
        });
    }

    private onFormUpdate(): void {
        this.whatsappForm.valueChanges.subscribe(data => {
            // if (!this.isFormUpdating) {
            this.designerService.unstagedChanges.next(true);
            this.isFormUpdating = true;
            const template = this.sectionService.template;
            if (this.senders && this.senders.length > 0) {
                template.senderName = data.sender;
            }
            template.body = data.whatsappContent;
            this.sectionService.setTemplate(template);
            this.sectionService.fieldsFilled.next(this.whatsappForm.valid);
            this.isFormUpdating = false;
            // }
        });
    }

    ngOnDestroy(): void {
        this.sectionService.template = this.whatsappForm.value;
        this.sectionService.listName = this.listName;
        this.subscriptions.forEach(s => s.unsubscribe());
        this.subscriptions = [];
    }

    patchForm(data): void {
        this.whatsappForm.patchValue(data);
    }


    // update current cursor position in content area
    updateCurrentCursor(event): void {
        this.currentCursor = event;
    }

    saveTemplate(): void {
        // call API for SAve template
        const model = this.whatsappForm.value;
        // call Save APIs
        if (this.whatsappForm.valid) {
            const payload = {
                campaignInvitationId: this.invitationId,
                senderName: model.sender,
                body: model.whatsappContent.content ? model.whatsappContent.content : model.whatsappContent,
                templateId: model.template.id,
                method: model.method
            };
            if (this.isReview) {
                this.dataService.saveTemplateDataReview(payload)
                    .subscribe(id => {
                        this.designerService.templateId = id;
                        this.whatsappForm.markAsUntouched();
                    });
            } else {
                if (this.isLanding) {
                    if (model.method === 'bot') {
                        this.dataService.saveBot(payload).subscribe(id => {
                            this.designerService.templateId = id;
                            this.whatsappForm.markAsUntouched();
                        });
                    } else {
                        this.dataService.saveTemplate(payload)
                            .subscribe(id => {
                                this.designerService.templateId = id;
                                this.whatsappForm.markAsUntouched();
                            });
                    }
                } else {
                    model.method = null;
                    this.dataService.saveTemplate(payload)
                        .subscribe(id => {
                            this.designerService.templateId = id;
                            this.whatsappForm.markAsUntouched();
                        });
                }
            }
        }
    }

    /// Sed test email template
    sendTestWhatsapp(): void {
    }

    canDeactivate(): any {
        if (this.whatsappForm.dirty && this.hasUnstagedChanges) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
            dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
            dialogRef.componentInstance.confirmButton = 'Leave page';
            dialogRef.componentInstance.cancelButton = 'Stay';
            dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
            return dialogRef.afterClosed();
        }
        else {
            return true;
        }
    }

    design(): void{
        this.saveTemplate();
        if (this.isDialog) {
            this.dialog.closeAll();
            setTimeout(() => {
                if (this.whatsappForm.controls['method'].value === 'bot') {
                    this.designerService.triggerBotPage();
                } else if (this.whatsappForm.controls['method'].value === 'template') {
                    this.designerService.triggerLandingPage();
                }
            }, 300);
        } else {
            if (this.whatsappForm.controls['method'].value === 'bot') {
                this.designerService.triggerBotPage();
            } else if (this.whatsappForm.controls['method'].value === 'template') {
                this.designerService.triggerLandingPage();
            }
        }
    }

}
