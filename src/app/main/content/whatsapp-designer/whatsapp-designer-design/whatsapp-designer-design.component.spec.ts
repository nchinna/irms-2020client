import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappDesignerDesignComponent } from './whatsapp-designer-design.component';

describe('WhatsappDesignerDesignComponent', () => {
  let component: WhatsappDesignerDesignComponent;
  let fixture: ComponentFixture<WhatsappDesignerDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappDesignerDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappDesignerDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
