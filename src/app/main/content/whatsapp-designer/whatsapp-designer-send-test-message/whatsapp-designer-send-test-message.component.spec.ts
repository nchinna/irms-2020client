import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappDesignerSendTestMessageComponent } from './whatsapp-designer-send-test-message.component';

describe('WhatsappDesignerSendTestMessageComponent', () => {
  let component: WhatsappDesignerSendTestMessageComponent;
  let fixture: ComponentFixture<WhatsappDesignerSendTestMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappDesignerSendTestMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappDesignerSendTestMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
