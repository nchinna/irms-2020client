import { Injectable } from '@angular/core';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { IResponseModel, IPage } from 'types';
import { WhatsappDesignerModel } from './whatsapp-designer.model';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class WhatsappDesignerDataService implements SectionDataService<WhatsappDesignerModel> {


  private url = `${BASE_URL}api/campaign`;
  private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;

  constructor(private http: HttpClient) { }

  getList(filterParam: IPage): Observable<IResponseModel> {
    throw new Error("Method not implemented.");
  }

  @Toast('Successfully updated')
  saveBot(model: any) {
    return this.http.post<any>(`${this.campaignInvitationUrl}/whatsapp-bot-template`, model);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  resetWhatsapp(model:any): Observable<any>{ // API CHANGE FOR METHOD SWITCHING
    return this.http.post<any>(`${this.campaignInvitationUrl}/delete-whatsapp-template`, model);
  }

  loadTemplates(invitationId: string): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/whatsapp-templates/${invitationId}`);
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/whatsapp`);
  }

  loadTemplatesDataReview(invitationId: string): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}`);
  }

  getDataReview(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/whatsapp/data-review/`);
  }

  @Toast('Successfully updated')
  saveResponse(model: any) {
    return this.http.post<any>(`${this.campaignInvitationUrl}/whatsapp-response-form`, model);
  }

  @Toast('Successfully updated')
  saveTemplate(model: any) {
    return this.http.post<any>(`${this.campaignInvitationUrl}/whatsapp-template`, model);
  }

  @Toast('Successfully updated')
  saveTemplateDataReview(model: any) {
    return this.http.post<any>(`${this.campaignInvitationUrl}/whatsapp-template/data-review/`, model);
  }

  getPlaceholders(filterParam, invitationId): Observable<any> {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/placeholder`, {
      ...filterParam,
      invitationId,
      templateTypeId: 1,
    });
  }

  loadSenders(): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/load-sms-senders`);
  }

  loadSendersDataReview(): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/load-sms-senders/data-review/`);
  }

  sendTestMsg(data): Observable<any> {
    return this.http.post<any>(`${this.campaignInvitationUrl}/send-test-whatsapp`, data);
  }

}
