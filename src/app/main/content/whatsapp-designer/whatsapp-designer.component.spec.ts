import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappDesignerComponent } from './whatsapp-designer.component';

describe('WhatsappDesignerComponent', () => {
  let component: WhatsappDesignerComponent;
  let fixture: ComponentFixture<WhatsappDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
