import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileAccountComponent } from './user-profile-account/user-profile-account.component';
import { UserProfileResetPasswordComponent } from './user-profile-reset-password/user-profile-reset-password.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { RouterModule } from '@angular/router';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { CredentialConfirmComponent } from '../components/shared/credential-confirm/credential-confirm.component';
import { UserProfileResetPasswordVerificationComponent } from './user-profile-reset-password/user-profile-reset-password-verification/user-profile-reset-password-verification.component';
import { UserProfileAccountChangePasswordModalComponent } from './user-profile-account/user-profile-account-change-password-modal/user-profile-account-change-password-modal.component';
import { DialogAnimationService } from '../services/dialog-animation.service';


const routes = [
  {
    path: '',
    component: UserProfileComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: 'account',
        canActivateChild: [AuthGuard],
        component: UserProfileAccountComponent,
      },
      {
        path: 'reset',
        canActivateChild: [AuthGuard],
        component: UserProfileResetPasswordComponent
      }]
    }];

@NgModule({
  declarations: [UserProfileComponent, UserProfileAccountComponent, UserProfileResetPasswordComponent, UserProfileResetPasswordVerificationComponent, UserProfileAccountChangePasswordModalComponent],
  imports: [
    MainModule,
    CommonModule,
    ContentModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  entryComponents: [CredentialConfirmComponent,UserProfileResetPasswordVerificationComponent,UserProfileAccountChangePasswordModalComponent],
  providers: [DialogAnimationService]
})
export class UserProfileModule { }
