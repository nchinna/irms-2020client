import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegexpPattern } from 'app/constants/constants';
import { UserProfileService } from '../user-profile.service';
import { UserProfileDataService } from '../user-profile-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppStateService } from '../../services/app-state.service';
import { CurrentUserService } from '../../services/current-user.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { UserProfileResetPasswordVerificationComponent } from './user-profile-reset-password-verification/user-profile-reset-password-verification.component';

@Component({
  selector: 'irms-user-profile-reset-password',
  templateUrl: './user-profile-reset-password.component.html',
  styleUrls: ['./user-profile-reset-password.component.scss'],
  animations: fuseAnimations
})
export class UserProfileResetPasswordComponent implements OnInit {


  public passChangeForm = this.fb.group({
    oldPassword: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.password))]],
    newPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16), Validators.pattern(new RegExp(RegexpPattern.password))]],
    confirmNewPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16), Validators.pattern(new RegExp(RegexpPattern.password))]]
  }, { validator: this.isMatch('newPassword', 'confirmNewPassword') });
  isReset = false;

  constructor(
    public sectionService: UserProfileService,
    protected dataService: UserProfileDataService,
    protected router: Router,
    protected route: ActivatedRoute,
    private appStateService: AppStateService,
    protected fb: FormBuilder,
    public currentUser: CurrentUserService,
    public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  private isMatch(first, second) {
    return (group: FormGroup): { [key: string]: any } => {
      if ((group.controls[first].value !== group.controls[second].value) &&
        ((group.controls[first].value !== '' && group.controls[first].value !== null) && (group.controls[second].value !== '' && group.controls[second].value !== null))
      ) {
        return {
          isMatch: true
        };
      }
      return null;
    };
  }
  updatePassword() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(UserProfileResetPasswordVerificationComponent, dialogConfig);
    if (this.passChangeForm.controls.isValid) {
      let passwords = {
        oldPassword: this.passChangeForm.controls.oldPassword.value,
        newPassword: this.passChangeForm.controls.newPassword.value,
        confirmNewPassword: this.passChangeForm.controls.confirmNewPassword.value
      }
      //API call through dataservice below
      this.dataService.updatePassword(passwords).subscribe(result => {
        if (result) {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;

          const dialogRef = this.dialog.open(UserProfileResetPasswordVerificationComponent, dialogConfig);
          dialogRef.afterClosed().subscribe(result2 => {
            if (result2) {

            }
          });
        }
      });
    }
  }

  resetToggle() {
    this.isReset = !this.isReset;
  }

}
