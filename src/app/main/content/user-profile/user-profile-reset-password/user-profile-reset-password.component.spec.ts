import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileResetPasswordComponent } from './user-profile-reset-password.component';

describe('UserProfileResetPasswordComponent', () => {
  let component: UserProfileResetPasswordComponent;
  let fixture: ComponentFixture<UserProfileResetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileResetPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
