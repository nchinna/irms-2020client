import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UserProfileModel } from './user-profile.model';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { IPage } from 'app/core/types/types';
import { BASE_URL } from 'app/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class UserProfileDataService {
  
  private url = `${BASE_URL}api/Contact`;

  constructor(private http: HttpClient) { }

  updateUserInfo(data): Observable<any>{
    return this.http.post(`${this.url}/API-Call`,data);
  }

  checkEmailUniqueness(email): Observable<any> {
    return this.http.post(`${this.url}/CheckEmailUniqueness`, email);
  }

  checkPhoneUniqueness(phone): Observable<any> {
    return this.http.post(`${this.url}/CheckPhoneUniqueness`, phone);
  }

  updatePassword(passwords): Observable<any> {
    return this.http.post(`${this.url}/updatePassword`, passwords)
  }
}
