import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

export interface CanExit {
  canDeactivate: () => Observable<any> | Promise<any> | boolean;
  }

@Injectable({
  providedIn: 'root'
})
export class CanExitGuard implements CanDeactivate<CanExit> {
  canDeactivate(component: CanExit): any {
  if (component.canDeactivate) {
  return component.canDeactivate();
  }
  return true;
  }
}