import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class QueryService{
    public queryString: Subject<string> = new BehaviorSubject<string>('');
    public openAdvancedOptionsDialog: any;
    public resetFilters: any;

    constructor(){    }

    resetAllFilters(): void{
        this.queryString.next('');
    }

}

