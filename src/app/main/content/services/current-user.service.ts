import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IUser} from '../../../../types';
import {BASE_URL} from '../../../constants/constants';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CurrentUserService implements OnInit{
  private url = `${BASE_URL}api/user`;

  private observable: Observable<any>;
  private user: IUser;

  public subject: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  getCurrentUser(): Observable<any> {
    if (this.user != null) {
      this.subject.next(this.user);
      return of(this.user);
    } else if (this.observable) {
      return this.observable;
    }else
    {
      this.observable = this.http.get(`${this.url}/myProfile`).pipe(
        tap((user: IUser) => {
          if (user) {
            this.user = user;
            this.observable = null;
            this.subject.next(this.user);
            return of(this.user);
          }
        })).share();
      return this.observable;
    }
  }

  clearCurrentUser() {
    this.user = null;
  }

  getlog() {
    return this.http.get(`${BASE_URL}api/system/log`);
  }
}
