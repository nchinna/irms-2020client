import { Injectable } from '@angular/core';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { SmsDesignerModel } from './sms-designer.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IResponseModel } from 'types';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class SmsDesignerDataService implements SectionDataService<SmsDesignerModel> {
  private url = `${BASE_URL}api/campaign`;
  private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;

  constructor(private http: HttpClient) { }

  getList(filterParam): Observable<any> {
    throw new Error('Method not implemented.');
  }
  create(model: any): Observable<string> {
    throw new Error('Method not implemented.');
  }
  update(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
  delete(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/sms`);
  }

  getDataReview(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/sms/data-review/`);
  }
  
  @Toast('Successfully updated')
  createOrUpdate(model: any): Observable<any> {
    return this.http.post<any>(`${this.campaignInvitationUrl}/sms-template`, model);
  }
  
  @Toast('Successfully updated')
  createOrUpdateDataReview(model: any): Observable<any> {
    return this.http.post<any>(`${this.campaignInvitationUrl}/sms-template/data-review/`, model);
  }

  getPlaceholders(filterParam, invitationId, response): Observable<any> {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/placeholder`, {
      ...filterParam,
      invitationId,
      templateTypeId: 1,
      response: response
    });
  }

  loadSenders(): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/load-sms-senders`);
  }

  sendTestSms(data): Observable<any> {
    return this.http.post<any>(`${this.campaignInvitationUrl}/send-test-sms`, data);
  }
}
