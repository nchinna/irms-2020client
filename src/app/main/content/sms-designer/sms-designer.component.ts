import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SmsDesignerSendTestSmsComponent } from './sms-designer-send-test-sms/sms-designer-send-test-sms.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'irms-sms-designer',
  templateUrl: './sms-designer.component.html',
  styleUrls: ['./sms-designer.component.scss']
})
export class SmsDesignerComponent implements OnInit {
  templateId;
  isReview: boolean;
  constructor(private dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute ) {
      
      this.activatedRoute.params.subscribe(params => {
        this.templateId = params['tempId'];
      });
    }

  ngOnInit() {
  }

  sendTestSms(): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '60vw';
    dialogConfig.maxWidth = '80vw';
    dialogConfig.data = {
      invitationId: this.templateId
    }
    const dialogRef = this.dialog.open(SmsDesignerSendTestSmsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
       
      });
  }
}
