import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SmsDesignerNavigation, SmsDesignerNavigationWithoutRsvp } from './sms-designer-navigation';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';
import { SmsDesignerService } from '../sms-designer.service';
import { SmsDesignerDataService } from '../sms-designer-data.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { Location } from '@angular/common';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { SmsDesignerSendTestSmsComponent } from '../sms-designer-send-test-sms/sms-designer-send-test-sms.component';


@Component({
  selector: 'irms-sms-designer-navbar',
  templateUrl: './sms-designer-navbar.component.html',
  styleUrls: ['./sms-designer-navbar.component.scss']
})
export class SmsDesignerNavbarComponent implements OnInit {
  navigation;
  invitationId: string;
  templateId: string;
  model;
  saveEnabled: boolean;
  @Input() isReview = false;

  constructor(
    private previewService: RsvpFormService,
    private smsService: SmsDesignerService,
    private dataService: SmsDesignerDataService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    protected location: Location,
    protected designerService: SmsDesignerService
  ) {
    if (this.router.url.includes('rsvp-form')) {
      this.navigation = SmsDesignerNavigation;
    } else {
      this.navigation = SmsDesignerNavigationWithoutRsvp;
    }
    if (this.router.url.includes('data-review')){
      this.isReview = true;
    }
    this.route.params.subscribe((params: Params) => {

      this.invitationId = params['tempId'];

      this.designerService.invitationId.next(this.invitationId);

      this.smsService.fieldsFilled.subscribe(x => {
        this.saveEnabled = x;
      });

      if (!this.invitationId) {
        this.invitationId = '00000000-0000-0000-0000-000000000000';
      }
      if (this.isReview){
        this.dataService.getDataReview(this.invitationId).subscribe(m => {
          this.model = m;
          if (m) {
            this.templateId = this.model.id;
  
            const sms = this.smsService.template;
            sms.senderName = m.senderName,
              sms.body = m.body;
            this.smsService.setTemplate(sms);
  
            const backgroundImage = m.backgroundImagePath;
  
            const formTheme = JSON.parse(m.themeJson);
            const themeJson = {
              buttons: formTheme.buttons,
              background: {
                color: formTheme.background.color,
                style: formTheme.background.style,
                image: backgroundImage
              }
            };
  
            this.previewService.rvspFormValue = {
              formContent: {
                welcome: {
                  content: m.welcomeHtml,
                  button: m.proceedButtonText
                },
                rsvp: {
                  content: m.rsvpHtml,
                  acceptButton: m.acceptButtonText,
                  rejectButton: m.rejectButtonText
                },
                accept: {
                  content: m.acceptHtml,
                },
                reject: {
                  content: m.rejectHtml
                }
              },
              formTheme: themeJson,
              formSettings: {
                title: ''
              }
            };
            this.previewService.setRsvpForm(this.previewService.rvspFormValue);
            this.previewService.refreshFormTrigger();
          } else { // form was not loaded
            // this.initDefaultForm();
          }
        });
      } else {
        this.dataService.get(this.invitationId).subscribe(m => {
          this.model = m;
          if (m) {
            this.templateId = this.model.id;
  
            const sms = this.smsService.template;
            sms.senderName = m.senderName,
              sms.body = m.body;
            this.smsService.setTemplate(sms);
  
            const backgroundImage = m.backgroundImagePath;
  
            const formTheme = JSON.parse(m.themeJson);
            const themeJson = {
              buttons: formTheme.buttons,
              background: {
                color: formTheme.background.color,
                style: formTheme.background.style,
                image: backgroundImage
              }
            };
  
            this.previewService.rvspFormValue = {
              formContent: {
                welcome: {
                  content: m.welcomeHtml,
                  button: m.proceedButtonText
                },
                rsvp: {
                  content: m.rsvpHtml,
                  acceptButton: m.acceptButtonText,
                  rejectButton: m.rejectButtonText
                },
                accept: {
                  content: m.acceptHtml,
                },
                reject: {
                  content: m.rejectHtml
                }
              },
              formTheme: themeJson,
              formSettings: {
                title: ''
              }
            };
            this.previewService.setRsvpForm(this.previewService.rvspFormValue);
            this.previewService.refreshFormTrigger();
          } else { // form was not loaded
            // this.initDefaultForm();
          }
        });
      }
    });
  }

  ngOnInit(): void {

  }
  // todo later select different templates for different forms

  
  onClickSend(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '60vw';
    dialogConfig.maxWidth = '80vw';
    dialogConfig.data = {
      invitationId: this.templateId
    };
    const dialogRef = this.dialog.open(SmsDesignerSendTestSmsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {

      });
  }

  onClickSave(): void {
    const rsvp = this.previewService.rvspFormValue;
    const sms = this.smsService.template;
    this.designerService.unstagedChanges.next(false);
    const formTheme = rsvp.formTheme;
    const themeJson = {
      buttons: formTheme.buttons,
      background: {
        color: formTheme.background.color,
        style: formTheme.background.style,
        image: ''// formTheme.background.image
      }
    };

    const payload = {
      id: this.templateId ? this.templateId : '00000000-0000-0000-0000-000000000000',
      campaignInvitationId: this.invitationId,
      senderName: sms.senderName,
      body: sms.body,
      welcomeHtml: rsvp.formContent.welcome.content,
      proceedButtonText: rsvp.formContent.welcome.button,
      rsvpHtml: rsvp.formContent.rsvp.content,
      acceptButtonText: rsvp.formContent.rsvp.acceptButton,
      rejectButtonText: rsvp.formContent.rsvp.rejectButton,
      acceptHtml: rsvp.formContent.accept.content,
      rejectHtml: rsvp.formContent.reject.content,
      themeJson: JSON.stringify(themeJson),
      backgroundImagePath: formTheme.background.image
    };

    const fd = new FormData();
    for (const key in payload) {
      fd.append(key, payload[key]);
    }

    if (this.isReview){
      this.dataService.createOrUpdateDataReview(fd)
      .subscribe(id => {
        this.templateId = id.toString();
        const toasterService = appInjector().get(ToasterService);
        toasterService.pop('success', null, 'Successfully updated');
      });
    } else {
      this.dataService.createOrUpdate(fd)
      .subscribe(id => {
        this.templateId = id.toString();
        const toasterService = appInjector().get(ToasterService);
        toasterService.pop('success', null, 'Successfully updated');
      });
    }
  }

  // Go back
  back(): void {
    this.location.back();
  }
}
