import { Component, OnInit, Input } from '@angular/core';
import { ResponseDataService } from '../response-data.service';
import { BehaviorSubject } from 'rxjs';
import { MediaType } from 'app/constants/constants';

@Component({
  selector: 'irms-whatsapp-response',
  templateUrl: './whatsapp-response.component.html',
  styleUrls: ['./whatsapp-response.component.scss']
})
export class WhatsappResponseComponent implements OnInit {
  @Input() id: string;
  @Input() welcome: string = '';
  @Input() rsvp: string = '';
  @Input() accepted: string = '';
  @Input() rejected: string = '';

  @Input() acceptedButton: string = '';
  @Input() rejectedButton: string = '';
  @Input() proceedButton: string = '';
  @Input() themeObj: any;

  currentTemplate = new BehaviorSubject<string>('');
  state: number = 0;

  constructor(
    private dataService: ResponseDataService,
  ) { }

  ngOnInit() {
    this.currentTemplate.next(this.welcome);
  }

  toWelcome() {
    this.state = 1;
    this.currentTemplate.next(this.rsvp);
  }

  toAccept() {
    this.state = 2;
    this.currentTemplate.next(this.accepted);
    this.dataService.save({
      id: this.id,
      answer: 1,
      mediaType: MediaType.whatsapp
    }).subscribe();
  }

  toReject() {
    this.state = 3;
    this.currentTemplate.next(this.rejected);
    this.dataService.save({
      id: this.id,
      answer: 2,
      mediaType: MediaType.whatsapp
    }).subscribe();
  }
}
