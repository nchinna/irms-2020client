import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResponseComponent } from './response.component';
import { RouterModule } from '@angular/router';
import { ResponseService } from './response.service';
import { SmsResponseComponent } from './sms-response/sms-response.component';
import { EmailResponseComponent } from './email-response/email-response.component';
import { WhatsappResponseComponent } from './whatsapp-response/whatsapp-response.component';
import { MainModule } from 'app/main/main.module';
import { SwipperFormComponentsModule } from '../components/shared/swipper-form-components/swipper-form-components.module';

const routes = [
  {
    path: '',
    component: ResponseComponent,
  },
  {
    path: ':tempId',
    component: ResponseComponent,
  }
];

@NgModule({
  declarations: [
    ResponseComponent,
    SmsResponseComponent,
    EmailResponseComponent,
    WhatsappResponseComponent
  ],
  imports: [
    CommonModule,
    MainModule,
    SwipperFormComponentsModule,
    RouterModule.forChild(routes)
  ],
  providers: [ResponseService],
  exports: [RouterModule]
})
export class ResponseModule { }
