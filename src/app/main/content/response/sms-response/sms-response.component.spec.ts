import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsResponseComponent } from './sms-response.component';

describe('SmsResponseComponent', () => {
  let component: SmsResponseComponent;
  let fixture: ComponentFixture<SmsResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
