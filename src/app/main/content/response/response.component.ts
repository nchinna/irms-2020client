import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ResponseDataService } from './response-data.service';

@Component({
  selector: 'irms-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.scss']
})
export class ResponseComponent implements OnInit {
  constructor(
    protected route: ActivatedRoute,
    protected dataService: ResponseDataService,
    private router: Router
  ) { }

  image = '';
  isLoading = true;
  redirect = false;
  hasError = false;
  responseMediaId: string;
  response: any;
  message = 'Loading...';
  redirectDestination = '';

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.responseMediaId = params['tempId'];
      this.dataService.get(this.responseMediaId).subscribe(response => {
        if (response.error) {
          this.message = response.error;
        } else if (response && response.hasOwnProperty('pendingRfiId') && response.pendingRfiId !== null
        && !(response.mediaType === 0 && response.acceptedOrRejected === 2) // acceptedorReject type 2 is when user arrives from reject link in email
        )  {
          // Redirect to respective RFI if RFI is pending
          const theme = JSON.parse(response.themeObj);
          if (theme && theme.background && theme.background.image) {
            this.image = theme.background.image;
          }
          if (response.backgroundImageUrl) {
            this.image = response.backgroundImageUrl;
          }
          this.redirectDestination = 'to fill some required details.';
          this.isLoading = false;
          this.redirect = true;
          setTimeout(() => { 
            this.router.navigate([`rfi/${response.pendingRfiId}`], { queryParams: { redirect_rsvp: this.responseMediaId } });
           }, 4000);
        } else {
          const theme = JSON.parse(response.themeObj);
          if (theme && theme.background && theme.background.image) {
            this.image = theme.background.image;
          }
          if (response.backgroundImageUrl) {
            this.image = response.backgroundImageUrl;
          }
          
          this.response = response;
          this.isLoading = false;
        }        
      });
    });
  }

}
