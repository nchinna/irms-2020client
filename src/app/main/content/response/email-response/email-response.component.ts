import { Component, OnInit, Input } from '@angular/core';
import { ResponseDataService } from '../response-data.service';
import { MediaType } from 'app/constants/constants';

@Component({
  selector: 'irms-email-response',
  templateUrl: './email-response.component.html',
  styleUrls: ['./email-response.component.scss']
})
export class EmailResponseComponent implements OnInit {
  @Input() id: string;
  @Input() type: number;
  @Input() htmlData: string;
  constructor(
    private dataService: ResponseDataService
  ) { 
  }

  ngOnInit() {
    this.dataService.save({
      id: this.id,
      answer: this.type,
      mediaType: MediaType.Email
    }).subscribe();
  }
}
