import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { RegexpPattern } from 'app/constants/constants';
import { CurrentUserService } from '../../services/current-user.service';
import { EmailDesignerDataService } from '../email-designer-data.service';
import { EmailDesignerService } from '../email-designer.service';

@Component({
  selector: 'irms-email-designer-send-test-email',
  templateUrl: './email-designer-send-test-email.component.html',
  styleUrls: ['./email-designer-send-test-email.component.scss']
})
export class EmailDesignerSendTestEmailComponent implements OnInit {

  public separatorKeysCodes = [ENTER, COMMA, SPACE];
  public emailList = [];
  removable = true;
  testEmailsForm: FormGroup;
  @Input() templateId: any;
  public isloading = false;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<EmailDesignerSendTestEmailComponent>,
              private currentUserService: CurrentUserService,
              public dataService: EmailDesignerDataService,
              public sectionService: EmailDesignerService) { }

  ngOnInit(): void {
    this.testEmailsForm = this.fb.group({
      emails: this.fb.array([]),
    });

    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.emailList.push({ value: user.email, invalid: false });
    });

    // this.emailForm.valueChanges.subscribe(data => {
    //   this.sectionService.setTemplate(data);
    // });

    // this.patchForm(this.sectionService.template);

  }

  addEmail(event): void {
    if (event.value) {
      if (this.validateEmail(event.value)) {
        this.emailList.push({ value: event.value, invalid: false });
      } else {
        this.emailList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllValid();
    if (this.testEmailsForm.hasError('noEmails')) {
      this.testEmailsForm.setErrors({ noEmails: null });
      this.testEmailsForm.updateValueAndValidity();
    }
  }


  removeEmail(data: any): void {
    if (this.emailList.indexOf(data) >= 0) {
      this.emailList.splice(this.emailList.indexOf(data), 1);
    }
    this.checkAllValid();
    if (this.emailList.length === 0) {
      this.testEmailsForm.setErrors({ noEmails: true });
    }
  }

  sendEmails(): void {
    this.isloading = true;
    const testEmails = this.emailList.map(email => email.value);
    const model = {
        id: this.templateId,
        recipients: testEmails
      };
    this.dataService.sendTestEmail(model).subscribe(id => {
        if (id){
          this.dialogRef.close();
        }
      });
  }

  private checkAllValid(): void {
    let invalidFound = false;
    this.emailList.forEach(email => {
      if (email.invalid) {
        invalidFound = true;
        this.testEmailsForm.setErrors({ incorrectEmail: true });
      }
    });
    if (!invalidFound && this.testEmailsForm.hasError('incorrectEmail')) {
      this.testEmailsForm.setErrors({ incorrectEmail: null });
      this.testEmailsForm.updateValueAndValidity();
    }

    if (this.emailList.length > 10) {
      this.testEmailsForm.setErrors({ overflow: true });
    } else if (this.testEmailsForm.hasError('overflow')) {
      this.testEmailsForm.setErrors({ overflow: null });
      this.testEmailsForm.updateValueAndValidity();
    }
  }

  private validateEmail(email): any {
    const pattern = new RegExp(RegexpPattern.email);
    return pattern.test(String(email).toLowerCase());
  }

  /// On dialog close
  close(): void {
    this.dialogRef.close();
  }
}
