import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailDesignerSendTestEmailComponent } from './email-designer-send-test-email.component';

describe('EmailDesignerSendTestEmailComponent', () => {
  let component: EmailDesignerSendTestEmailComponent;
  let fixture: ComponentFixture<EmailDesignerSendTestEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailDesignerSendTestEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailDesignerSendTestEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
