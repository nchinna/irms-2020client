import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from '../../services/can-exit.guard';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EmailDesignerDataService } from '../email-designer-data.service';
import { EmailDesignerService } from '../email-designer.service';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';

@Component({
  selector: 'irms-email-designer-response',
  templateUrl: './email-designer-response.component.html',
  styleUrls: ['./email-designer-response.component.scss']
})
export class EmailDesignerResponseComponent implements OnInit, OnDestroy, CanExit {

  public rsvpForm = this.fb.group(
    {
      formContent: this.fb.group({
        accept: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`, [Validators.required]]
        }),
        reject: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`, [Validators.required]]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['']
      })
    });
  subscriptions: Subscription[] = [];
  invitationId: any;
  templateId: any;

  constructor(private fb: FormBuilder, private dialog: MatDialog,
              protected route: ActivatedRoute,
              protected router: Router,
              public preview: RsvpFormService,
              public dataService: EmailDesignerDataService,
              public sectionService: EmailDesignerService) {
    this.subscriptions.push(this.sectionService.saveObs().subscribe(data => this.saveForm()));

    this.route.params.subscribe((params: Params) => {
      this.invitationId = params['tempId'];
      this.dataService.get(this.invitationId).subscribe(m => {
        if (m) {
          this.templateId = m.id;
          const formValue = this.preview.rsvpForm.value;
          if (m.acceptHtml) {
            formValue.formContent.accept.content = m.acceptHtml;
            this.rsvpForm.controls.formContent['controls'].accept.controls.content.setValue(m.acceptHtml);
          }

          if (m.rejectHtml) {
            formValue.formContent.reject.content = m.rejectHtml;
            this.rsvpForm.controls.formContent['controls'].reject.controls.content.setValue(m.rejectHtml);
          }
          if (m.themeJson) {
            const json = JSON.parse(m.themeJson);
            json.background.image = m.backgroundImagePath;
            formValue.formTheme.background.image = m.backgroundImagePath;
            this.rsvpForm.controls.formTheme.setValue(json);
          }

          this.preview.setRsvpForm(formValue);
        }
      });
    });
  }

  ngOnInit(): void {
  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  saveForm(): void {
    /// Save RSVP response form

    const background = this.sectionService.template.formTheme && this.sectionService.template.formTheme.background;
    const theme = this.rsvpForm.controls.formTheme.value;

    let img = '';
    img = background && background.image;
    if (background && !background.image) {
      img = theme.background.image;
    }
    if (!img) {
      img = '';
    }

    theme.background.image = '';
    if (this.templateId) {
      const model = {
        id: this.templateId,
        acceptHtml: this.rsvpForm.controls.formContent['controls'].accept.controls.content.value,
        rejectHtml: this.rsvpForm.controls.formContent['controls'].reject.controls.content.value,
        themeJson: JSON.stringify(theme),
        formSettings: JSON.stringify(this.rsvpForm.controls.formSettings.value),
        backgroundImage: img
      };

      if (this.sectionService.template.formTheme && this.sectionService.template.formTheme.background.image) {
        model['backgroundImage'] = this.sectionService.template.formTheme.background.image;
      }

      const fd = new FormData();
      for (const key in model) {
        fd.append(key, model[key]);
      }

      this.dataService.updateResponseForm(fd).subscribe(id => {
        this.rsvpForm.markAsPristine();
      });
    }
  }

  canDeactivate(): any {
    if (this.rsvpForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

}
