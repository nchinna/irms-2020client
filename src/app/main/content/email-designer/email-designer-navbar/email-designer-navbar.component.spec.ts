import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailDesignerNavbarComponent } from './email-designer-navbar.component';

describe('EmailDesignerNavbarComponent', () => {
  let component: EmailDesignerNavbarComponent;
  let fixture: ComponentFixture<EmailDesignerNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailDesignerNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailDesignerNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
