import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { fuseAnimations } from 'app/core/animations';
import { EmailDesignerService } from '../email-designer.service';
import { tinyMceKey, Section, RegexpPattern, invitationWorkflowStep } from 'app/constants/constants';
import { Subscription } from 'rxjs';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { EmailDesignerSendTestEmailComponent } from '../email-designer-send-test-email/email-designer-send-test-email.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EmailDesignerDataService } from '../email-designer-data.service';
import { EmailDesignerModel } from '../email-designer.model';
import { SectionViewEditComponent } from '../../components/shared/section/section-view-edit/section-view-edit.component';
import { CanExit } from '../../services/can-exit.guard';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'irms-email-designer-design',
  templateUrl: './email-designer-design.component.html',
  styleUrls: ['./email-designer-design.component.scss'],
  animations: fuseAnimations
})

export class EmailDesignerDesignComponent extends SectionViewEditComponent<EmailDesignerModel> implements OnInit, OnDestroy, CanExit {
  tinymce: any;
  editorKey = tinyMceKey;
  editor: any;
  @Input() invitationId: string;
  @Input() isReview = false;
  @Input() isLanding = true;
  @Input() isDialog = false;
  @Input() step;
  thisState = 'component';
  invitationStep = invitationWorkflowStep;
  // Email Template Form
  public emailForm = this.fb.group({
    subject: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    senderEmail: ['', [Validators.required, Validators.maxLength(500), Validators.pattern(RegexpPattern.email)]],
    body: ['', [Validators.required]],
    plainBody: [''],
    toName: [''],
  });
  // Button style Form
  public rsvpButtonForm = this.fb.group({
    background: ['#ff0000'],
    borderColor: [''],
    borderWidth: [''],
    borderRadius: [''],
  });
  containsTags = false;
  templateId: string;
  tags: any;
  tinyMceConfig = {
    menubar: false,
    statusbar: false,
    branding: false,
    height: 246,
    placeholder: 'Enter the Email Template content here',
    inline: false,
    theme_advanced_toolbar_location: 'bottom',
    plugins: [
      'advlist charmap lists link image imagetools directionality',
      'searchreplace visualblocks visualchars media table paste pagebreak code noneditable fullpage quickbars'
    ],
    // toolbar: 'copy undo redo | link | alignleft aligncenter alignright alignjustify | numlist bullist | image | hr code',
    toolbar: '| formatting | blockquote link image',
    quickbars_selection_toolbar: 'bold italic | styleselect | forecolor | alignleft aligncenter alignright ',
    toolbar_groups: {
      formatting: {
        icon: 'text-color',
        tooltip: 'Formatting',
        items: 'styleselect |bold italic underline strikethrough | forecolor backcolor |  alignleft aligncenter alignright | numlist bullist'
      }
    },
    fullpage_default_doctype: '<!DOCTYPE html>',
    noneditable_noneditable_class: 'mceNonEditable',
    noneditable_editable_class: 'mceEditable',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    paste_data_images: !0,
    importcss_append: !0,
    images_upload_handler(e, t, a) {
      t('data:' + e.blob().type + ';base64,' + e.base64());
    },
  };

  subscriptions: Subscription[] = [];
  tempId = 0;
  rsvpButtonsPresent: boolean;
  isRsvpButtons: boolean;

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              public dataService: EmailDesignerDataService,
              public sectionService: EmailDesignerService,
              private dialog: MatDialog,
              public fb: FormBuilder) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
    if (this.router.url.includes('data-review')) {
      this.isReview = true;
    }
    this.subscriptions.push(this.sectionService.saveObs().subscribe(data => {
      if (this.thisState === this.sectionService.getCurrentComponentState()) {
        this.saveTemplate();
      }
    }));
    this.subscriptions.push(this.sectionService.sendTestObs().subscribe(data => this.sendTestEmail()));
    this.subscriptions.push(this.sectionService.getRefreshTrigger().subscribe(data => {
      if (this.sectionService.template) {
        this.emailForm.setValue(this.sectionService.template);
      }
    }));
  }

  ngOnInit(): void {
    // console.log("ISREVIEW",this.isReview)
    if (this.step === this.invitationStep.Invitation || this.step === this.invitationStep.Pending) {
      this.isLanding = true;
      this.isRsvpButtons = true;
    } else {
      this.isLanding = false;
      this.isRsvpButtons = false;
    }
    this.emailForm.controls.body.valueChanges.pipe(distinctUntilChanged()).subscribe(htmlDoc => {
      if (this.editor) {
        if (this.isRsvpButtons && htmlDoc && htmlDoc.includes('accept-button') && htmlDoc.includes('reject-button')) {
          this.rsvpButtonsPresent = true;
        } else {
          this.rsvpButtonsPresent = false;
          if (this.isRsvpButtons) {
            this.insertRSVPButtons();
          }
        }
        
        if (this.isReview && !htmlDoc.includes('{{rfi_link}}')) {
          this.insertRFILink();
        }
        
        const plainText = this.editor.getContent({ format: 'text' });
        if (plainText.includes('{{')) {
          this.containsTags = true;
        } else {
          this.containsTags = false;
        }
        // to get only body tag content in plain text
        this.emailForm.controls.plainBody.setValue(plainText);
        this.emailForm.get('body').updateValueAndValidity();
      }
    });

    this.emailForm.valueChanges.subscribe(data => {
      this.saveTemplateInService();
    });

    this.patchForm(this.sectionService.template);

    if (!this.isDialog && this.invitationId) {
      this.loadEmailTemplate(this.invitationId);
      const filter = this.sectionService.getFilter();
      this.loadTags(filter);
    }
    else if (this.isDialog) {
      this.thisState = 'dialog';
      this.emailForm.patchValue(this.sectionService.template);
      const filter = this.sectionService.getFilter();
      this.loadTags(filter);
    }
    this.rsvpButtonForm.valueChanges.subscribe(data => {
      if (this.editor && this.emailForm.controls.body.value) {
        // style="line-height: 40px; margin: 10px; color: #fff;
        const bodyCont = this.emailForm.controls.body.value;
        if (bodyCont.includes('<style>')) {
          const newCss = bodyCont.replace(/\/\*buttonsStyleS\*\/(.|\n|\r)+\/\*buttonsStyleE\*\//g,
            `/*buttonsStyleS*/
          line-height: ${data.borderWidth === '' || data.borderWidth == null ? 5 : (2 + (data.borderWidth / 4)) < 5 ? 5 : 2 + (data.borderWidth / 4)};
          margin: 10px;
          padding: 8px 12px;
          text-decoration: none;
          text-align: center;
          background: ${data.background};
          border-style: solid;
          border-width: ${data.borderWidth}px;
          border-color: ${data.borderColor};
          border-radius: ${data.borderRadius}px;
          /*buttonsStyleE*/`
          );
          this.emailForm.controls.body.setValue(newCss);
        } else {
          // line-height: 1.6;
          // line-height: ${2+data.borderWidth/data.borderWidth>2?data.borderWidth-1:1};
          const newCss = bodyCont.replace(/<head>(.|\n|\r)+<\/head>/g,
            `<head>
        <style>
        .rsvp-button {
          /*buttonsStyleS*/
          line-height: ${data.borderWidth === '' || data.borderWidth == null ? 5 : (2 + (data.borderWidth / 4)) < 5 ? 5 : 2 + (data.borderWidth / 4)};
          margin: 10px 10px;
          padding: 8px 12px;
          text-decoration: none;
          text-align: center;
          background: ${data.background};
          border-style: solid;
          border-width: ${data.borderWidth}px;
          border-color: ${data.borderColor};
          border-radius: ${data.borderRadius}px;
          /*buttonsStyleE*/
        }
        </style>
        </head>`
          );
          this.emailForm.controls.body.setValue(newCss);
        }

        // this.editor.iframeElement.contentDocument.getElementsByTagName('style')[0].innerHTML = ;
      }
    });

  }
  insertRSVPButtons(): void {
    if (!this.editor || !this.isRsvpButtons) { return; }
    const body = this.emailForm.controls.body.value;
    if (body && body.includes('<!-- button-wrapper-start -->') && body.includes('<!-- button-wrapper-end -->')) {
      const newDiv = body.replace(/<!-- button-wrapper-start -->(.|\n|\r|)+<!-- button-wrapper-end -->/g,
        `<!-- button-wrapper-start -->
        <div class="mceNonEditable" style="text-align: center; width: 100%;"><a class="rsvp-button accept-button mceEditable" href="{{invitation_accepted_link}}"><span>Accept</span></a> <a class="rsvp-button reject-button mceEditable" href="{{invitation_rejected_link}}"><span>Reject</span></a></div>
        <!-- button-wrapper-end -->`
      );
      this.emailForm.controls.body.setValue(newDiv);
    } else {
      this.editor.insertContent(`<!-- button-wrapper-start -->
      <div class="mceNonEditable" style="text-align: center; width: 100%;"><a class="rsvp-button accept-button mceEditable" href="{{invitation_accepted_link}}"><span>Accept</span></a> <a class="rsvp-button reject-button mceEditable" href="{{invitation_rejected_link}}"><span>Reject</span></a></div>
      <!-- button-wrapper-end -->`);
    }

  }

  insertRFILink(): void {
    if (!this.editor || !this.isReview) { return; }
    const body = this.emailForm.controls.body.value;
    if (body && body.includes('<!-- rfi-wrapper-start -->') && body.includes('<!-- rfi-wrapper-end -->')) {
      const newDiv = body.replace(/<!-- rfi-wrapper-start -->(.|\n|\r|)+<!-- rfi-wrapper-end -->/g,
        `<!-- rfi-wrapper-start -->
        <div class="mceNonEditable" style="text-align: center; width: 100%;"><span class="mceEditable">{{rfi_link}}</span></div>
        <!-- rfi-wrapper-end -->`
      );
      this.emailForm.controls.body.setValue(newDiv);
    } else {
      this.editor.insertContent(`<!-- rfi-wrapper-start -->
      <div class="mceNonEditable" style="text-align: center; width: 100%;"><span class="mceEditable">{{rfi_link}}</span></div>
      <!-- rfi-wrapper-end -->`);
    }

  }

  loadEmailTemplate(invitationId: string): void {

    this.dataService.get(invitationId).subscribe(m => {
      this.model = m;
      if (m) {
        this.templateId = this.model.id;
        this.isDataLoaded = true;
        this.emailForm.controls['toName'].setValue(m.listName);
        this.patchModelInForms();
        this.patchButtonStyles();
      } else {

      }
    });

  }

  patchButtonStyles(): void {
    const body = this.emailForm.controls.body.value;
    if (body.includes('/*buttonsStyleS*/')) {
      const styleFrom = body.indexOf('/*buttonsStyleS*/') + '/*buttonsStyleS*/'.length;
      const styleTo = body.indexOf('/*buttonsStyleE*/');
      const styles = body.substring(styleFrom, styleTo);
      const classPropRegex = /([\w-]*)\s*:\s*([^;]*)/g;
      let match;
      const properties: any = {};
      while (match = classPropRegex.exec(styles)) {
        properties[match[1]] = match[2].trim();
      }
      const toPatch = {
        background: properties.background,
        borderColor: properties['border-color'],
        borderWidth: properties['border-width'].substring(0, properties['border-width'].length - 2),
        borderRadius: properties['border-radius'].substring(0, properties['border-radius'].length - 2),
      };
      this.rsvpButtonForm.patchValue(toPatch);
    } else {
      const newCss = body.replace(/<head>(.|\n|\r)+<\/head>/g,
        `<head>
        <style>
        .rsvp-button {
          /*buttonsStyleS*/
          margin: 10px;
          display:inline-block
          padding: 8px 12px;
          text-decoration: none;
          text-align: center;
          background: #fff;
          border-style: solid;
          border-width: 2px;
          border-color: #000;
          border-radius: 13px;
          /*buttonsStyleE*/
        }
        </style>
        </head>`
      );
      this.emailForm.controls.body.setValue(newCss);
    }

  }

  loadTags(filter): void {
      this.dataService.getPlaceholders(filter, this.invitationId, this.tempId, false).subscribe(result => {
        this.tags = this.addDefaultValues(result);
      });
    
  }

  addDefaultValues(tags): any {

    tags.forEach(tag => {
      if (tag.title === 'Invitation rejected webpage link' || tag.title === 'Invitation accepted webpage link' || tag.title === 'RFI form link') {
        return;
      }
      tag.placeholder = [tag.placeholder.slice(0, tag.placeholder.length - 2), ' | DEFAULT VALUE', tag.placeholder.slice(tag.placeholder.length - 2)].join('');
    });
    return tags;
  }

  ngOnDestroy(): void {
    if (this.isDialog) {
      this.sectionService.setTemplate(this.emailForm.value);
    } else {
      this.emailForm.reset();
      this.sectionService.flushTemplate();
      this.sectionService.setTemplate(this.emailForm.value);
    }

    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  patchForm(data): void {
    if (data) {
      this.emailForm.patchValue(data);
    }
  }

  patchModelInForms(): void {
    this.emailForm.patchValue(this.model);
    if (this.emailForm.controls.plainBody.value.includes('{{')) {
      this.containsTags = true;
    } else {
      this.containsTags = false;
    }
    if (this.isRsvpButtons && this.emailForm.controls.body.value.includes('accept-button') && this.emailForm.controls.body.value.includes('reject-button')) {
      this.rsvpButtonsPresent = true;
    } else {
      this.rsvpButtonsPresent = false;
      if (this.isRsvpButtons) {
        this.insertRSVPButtons();
      }
    }
  }

  landingEmail(): void {
    this.sectionService.triggerLandingPage();
  }

  // on search tag event
  searchTags(event): void {
    const filter = this.sectionService.getFilter();
    filter.searchText = event;
    this.loadTags(filter);
  }

  // adds Tag to the selected content editor
  addTag(link): void {
    if (!this.editor) { return; }
    this.editor.insertContent(link);
  }

  // set current content Editor
  setCurrentEditor(event): void {
    this.editor = event.editor;
  }

  saveTemplateInService(): void {
    this.sectionService.setTemplate(this.emailForm.value);
  }

  saveTemplate(): void {
    this.emailForm.markAsTouched();

    // call Save APIs
    if (this.emailForm.valid) {
      const model = this.sectionService.trimValues(this.emailForm.value);

      if (this.templateId) {
        model.id = this.templateId;
      }

      model.campaignInvitationId = this.invitationId;

      if (!model.plainBody) {
        model.plainBody = '<p></p>';
      }

      // convert object to formdata
      const fd = new FormData();
      for (const key in model) {
        fd.append(key, model[key]);
      }
      this.dataService.createOrUpdate(fd).subscribe(id => {
        // this.sectionService.loading = false;
        this.templateId = id.toString();
        this.emailForm.markAsPristine();
        this.loadEmailTemplate(this.invitationId);
      });
    }
  }

  /// Sed test email template
  sendTestEmail(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '60vw';
    dialogConfig.maxWidth = '80vw';
    const dialogRef = this.dialog.open(EmailDesignerSendTestEmailComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        // call API for test template
        if (this.emailForm.valid) {
          const model = {
            id: this.templateId,
            recipients: data.data
          };
          this.dataService.sendTestEmail(model).subscribe(id => {
          });
        }
      });
  }

  canDeactivate(): any {
    if (this.emailForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }
}
