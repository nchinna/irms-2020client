import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailDesignerService } from './email-designer.service';

@Component({
  selector: 'irms-email-designer',
  templateUrl: './email-designer.component.html',
  styleUrls: ['./email-designer.component.scss']
})
export class EmailDesignerComponent implements OnInit {
  templateId;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    protected designerService: EmailDesignerService) {
    this.activatedRoute.params.subscribe(params => {
      this.templateId = params['tempId'];
    });
  }

  ngOnInit() {
  }
  sendTestEmails() { }

  saveTemplate() { }
}
