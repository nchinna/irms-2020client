import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailDesignerPreviewComponent } from './email-designer-preview.component';

describe('EmailDesignerPreviewComponent', () => {
  let component: EmailDesignerPreviewComponent;
  let fixture: ComponentFixture<EmailDesignerPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailDesignerPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailDesignerPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
