import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormDesignerService {
  private save = new Subject<any>();
  constructor() {
  }

  triggerSave(data: any): void {
    this.save.next(data);
  }

  saveObs(): Observable<any> {
    return this.save.asObservable();
  }
}
