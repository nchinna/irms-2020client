import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EventGuestDataService } from '../event-guest-data.service';
import { EventGuestService } from '../event-guest.service';
import { ImportAnalysisFormService } from './import-analysis-form.service';

@Component({
  selector: 'irms-event-guest-list-analysis',
  templateUrl: './event-guest-list-analysis.component.html',
  styleUrls: ['./event-guest-list-analysis.component.scss']
})
export class EventGuestListAnalysisComponent implements OnInit, OnDestroy {
  isSaved = true;
  subscription: Subscription;
  currentStepIndex = 0;
  stepConfig: any;
  listId: any;
  eventId: any;

  constructor(
    public formService: ImportAnalysisFormService,
    public sectionService: EventGuestService,
    public dataService: EventGuestDataService,
    public route: ActivatedRoute,
    ) { }

  ngOnInit(): void {
    this.subscription = this.formService.getStepperIndex().subscribe(x => {
      this.currentStepIndex = x;
    });
    
    this.route.params.subscribe((params: Params) => {
      this.listId = params['glid'];
      this.eventId = params['id'];
    });
    this.sectionService.eventId = this.eventId;
    const model = {
      listId: this.listId,
      eventId: this.eventId
    };

    this.dataService.getStep(model).subscribe(res => {
      if (res){
        this.sectionService.stepConfig = res.listAnalysisStep;
        this.sectionService.pageConfig = {
          pageSize: res.listAnalysisPageSize ? res.listAnalysisPageSize : 5 ,
          pageNo: res.listAnalysisPageNo ? res.listAnalysisPageNo : 1
        };
        this.currentStepIndex = res.listAnalysisStep > 2 ? 2 : res.listAnalysisStep;
        if (this.sectionService.stepConfig.step === 1){
          this.formService.finalStep = true;
        }
      }
    });
  }
  ngOnDestroy(): void {
    this.formService.setStepperIndex(0);
    this.sectionService.stepConfig = null;
    this.formService.finalStep = false;
    this.formService.headers = [];
    this.subscription.unsubscribe();
    
  }

}
