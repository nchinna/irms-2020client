import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectImportantFieldsComponent } from './collect-important-fields.component';

describe('CollectImportantFieldsComponent', () => {
  let component: CollectImportantFieldsComponent;
  let fixture: ComponentFixture<CollectImportantFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectImportantFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectImportantFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
