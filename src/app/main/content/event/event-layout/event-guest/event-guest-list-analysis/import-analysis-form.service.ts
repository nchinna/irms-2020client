import { Injectable } from '@angular/core';
import { reservedFieldsWithValidation } from 'app/constants/constants';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImportAnalysisFormService {
  selectedFields: any;
  public finalStep = false;
  public linear = true; // stepper linear bool
  selectedGuests: any;
  currentStepIndex = 0;
  private currentStepIndexSub = new Subject<any>();
  headers = [];
  reservedFieldsWithValidations: any = reservedFieldsWithValidation;
  getStepperIndex(): Observable<any> {
    return this.currentStepIndexSub.asObservable();
  }

  setStepperIndex(data: any): void {
    this.currentStepIndex = data;
    this.currentStepIndexSub.next(data);
  }

  setSelectedFields(data: any): void {
    
    this.selectedFields = data;
  }
  getSelectedFields(): any {
    return this.selectedFields;
  }
  
  setSelectedGuests(data): void {
    this.selectedGuests = data;
  }
  getSelectedGuests(): any {
    return this.selectedGuests;
  }
  

  constructor() {}
}
