import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportAnalysisComponent } from './import-analysis.component';

describe('ImportAnalysisComponent', () => {
  let component: ImportAnalysisComponent;
  let fixture: ComponentFixture<ImportAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
