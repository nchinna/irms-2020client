import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestDetailsComponent } from './event-guest-details.component';

describe('EventGuestDetailsComponent', () => {
  let component: EventGuestDetailsComponent;
  let fixture: ComponentFixture<EventGuestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
