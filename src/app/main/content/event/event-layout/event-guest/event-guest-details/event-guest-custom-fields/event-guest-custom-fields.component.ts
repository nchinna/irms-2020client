import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { fuseAnimations } from '@fuse/animations';
import { RegexpPattern } from 'app/constants/constants';
import { DateTimeAdapter } from 'ng-pick-datetime';

@Component({
  selector: 'irms-event-guest-custom-fields',
  templateUrl: './event-guest-custom-fields.component.html',
  styleUrls: ['./event-guest-custom-fields.component.scss'],
  animations: fuseAnimations
})
export class EventGuestCustomFieldsComponent implements OnInit {
  public isLocked = true;
  public customFields = new FormArray([]);
  guestId;
  listId;
  model: {
    fields: []
  };
  filter;

  constructor(protected fb: FormBuilder,
              public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              protected route: ActivatedRoute,
              private appStateService: AppStateService,
              dateTimeAdapter: DateTimeAdapter<any>) {
    dateTimeAdapter.setLocale('en-GB');
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];
      this.listId = params['glid'];
      this.filter = {
        guestId: params['gid']
      };
      this.loadFields();
    });
  }

  updateGuestInfo(): void {
    // campaign id param
    this.dataService.getEventGuestBasicInfo(this.listId, this.guestId).subscribe(data => {
      this.sectionService.setGuestInfo(data);
      this.sectionService.setSelectedGuestName(data.fullName);
    });
  }

  loadFields(): void {
    this.dataService.getGuestCustomFields(this.filter).subscribe(data => {
      this.model = data;
      this.addFields(data);
    });
  }

  addFields(data): void {
    this.customFields.clearValidators();
    data.fields.forEach(element => {
      const validators = [];
      switch (element.customFieldType) {
        default:
        case 0:  // Text
          const textFC = new FormControl(element.value);
          if (element.minValue) {
            validators.push(Validators.minLength(element.minValue));
          }
          if (element.maxValue) {
            validators.push(Validators.maxLength(element.maxValue));
          }
          // this.customFields.push(new FormControl(element.value ? element.value : '', [
          //   element.minValue ? Validators.minLength(element.minValue) : null,
          //   element.maxValue ? Validators.minLength(element.maxValue) : null,
          // ]))
          break;

        case 1: // Number
          if (element.minValue) {
            validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
            validators.push(this.minDigitsLength(element.minValue));
          }
          if (element.maxValue) {
            validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
            validators.push(this.maxDigitsLength(element.maxValue));
          }
          // this.customFields.push(new FormControl(element.value ? element.value : '', numberValidators));
          break;

        case 2: // Date

            element.value = element.value ? new Date(element.value) : ''; // .length != 10? element.value: element.value.toISOString()) : '';
            element.isDate = true;
            if (element.minValue) {
            validators.push(this.minDate(element.minValue));
          }
            if (element.maxValue) {
            validators.push(this.maxDate(element.maxValue));
          }
            validators.push(Validators.pattern(new RegExp(RegexpPattern.date)));

          // this.customFields.push(new FormControl(element.value ? element.value : '', dateValidators));

            break;
      }
      const fc = new FormControl(element.value ? element.value : '');
      fc.clearValidators();
      fc.setValidators(validators);
      this.customFields.push(fc);
    });
  }

  toggleLock(): void {
    
    this.customFields = new FormArray([]);
    this.addFields(this.model);
    this.isLocked = !this.isLocked;
  }

  update(): void {
    const formData = this.model as any;
    formData.fields.forEach((field, i) => {
      field.value = this.customFields.controls[i].value;
    });
    // call update API 
    this.dataService.updateGuestCustomFields(this.guestId, formData)
      .subscribe(() => {
        this.loadFields();
        this.toggleLock();
        this.updateGuestInfo();
      });
  }

  private minDigitsLength(size: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      if (control.value == null) {
        return null;
      }

      const number = control.value;
      if (number === '' || !number || number == null) {
        return null;
      }
      if (number < size) {
        return { minDigits: true };
      }
      return null;
    };
  }

  private maxDigitsLength(size: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      if (control.value == null) {
        return null;
      }

      const number = control.value;
      if (number === '' || !number || number == null) {
        return null;
      }

      if (number > size) {
        return { maxDigits: true };
      }
      return null;
    };
  }

  private minDate(date: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      if (control.value == null) {
        return null;
      }

      const value = Date.parse(control.value);
      if (date > value) {
        return { minDate: true };
      }

      return null;
    };
  }

  private maxDate(date: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      if (control.value == null) {
        return null;
      }

      const value = Date.parse(control.value);
      if (date < value) {
        return { maxDate: true };
      }

      return null;
    };
  }
}
