import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subscription, Subject } from 'rxjs';
import { EventGuestService } from '../event-guest.service';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';
import { EventGuestDataService } from '../event-guest-data.service';

@Component({
  selector: 'irms-event-guest-details',
  templateUrl: './event-guest-details.component.html',
  styleUrls: ['./event-guest-details.component.scss'],
  animations: fuseAnimations
})
export class EventGuestDetailsComponent implements OnInit, OnDestroy {
  guestName = 'Guest Details';
  contactListId;
  guestId;
  guest;
  public tabGroup = [
    {
      label: 'Personal Information',
      link: './personal-info'
    },
    {
      label: 'Organization Information',
      link: './organization-info'
    },
    {
      label: 'Event Admission',
      link: './event-admission'
    },
    {
      label: 'Custom Fields',
      link: './custom-fields'
    },
    {
      label: 'Associated Lists',
      link: './associated-lists'
    }
  ];

  subscription: Subscription;
  private unsubscribeAll: Subject<any>;
  constructor(protected  service: EventGuestService,  protected route: ActivatedRoute,
              protected dataService: EventGuestDataService) {
                this.unsubscribeAll = new Subject();
               }

  ngOnInit(): void {
    this.service.getGuestInfo()
    .subscribe(guest => {
      this.guest = guest;
    })

    // set page title as campaign name
    this.subscription = this.service.getSelectedGuestName().pipe(takeUntil(this.unsubscribeAll)).subscribe(x => {
      this.guestName = x;
    });
    
    // campaign id param
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];
      this.contactListId = params['glid'];
      this.dataService.getEventGuestBasicInfo(this.contactListId, this.guestId).subscribe(data => {
        this.service.setGuestInfo(data)
        this.service.setSelectedGuestName(data.fullName);
      });
    });
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    this.service.resetFilter();
  }

}
