import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestCustomFieldsComponent } from './event-guest-custom-fields.component';

describe('EventGuestCustomFieldsComponent', () => {
  let component: EventGuestCustomFieldsComponent;
  let fixture: ComponentFixture<EventGuestCustomFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestCustomFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestCustomFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
