import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { EventGuestDataService } from '../../event-guest-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { transparentSource } from 'app/constants/constants';
import { EventGuestService } from '../../event-guest.service';

@Component({
  selector: 'irms-event-guest-event-admission',
  templateUrl: './event-guest-event-admission.component.html',
  styleUrls: ['./event-guest-event-admission.component.scss'],
  animations: fuseAnimations
})
export class EventGuestEventAdmissionComponent implements OnInit {
  public eventAdmissionForm = this.fb.group({
    status: '',
    seat: '',
    parking: '',
    badgeUrl: ''
  });
  statuses = ['Accepted', 'Rejected'];
  transparentSource = transparentSource;
  guestId: any;
  model;
  filter;
  editInfoMode = false;
  constructor(protected fb: FormBuilder, 
    protected dataService: EventGuestDataService, 
    protected route: ActivatedRoute,
    protected service: EventGuestService
    ) { }

  ngOnInit(): void {
    // campaign id param
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];
      this.filter = {
        eventId: params['id'],
        guestId: params['gid'],
        listId: params['glid']
      };
      this.loadAdmissionInfo();
    });
  }

  updateGuestInfo() {
    // campaign id param
    this.dataService.getEventGuestBasicInfo(this.filter.listId, this.filter.guestId).subscribe(data => {
      this.service.setGuestInfo(data)
      this.service.setSelectedGuestName(data.fullName);
    });    
  }

  loadAdmissionInfo() {
    this.dataService.getEventGuestAdmissionInfo(this.filter).subscribe(data => {
      this.model = data;
      this.eventAdmissionForm.patchValue(data);
    });
  }

  doPathUrl(path): any {
    return `url(${path})`;
  }
  /// Handle Edit Info Tab 
  public editInfo(): void {
    this.eventAdmissionForm.patchValue(this.model);
    this.editInfoMode = true;
  }

  public viewGuestInfo(): void {
    this.editInfoMode = false;
  }

  // Update Guest organization
  updateGuest(): void {
    const formData = this.eventAdmissionForm.value;
    // Call API to update guest organization
    const request = {
      ...formData,
      ...this.filter
    }

    this.dataService.updateEventGuestAdmissionInfo(request)
    .subscribe(x => {
      this.loadAdmissionInfo();
      this.viewGuestInfo();
      this.updateGuestInfo();
    })
    // after call    
  }
  saveBadge(): void {

  }

}
