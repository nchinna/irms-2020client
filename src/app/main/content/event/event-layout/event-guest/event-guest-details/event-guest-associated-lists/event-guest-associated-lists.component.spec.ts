import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestAssociatedListsComponent } from './event-guest-associated-lists.component';

describe('EventGuestAssociatedListsComponent', () => {
  let component: EventGuestAssociatedListsComponent;
  let fixture: ComponentFixture<EventGuestAssociatedListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestAssociatedListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestAssociatedListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
