import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectGuestComponent } from './select-guest/select-guest.component';

@Injectable({
  providedIn: 'root'
})
export class EventGuestImportFormService {
  

  public selectGuestFormSource: BehaviorSubject<FormGroup> = new BehaviorSubject(null);
  selectGuestForm: Observable<FormGroup> = this.selectGuestFormSource.asObservable();

  public selectFieldsFormSource: Subject<FormGroup> = new Subject();
  selectFieldsForm: Observable<FormGroup> = this.selectFieldsFormSource.asObservable();

  private selectedFields = [];
  private selectedGuests = [];
  public selectedGuestList;
  finalStep: boolean;
  public currentStepIndex = 0;
  public guestList = [];

  public importAnalysisFormSource: Subject<FormGroup> = new Subject();
  importAnalysisForm: Observable<FormGroup> = this.selectFieldsFormSource.asObservable();

  guestImportForm: FormGroup = this._formBuilder.group({
    list: ['existing', [Validators.required]],
    listName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
    listId: [''],
    selectedGuests: ['', [Validators.required]],
    selectedReservedFields: [[]],
  });
  guestListFiltered: any[];
  toBeGuestsList = [];


  setSelectedFields(data: any): void {
    this.selectedFields = data;
  }
  getSelectedFields(): any {
    return this.selectedFields;
  }

  setSelectedGuests(data): void {
    this.selectedGuests = data;
  }
  getSelectedGuests(): any {
    return this.selectedGuests;
  }

  add(item: any) {
   // this.selectGuestComponent.guestList.push(item);
    this.guestList.push(item);
  }

  

  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.selectGuestForm.subscribe(form => {
      if (form) {
        form.valueChanges.subscribe(val => {
          this.guestImportForm.value.selectedGuests = val.selectedGuests;
          this.guestImportForm.value.list = val.list;
          this.guestImportForm.value.listName = val.listName;
          this.guestImportForm.value.listId = val.listId;
          this.guestImportForm.value.selectedReservedFields = val.selectedReservedFields;
          this.guestImportForm.value.importAnalysisForm = val.importAnalysisForm;
        })
      }
    });

    this.selectFieldsForm.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.guestImportForm.value.selectedReservedFields = val.selectedReservedFields;
      })
    );

    this.importAnalysisForm.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.guestImportForm.value.selectedReservedFields = val.selectedReservedFields;
      })
    );
  }

  stepReady(form: FormGroup, part): void {
    switch (part) {
      case 'selectGuest': {
        this.selectGuestFormSource.next(form);
        break;
      }
      case 'selectList': {
        this.selectGuestFormSource.next(form);
        break;
      }
      case 'importantFields': {
        this.selectFieldsFormSource.next(form);
        break;
      }
      case 'importAnalysis': {
        this.importAnalysisFormSource.next(form);
        break;
      }
    }
  }
}
