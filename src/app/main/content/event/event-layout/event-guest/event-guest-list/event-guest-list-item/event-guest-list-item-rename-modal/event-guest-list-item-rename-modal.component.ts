import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'irms-event-guest-list-item-rename-modal',
  templateUrl: './event-guest-list-item-rename-modal.component.html',
  styleUrls: ['./event-guest-list-item-rename-modal.component.scss']
})
export class EventGuestListItemRenameModalComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EventGuestListItemRenameModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.form = this.formBuilder.group({
      name: [data.name, [Validators.required, Validators.required, Validators.minLength(2), Validators.maxLength(100)]]
    })
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

  confirm() {
    this.dialogRef.close({
      name: this.form.controls['name'].value
    });
  }
}
