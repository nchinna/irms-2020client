import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'irms-event-guest-list-create',
  templateUrl: './event-guest-list-create.component.html',
  styleUrls: ['./event-guest-list-create.component.scss']
})
export class EventGuestListCreateComponent implements OnInit {
  public newGuestListForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]]
    });
  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<EventGuestListCreateComponent>) { }

  ngOnInit() {
  }
  create(): void {
    this.dialogRef.close(this.newGuestListForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

}
