import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventCampaignModel } from '../event-campaign.model';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { Validators, FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { EventCampaignEditService } from '../event-campaign-edit/event-campaign-edit.service';
import { CampaignConfigurationsService } from '../event-campaign-edit/campaign-configurations/campaign-configurations.service';
import { CampaignConfigurationsDataService } from '../event-campaign-edit/campaign-configurations/campaign-configurations-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Section, sideDialogConfig, MediaType, timePeriodUnits } from 'app/constants/constants';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CampaignContactImportDialogComponent } from '../campaign-contact-import-dialog/campaign-contact-import-dialog.component';
import { CampaignPreferredMediaDialogComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/campaign-preferred-media-dialog.component';

@Component({
  selector: 'irms-event-campaign-whatsapp-opt-in',
  templateUrl: './event-campaign-whatsapp-opt-in.component.html',
  styleUrls: ['./event-campaign-whatsapp-opt-in.component.scss']
})
export class EventCampaignWhatsappOptInComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit, OnDestroy, CanExit {

  @Output() public campaignName: EventEmitter<string> = new EventEmitter();

  public configForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    entryCriteriaTypeId: ['', [Validators.required]],
    guestListId: ['', [Validators.required]],
    // entryAdmin: ['', [Validators.required]],
    isInstant: ['', [Validators.required]],
    delay: ['' , [Validators.required]],
    exitCriteriaType: ['', [Validators.required]],
    messages: this.fb.array([])
  });
  min = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 23, 59); // minimum date that can be used for expiration date

  
  messages = [{
    id: 'b9a371d8-72d4-4a27-8d19-be2df5607b93',
    isInstant: false,
    interval: 0,
    title: 'Message 1',
    intervalType: null,
    emailSubject: null,
    emailSender: null,
    smsSender: null,
    whatsappSender: null,
    formCreated: false,
    formBackgroundPath: null,
    smsImage: null,
    emailImage: null,
    whatsappImage: null,
    startDate: '2020-08-19T08:13:06.990Z',
  }];
  mediaType = MediaType;
  guestLists = [];
  reachability: any;
  reachabilityLoading = false;
  prefferedMediaStats: any;
  criteriaTypeList = [];
  campaignId: string;
  subscription: Subscription;
  eventId: any;
  timePeriodUnits = timePeriodUnits;

  constructor(protected fb: FormBuilder,
              public animDialog: DialogAnimationService,
              public service: EventCampaignEditService,
              public sectionService: CampaignConfigurationsService,
              protected dataService: CampaignConfigurationsDataService,
              protected router: Router,
              protected route: ActivatedRoute,
              private dialog: MatDialog) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }

  ngOnInit(): void {
    // save form data form global save
    this.subscription = this.service.getCampaignSave().subscribe(() => {
      this.configForm.markAllAsTouched();
      this.update();
    });

    this.configForm.get('guestListId').valueChanges
      .subscribe(x => {
        this.prefferedMediaStats = void 0;
      });

    this.route.params.subscribe((params: Params) => {
      this.loadLookups(params['id']);
      this.eventId = params['id'];
      this.campaignId = params['cid'];
      this.dataService.get(params['cid']).subscribe(m => {
        this.model = m;
        if (m) {
          this.isDataLoaded = true;
          this.patchModelInForms();
          this.loadPrefferedMediaStats();
        }
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // load drop downs
  loadLookups(eventId) {
    this.dataService.getCampaignCriteriaTypes().subscribe(result => this.criteriaTypeList = result);
    this.dataService.getguestLists(eventId).subscribe(result => this.guestLists = result);
  }

  // load preffered media stats
  loadPrefferedMediaStats() {
    if (this.configForm.controls.guestListId.value) {
      this.dataService.loadPrefferedMediaStats(this.campaignId, this.configForm.controls.guestListId.value).subscribe(result => this.prefferedMediaStats = result);
    }
  }

  public patchModelInForms() {
    // emit campaign name
    this.service.setCampaignName(this.model.name);

    this.configForm.patchValue(this.model);
    this.configForm.controls.exitCriteriaType.setValue(this.model['exitCriteriaType'] ? this.model['exitCriteriaType'] : 1);
  }

  /// check reachability
  checkReachability() {
    if (this.configForm.controls.guestListId.value) {
      this.reachabilityLoading = true;
      this.dataService.checkReachability(this.configForm.controls.guestListId.value).subscribe(result => {
        this.reachabilityLoading = false;
        this.reachability = result;
        this.preferredMediaSelect(true);
      }, () => {
        this.reachabilityLoading = false;
      });
    }
  }

  /// update 
  update(next: boolean = false) {
    if (this.configForm.valid) {
      const model = this.sectionService.trimValues(this.configForm.value);
      model.id = this.campaignId;
      this.sectionService.loading = true;
      this.dataService.update(model).subscribe(() => {
        this.sectionService.loading = false;
        this.service.setCampaignName(model.name);
        this.configForm.markAsPristine();
        if (next) {
          this.router.navigate([`../invitation`], { relativeTo: this.route });
        }
      }, () => {
        this.sectionService.loading = false;
      });
    }
  }

  /// Handle Preferred media select
  preferredMediaSelect(needCheck: boolean): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '80vw';
    dialogConfig.data = { listId: this.configForm.controls.guestListId.value, campaignId: this.campaignId, reachability: this.reachability, needCheck: needCheck };

    const dialogRef = this.dialog.open(CampaignPreferredMediaDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      () => {
        this.loadPrefferedMediaStats();
      });
  }
  canDeactivate(): any {
    if (this.configForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  CreateList() {
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '70vw';
    dialogConfig.data = { eventId: this.eventId };
    const dialogRef = this.animDialog.open(CampaignContactImportDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      this.dataService.getguestLists(this.eventId).subscribe(result => this.guestLists = result);
      if (this.sectionService.listId != null || !this.sectionService.listId) {
        this.configForm.controls.guestListId.setValue(this.sectionService.listId);
        this.configForm.controls.entryCriteriaTypeId.setValue(1);
      }
    });
  }

  get msgArray(){ return this.configForm.controls.messages as FormArray; }
  
  addReminder(){
    this.dataService.addReminder({id: this.campaignId}).subscribe(data => {
      if (data){
        this.msgArray.controls.push(this.fb.group(
          data
        ));
        
      }
    });
  }

}
