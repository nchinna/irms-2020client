import { Component, OnInit, Input } from '@angular/core';
import { campaignStatus } from 'app/constants/constants';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'irms-event-campaign-list-item',
  templateUrl: './event-campaign-list-item.component.html',
  styleUrls: ['./event-campaign-list-item.component.scss']
})
export class EventCampaignListItemComponent implements OnInit {
  @Input() data;
  status = campaignStatus;

  invitations = [];

  formGroup = this.fb.group({
    selected: 'all',
    text: '',
    id: '',
    delivery: this.fb.group({
      email: 0,
      sms: 0,
      whatsapp: 0
    }),
    accepted: this.fb.group({
      email: 0,
      sms: 0,
      whatsapp: 0
    }),
    rejected: this.fb.group({
      email: 0,
      sms: 0,
      whatsapp: 0
    }),
    total: this.fb.group({
      email: 0,
      sms: 0,
      whatsapp: 0
    }),
    pending: this.fb.group({
      emailPending: 0,
      emailOpened: 0,
      whatsAppRead: 0,
      whatsappPending: 0,
    })
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    if (this.data.invitationData) {
      const stats: any[] = JSON.parse(this.data.invitationData);

      stats.forEach(stat => {
        this.invitations.push({
          selected: stat.CampaignInvitationId,
          id: stat.CampaignInvitationId,
          text: stat.Name ? stat.Name : 'Invitation',
          delivery: {
            email: stat.EmailDeliveredSend,
            sms: stat.SmsDeliveredSend,
            whatsapp: stat.WhatsappDeliveredSend
          },
          accepted: {
            email: stat.EmailAccepted,
            sms: stat.SmsAccepted,
            whatsapp: stat.WhatsappAccepted
          },
          rejected: {
            email: stat.EmailRejected,
            sms: stat.SmsRejected,
            whatsapp: stat.WhatsappRejected
          },
          total: {
            email: stat.EmailDeliveredTotal,
            sms: stat.SmsDeliveredTotal,
            whatsapp: stat.WhatsappDeliveredTotal
          },
          pending: {
            emailPending: stat.EmailPending,
            emailOpened: stat.EmailOpened,
            whatsappPending: stat.WhatsappPending,
            whatsAppRead: stat.WhatsAppRead
          }
        });
      });

      if (this.invitations && this.invitations.length > 0) {
        this.formGroup.setValue(this.invitations[0]);
      }
    }
  }

  selectInvitation(event, invitation) {
    this.formGroup.setValue(this.invitations.filter(x => x.id === event.value)[0]);
  }

  manageRouterLink(data): any[] {
    if (data.status === this.status.Live) {
      return ['../' + data.id + '/view/config'];
    } else if (data.status === this.status.Draft) {
      return ['../' + data.id + '/edit/config'];
    }
  }
}
