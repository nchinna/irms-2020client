import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptedMessageConfigComponent } from './accepted-message-config.component';

describe('AcceptedMessageConfigComponent', () => {
  let component: AcceptedMessageConfigComponent;
  let fixture: ComponentFixture<AcceptedMessageConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptedMessageConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptedMessageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
