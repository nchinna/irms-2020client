import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingReminderConfigComponent } from './pending-reminder-config.component';

describe('PendingReminderConfigComponent', () => {
  let component: PendingReminderConfigComponent;
  let fixture: ComponentFixture<PendingReminderConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingReminderConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingReminderConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
