import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignCreateComponent } from './event-campaign-create.component';

describe('EventCampaignCreateComponent', () => {
  let component: EventCampaignCreateComponent;
  let fixture: ComponentFixture<EventCampaignCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
