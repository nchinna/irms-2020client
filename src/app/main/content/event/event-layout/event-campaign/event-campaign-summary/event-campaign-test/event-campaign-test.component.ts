import { Component, OnInit, Inject } from '@angular/core';
import { RegexpPattern, ForbiddenPhoneCodes } from 'app/constants/constants';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { EventCampaignDataService } from '../../event-campaign-data.service';
import { ActivatedRoute } from '@angular/router';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';
import { CurrentUserService } from 'app/main/content/services/current-user.service';

@Component({
  selector: 'irms-event-campaign-test',
  templateUrl: './event-campaign-test.component.html',
  styleUrls: ['./event-campaign-test.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignTestComponent implements OnInit {

  public separatorKeysCodes = [ENTER, COMMA, SPACE];
  public emailList = [];
  public phoneNumbersList = [];
  removable = true;
  testCampaignForm: FormGroup;

  isSending = true;

  public isSelected = {
    email: false,
    sms: false,
    whatsapp: false
  }

  whatsappList = [];
  campaignId: string;

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<EventCampaignTestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataService: EventCampaignDataService,
    protected route: ActivatedRoute,
    private currentUserService: CurrentUserService) { }
  forbiddenCodes = ForbiddenPhoneCodes;
  ngOnInit(): void {

    this.campaignId = this.data.campaignId;

    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.emailList.push({ value: user.email, invalid: false });
      this.phoneNumbersList.push({ value: user.phoneNo, invalid: false });
      this.whatsappList.push({ value: user.phoneNo, invalid: false });
    });

    this.testCampaignForm = this.fb.group({
      // emails: this.fb.array([]),
      // phoneNumbers: this.fb.array([]),
      // whatsapp: this.fb.array([])
    });
  }

  selectEmail() {
    this.testCampaignForm.addControl("emails", new FormArray([]));
    this.isSelected.email = true;
    this.testCampaignForm.updateValueAndValidity();
  }

  unselectEmail() {
    this.testCampaignForm.removeControl("emails");
    this.isSelected.email = false;
    this.testCampaignForm.updateValueAndValidity();
  }

  selectSms() {
    this.testCampaignForm.addControl("phoneNumbers", new FormArray([]));
    this.isSelected.sms = true;
    this.testCampaignForm.updateValueAndValidity();
  }

  unselectSms() {
    this.testCampaignForm.removeControl("phoneNumbers");
    this.isSelected.sms = false;
    this.testCampaignForm.updateValueAndValidity();
  }

  selectWhatsapp() {
    this.testCampaignForm.addControl("whatsapp", new FormArray([]));
    this.isSelected.whatsapp = true;
    this.testCampaignForm.updateValueAndValidity();
  }

  unselectWhatsapp() {
    this.testCampaignForm.removeControl("whatsapp");
    this.isSelected.whatsapp = false;
    this.testCampaignForm.updateValueAndValidity();
  }

  addEmail(event): void {
    if (event.value) {
      if (this.validateEmail(event.value)) {
        this.emailList.push({ value: event.value, invalid: false });
      } else {
        this.emailList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllEmailValid();
    if (this.testCampaignForm.hasError('noEmails')) {
      this.testCampaignForm.setErrors({ noEmails: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }


  removeEmail(data: any): void {
    if (this.emailList.indexOf(data) >= 0) {
      this.emailList.splice(this.emailList.indexOf(data), 1);
    }
    this.checkAllEmailValid();
    if (this.emailList.length === 0) {
      this.testCampaignForm.setErrors({ noEmails: true });
    }
  }

  private checkAllEmailValid(): void {
    let invalidFound = false;
    this.emailList.forEach(email => {
      if (email.invalid) {
        invalidFound = true;
        this.testCampaignForm.setErrors({ incorrectEmail: true });
      }
    });
    if (!invalidFound && this.testCampaignForm.hasError('incorrectEmail')) {
      this.testCampaignForm.setErrors({ incorrectEmail: null });
      this.testCampaignForm.updateValueAndValidity();
    }

    if (this.emailList.length > 10) {
      this.testCampaignForm.setErrors({ overflowEmail: true });
    } else if (this.testCampaignForm.hasError('overflowEmail')) {
      this.testCampaignForm.setErrors({ overflowEmail: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }

  private validateEmail(email): any {
    const pattern = new RegExp(RegexpPattern.email);
    return pattern.test(String(email).toLowerCase());
  }

  addPhoneNumber(event): void {
    if (event.value) {
      if (this.validatePhoneNumber(event.value)) {
        this.phoneNumbersList.push({ value: event.value, invalid: false });
      } else {
        this.phoneNumbersList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllPhoneValid();
    if (this.testCampaignForm.hasError('noPhoneNumbers')) {
      this.testCampaignForm.setErrors({ noPhoneNumbers: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }

  addWhatsApp(event): void {
    if (event.value) {
      if (this.validatePhoneNumber(event.value)) {
        this.whatsappList.push({ value: event.value, invalid: false });
      } else {
        this.whatsappList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllWhatsappValid();
    if (this.testCampaignForm.hasError('noWhatsApp')) {
      this.testCampaignForm.setErrors({ noWhatsApp: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }

  removeWhatsApp(data: any): void {
    if (this.whatsappList.indexOf(data) >= 0) {
      this.whatsappList.splice(this.whatsappList.indexOf(data), 1);
    }
    this.checkAllWhatsappValid();
    if (this.whatsappList.length === 0) {
      this.testCampaignForm.setErrors({ noWhatsApp: true });
    }
  }


  removePhoneNumber(data: any): void {
    if (this.phoneNumbersList.indexOf(data) >= 0) {
      this.phoneNumbersList.splice(this.phoneNumbersList.indexOf(data), 1);
    }
    this.checkAllPhoneValid();
    if (this.phoneNumbersList.length === 0) {
      this.testCampaignForm.setErrors({ noPhoneNumbers: true });
    }
  }

  private checkAllPhoneValid(): void {
    let invalidFound = false;
    this.phoneNumbersList.forEach(phoneNumbers => {
      if (phoneNumbers.invalid) {
        invalidFound = true;
        this.testCampaignForm.setErrors({ incorrectPhoneNumber: true });
      }
    });
    if (!invalidFound && this.testCampaignForm.hasError('incorrectPhoneNumber')) {
      this.testCampaignForm.setErrors({ incorrectPhoneNumber: null });
      this.testCampaignForm.updateValueAndValidity();
    }

    if (this.phoneNumbersList.length > 10) {
      this.testCampaignForm.setErrors({ overflowPhoneNumber: true });
    } else if (this.testCampaignForm.hasError('overflowPhoneNumber')) {
      this.testCampaignForm.setErrors({ overflowPhoneNumber: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }

  private checkAllWhatsappValid(): void {
    let invalidFound = false;
    this.whatsappList.forEach(phoneNumbers => {
      if (phoneNumbers.invalid) {
        invalidFound = true;
        this.testCampaignForm.setErrors({ incorrectWhatsApp: true });
      }
    });
    if (!invalidFound && this.testCampaignForm.hasError('incorrectWhatsApp')) {
      this.testCampaignForm.setErrors({ incorrectWhatsApp: null });
      this.testCampaignForm.updateValueAndValidity();
    }

    if (this.whatsappList.length > 10) {
      this.testCampaignForm.setErrors({ overflowWhatsApp: true });
    } else if (this.testCampaignForm.hasError('overflowWhatsApp')) {
      this.testCampaignForm.setErrors({ overflowWhatsApp: null });
      this.testCampaignForm.updateValueAndValidity();
    }
  }

  private validatePhoneNumber(phoneNumber: string): any {
    let isForbidden = false;
    this.forbiddenCodes.forEach(code => {
      if (phoneNumber.includes(code)) {
        isForbidden = true;
      }
    });
    if (isForbidden) {
      return false;
    }
    const pattern = new RegExp(RegexpPattern.intlPhone);
    return pattern.test(String(phoneNumber).toLowerCase());
  }

  // Send Test campaign
  sendCampaign(): void {
    const testEmails = this.emailList.map(email => email.value);
    const testPhoneNumbers = this.phoneNumbersList.map(phoneNumber => phoneNumber.value);
    const testWhatsappNumbers = this.whatsappList.map(item => item.value);

    this.dataService.testCampaign({
      id: this.campaignId,
      emails: testEmails,
      phoneNumbers: testPhoneNumbers,
      whatsappNumbers: testWhatsappNumbers
    }).subscribe(result => {
      if (result) {
        this.isSending = false;
      }
    }, error => {
      if (error.error && error.error.code === 503) {
        const toasterService = appInjector().get(ToasterService);
        if (error.error && error.error.message) {
          toasterService.pop('error', null, error.error.message);
        } else {
          toasterService.pop('success', null, 'Sending test messages are queued by background process');
        }
      }
    })
  }

  validateForm(): boolean {
    return false;
  }

  /// On dialog close
  close(): void {
    this.dialogRef.close();
  }

  sendAnother() {
    this.isSending = true;
    this.unselectWhatsapp();
    this.unselectEmail();
    this.unselectSms();
    this.emailList = [];
    this.whatsappList = [];
    this.phoneNumbersList = [];
  }

}
