import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { EventCampaignDataService } from '../../event-campaign-data.service';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';

@Component({
  selector: 'irms-event-campaign-go-live-modal',
  templateUrl: './event-campaign-go-live-modal.component.html',
  styleUrls: ['./event-campaign-go-live-modal.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignGoLiveModalComponent implements OnInit {
  isLive = false;
  campaignId: any;
  isGoingLive: boolean;
  constructor(
    public dialogRef:MatDialogRef<EventCampaignGoLiveModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataService: EventCampaignDataService,
    ) { }

  ngOnInit() {
    this.campaignId = this.data;

  }

  cancel(){
    this.dialogRef.close({ res: false, message: '' });

  }

  showConfirmation(){
    this.isGoingLive = true;
    this.dataService.goLive(this.campaignId)
    .subscribe(x => {
      this.isLive = true;
    }, error => {
      this.dialogRef.close({ res: false, message: error.error.message });
      if (error.error && error.error.code === 503) {
        const toasterService = appInjector().get(ToasterService);
        if (error.error && error.error.message) {
          toasterService.pop('error', null, error.error.message);
        } else {
          toasterService.pop('success', null, 'Campaign is queued and will be launched by background process');
        }
      }
    })
  }

  confirm(){
    this.dialogRef.close({ res: true, message: '' });
  }

}
