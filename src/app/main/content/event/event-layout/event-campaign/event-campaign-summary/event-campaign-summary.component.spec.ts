import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignSummaryComponent } from './event-campaign-summary.component';

describe('EventCampaignSummaryComponent', () => {
  let component: EventCampaignSummaryComponent;
  let fixture: ComponentFixture<EventCampaignSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
