import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignContactImportDialogComponent } from './campaign-contact-import-dialog.component';

describe('CampaignContactImportDialogComponent', () => {
  let component: CampaignContactImportDialogComponent;
  let fixture: ComponentFixture<CampaignContactImportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignContactImportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignContactImportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
