import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ContactListItemComponent } from '../../event-guest/event-guest-import/select-guest/contact-list-item/contact-list-item.component';
import { FormGroup, FormBuilder, Validators, AsyncValidatorFn } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { IResponseModel } from 'types';
import { EventGuestService } from '../../event-guest/event-guest.service';
import { EventGuestDataService } from '../../event-guest/event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { map, debounceTime, distinctUntilChanged, switchMap, first } from 'rxjs/operators';
import { ContactsGroupedListService } from 'app/main/content/components/shared/contacts-grouped-list/contacts-grouped-list.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContactListItemModalComponent } from './contact-list-item-modal/contact-list-item-modal.component';
import { EventCampaignService } from '../event-campaign.service';
import { CampaignConfigurationsService } from '../event-campaign-edit/campaign-configurations/campaign-configurations.service';

@Component({
  selector: 'irms-campaign-contact-import-dialog',
  templateUrl: './campaign-contact-import-dialog.component.html',
  styleUrls: ['./campaign-contact-import-dialog.component.scss']
})
export class CampaignContactImportDialogComponent implements OnInit, OnDestroy {
  public listItemComponent: Object = ContactListItemModalComponent;
  public selectable = true;
  public data: Observable<IResponseModel>;
  public listId = '';
  public listName = '';
  isTaken = false;

  public isContactSearching = false;
  public isGuestsSearching = false;

  public contactsListFiltered: any[];
  public guestListFiltered: any[];
  guestImportForm: FormGroup = this.fb.group({
    listName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
    selectedGuests: ['', [Validators.required]],
  });
  importFrom = 'contacts';
  selectedListsIds = {
    contacts: [],
    guests: []
  };
  selectedToImport = [];
  selectedToImportLists = [];
  selectedGuests = [];
  guestList = [];
  public allImportCheck = false;
  public allGuestCheck = false;
  eventId = '';
  selectedListNames = [];
  isNameValid = true;
  isEmpty = false;
  isLoading = false;


  constructor(
    private fb: FormBuilder,
    public sectionService: CampaignConfigurationsService,
    protected dataService: EventGuestDataService,
    protected router: Router,
    public route: ActivatedRoute,
    private groupedListService: ContactsGroupedListService,
    public matDialogRef:MatDialogRef<CampaignContactImportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) {
    this.eventId = this.dialogData.eventId;
  }

  ngOnInit(): void{
    this.guestList = [];
    this.guestImportForm.controls.listName.valueChanges.subscribe(change =>{
      this.dataService.isListNameUnique(change).subscribe(result=>{
        if(result){
          this.isNameValid = false;
        } else {
          this.isNameValid = true;
        }
      })
    })
  }

  ngOnDestroy(): void{
  }

  /// Select lists to import form dropdown 
  filterDropdown(data): void {
    if (data == null) {
      return;
   }
    this.selectedListNames = [ ...data.loadedData.contacts.map(a => a.name), ...data.loadedData.guests.map(a => a.name)];
    this.selectedToImportLists = [];
    const contacts$ = this.loadContacts(data);
    const guests$ = this.loadGuests(data);
    this.joinQUeries(contacts$, guests$);
  }

  // Contacts loading from server
  loadContacts(data: any): any{
    return this.dataService.getContactsByListId(data.selectedListsIds.contacts)
      .pipe(map(res => {
        data.loadedData.contacts.forEach(x => {
          const selectedItems = res.filter(y => y.contactListId === x.id);
          
          this.selectedToImportLists.push({
            listId: x.id,
            listName: x.name,
            listCount: selectedItems.length,
            items: selectedItems,
            type: 0
          });
        
        });
      }));
  }

  // Guests loading from server
  loadGuests(data: any): any{
    return this.dataService.getContactsByListId(data.selectedListsIds.guests)
      .pipe(map(res => {
        data.loadedData.guests.forEach(x => {
          const selectedItems = res.filter(y => y.contactListId === x.id);

          this.selectedToImportLists.push({
            listId: x.id,
            listName: x.name,
            listCount: selectedItems.length,
            items: selectedItems,
            type: 1
          });
          
        });
      }));
  }

  /// On import selection change 
  onImportCheckChange(event): void {
    this.selectedToImport = event;
  }

  /// On guests selection change 
  onGuestCheckChange(event): void {
    this.selectedGuests = event;
  }
  /// Select all guest
  selectAllGuests(event): void {
    const arr = [];
    if (event) {
      this.guestList.forEach(guest => {
        arr.push(guest.id);
      });
    }
    this.selectedGuests = arr;
  }

  /// Import to guests
  import(): void {
    
    this.selectedToImport.forEach(list => {
      list.selected.forEach(contact => {
        const item = contact.item != null ? contact.item : contact;
        if (list.selectedIds.filter(f => f === item.id).length === 1 || !list.selectedIds[0]) {
          if (this.guestList.filter(f => f.id === item.id).length === 0) {
            this.guestList.push(item);
          }
        }
      });
    });
    
    this.selectAllList(true);
    setTimeout(() => this.selectAllList(false), 0);    
    this.selectedToImport.length = 0;
  }

  /// Remove from guests
  remove(): void {
    this.selectedGuests.forEach(guestId => {
      const index = this.guestList.findIndex(x => x.id === guestId);
      if (index > -1) {
        this.guestList.splice(index, 1);
      }
    });
    // clear selected
    this.selectedGuests.length = 0;
    this.allGuestCheck = false;
  }

  /// Select all toggle for all list
  selectAllList(event): void {
    if (event) {
      this.groupedListService.selectAll();
      this.allImportCheck = true;
    } else {
      this.groupedListService.deselectAll();
      this.allImportCheck = false;
      this.selectedToImport.length = 0;
    }
  }

  // Save guests list on server
  importGuestList(): void{
    this.isLoading = true;
    /// Create List
    const listName = this.guestImportForm.controls.listName.value;
    const dataList = {
      name: listName,
      eventId: this.eventId
    };
    this.dataService.createList(dataList)
      .subscribe(listId => {
        const toasterService = appInjector().get(ToasterService);
        if (listId) {
          this.listId = listId;
          this.sectionService.listId = this.listId;
          toasterService.pop('success', null, 'Successfully created');
          const guestIds = this.guestList.map(guest => guest.id);
          const data = {
            guestIds: guestIds,
            listId: listId
          };
          this.dataService.importGuestList(data).subscribe(result=>{
            if(result){
              this.matDialogRef.close({id : this.listId});
              this.isLoading = false;
            } 
          });
        } else {
          toasterService.pop('error', null, 'List name must be unique.');
          return;
        }
      });
    
  }

  cancel(){
    this.matDialogRef.close();
  }

  // Searching in contacts block
  searchForContacts(keyword: string): void{
    keyword = keyword.toUpperCase();
    if (keyword && keyword.length > 0) {
      this.isContactSearching = true;
      const items = [];

      this.selectedToImportLists.forEach(list => {
        const choosenItems = list.items.filter(x => x.item.fullName.toUpperCase().includes(keyword));
        items.push({
          listId: list.listId,
          listName: list.listName,
          listCount: choosenItems.length,
          type: list.type,
          items: choosenItems
        });
      });

      this.contactsListFiltered = items;
    } else {
      this.isContactSearching = false;

    }
  }

  // String searching in guests block
  searchForGuests(keyword: string): void{
    keyword = keyword.toUpperCase();
    if (keyword && keyword.length > 0) {
      this.isGuestsSearching = true;
      this.guestListFiltered = this.guestList.filter(x => x.fullName.toUpperCase().includes(keyword));
    } else {
      this.isGuestsSearching = false;
    }
  }

  // Perform joined queries with contacts and guests
  joinQUeries(request1$: Observable<void>, request2$: Observable<void>): void{
    forkJoin([request1$, request2$])
      .subscribe();
  }

  validateListNameUniqueness(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(value => this.dataService.isListNameUnique(control.value)),
        map((unique: boolean) => {
          this.isNameValid = !unique;
          return !unique ? null : { 'listNameTaken': true }
        }),
        first());
  }
  
}
