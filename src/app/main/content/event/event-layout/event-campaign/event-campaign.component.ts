import { Component, OnInit } from '@angular/core';
import { SectionComponent } from 'app/main/content/components/shared/section/section.component';
import { EventCampaignService } from './event-campaign.service';
import { EventCampaignDataService } from './event-campaign-data.service';
import { Section } from 'app/constants/constants';
import { Router } from '@angular/router';
import { EventCampaignModel } from './event-campaign.model';

@Component({
  selector: 'irms-event-campaign',
  templateUrl: './event-campaign.component.html',
})
export class EventCampaignComponent extends SectionComponent<EventCampaignModel> implements OnInit {

  constructor(public sectionService: EventCampaignService,
              protected dataService: EventCampaignDataService,
              protected router: Router) {
    super(Section.EventCampaigns, sectionService, dataService, router);
  }
}
