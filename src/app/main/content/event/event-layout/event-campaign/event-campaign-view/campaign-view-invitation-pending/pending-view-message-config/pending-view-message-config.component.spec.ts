import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingViewMessageConfigComponent } from './pending-view-message-config.component';

describe('PendingViewMessageConfigComponent', () => {
  let component: PendingViewMessageConfigComponent;
  let fixture: ComponentFixture<PendingViewMessageConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingViewMessageConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingViewMessageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
