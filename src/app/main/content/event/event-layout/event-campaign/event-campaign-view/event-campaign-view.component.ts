import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { EventCampaignViewService } from './event-campaign-view.service';
import { takeUntil } from 'rxjs/operators';
import { CampaignViewConfigurationsService } from './campaign-view-configurations/campaign-view-configurations.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-event-campaign-view',
  templateUrl: './event-campaign-view.component.html',
  styleUrls: ['./event-campaign-view.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignViewComponent implements OnInit, OnDestroy {

  public tabGroup = [
    {
      label: 'Campaign Configurations',
      link: './config'
    },
    {
      label: 'Invitation (RSVP)',
      link: './invitation'
    },
    {
      label: 'Invitation Pending',
      link: './invitation-pending'
    },
    {
      label: 'Invitation Accepted',
      link: './invitation-accepted'
    },
    {
      label: 'Invitation Rejected',
      link: './invitation-rejected'
    }
  ];

  public campaignName: string;
  subscription: Subscription;
  private unsubscribeAll = new Subject<any>();

  constructor(
    private service: EventCampaignViewService,
    public configStepService: CampaignViewConfigurationsService
  ) { }

  ngOnInit() {
    this.subscription = this.service.getCampaignName().pipe(takeUntil(this.unsubscribeAll)).subscribe(x => {
      this.campaignName = x;
   });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }
}
