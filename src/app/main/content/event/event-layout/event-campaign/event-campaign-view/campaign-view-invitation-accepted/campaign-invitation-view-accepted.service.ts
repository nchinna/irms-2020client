import { Injectable } from '@angular/core';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { CampaignInvitationAcceptedDataService } from '../../event-campaign-edit/campaign-invitation-accepted/campaign-invitation-accepted-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationViewAcceptedService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: CampaignInvitationAcceptedDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
