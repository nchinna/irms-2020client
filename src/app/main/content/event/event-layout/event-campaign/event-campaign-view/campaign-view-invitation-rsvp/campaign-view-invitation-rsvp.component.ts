import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CampaignConfigurationsDataService } from '../../event-campaign-edit/campaign-configurations/campaign-configurations-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CampaignInvitationDataService } from '../../event-campaign-edit/campaign-invitation/campaign-invitation-data.service';
import { EventCampaignEditService } from '../../event-campaign-edit/event-campaign-edit.service';

@Component({
  selector: 'irms-campaign-view-invitation-rsvp',
  templateUrl: './campaign-view-invitation-rsvp.component.html',
  styleUrls: ['./campaign-view-invitation-rsvp.component.scss']
})
export class CampaignViewInvitationRsvpComponent implements OnInit {

  campaignId: string;
  invitationId: string;
  now = new Date();
  model;
  isLoaded = false;

  public invitationForm = this.fb.group({
    dateInfo: [{}],
    emailTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      subject: '',
      sender: '',
    }),
    smsTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    }),
    whatsappTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    }),
    statistics: this.fb.group({
      email: this.fb.group({
        triggered: 0,
        processed: 0,
        delivered: 0,
        opens: 0,
        uniqueOpens: 0,
        clicks: 0,
        uniqueClicks: 0,
        bounces: 0,
        spamReports: 0,
        blocks: 0,
        accepted: 0,
        rejected: 0,
      }),
      sms: this.fb.group({
        triggered: 0,
        sent: 0,
        delivered: 0,
        unDelivered: 0,
        failed: 0,
        accepted: 0,
        rejected: 0,
      }),
      whatsapp: this.fb.group({
        initiated: 0,
        sent: 0,
        delivered: 0,
        read: 0,
        failed: 0,
        accepted: 0,
        rejected: 0
      })
    })
  });
  subscription: Subscription;

  constructor(
    protected fb: FormBuilder,
    private campaignDataService: CampaignConfigurationsDataService,
    protected dataService: CampaignInvitationDataService,
    private campaignService: EventCampaignEditService,
    protected route: ActivatedRoute,
    protected router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.reloadCampaignMetadata();
      this.reloadCampaignName();
    });
  }

  getStatistics() {
    const stats = this.invitationForm.controls['statistics'];
    return stats;
  }

  loadTemplates() {

  }

  reloadCampaignMetadata() {
    this.dataService.get(this.campaignId).subscribe(m => {
      this.isLoaded = true;
      this.model = m;
      if (m) {
        this.invitationId = this.model.id;
        this.invitationForm.controls.emailTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.emailTemplate['controls']['sender'].setValue(m.emailSender);
        this.invitationForm.controls.emailTemplate['controls']['subject'].setValue(m.emailSubject);
        this.invitationForm.controls.emailTemplate['controls']['image'].setValue(m.emailImage);
        this.invitationForm.controls.smsTemplate['controls']['sender'].setValue(m.smsSender);
        this.invitationForm.controls.smsTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.smsTemplate['controls']['image'].setValue(m.smsImage);
        this.invitationForm.controls.whatsappTemplate['controls']['image'].setValue(m.whatsappImage);
        this.invitationForm.controls.whatsappTemplate['controls']['sender'].setValue(m.whatsappSender);
        this.isLoaded = true;
        this.patchModelInForms();
        this.invitationForm.controls['dateInfo'].setValue({
          isInstant: m.isInstant,
          interval: m.interval,
          intervalType: m.intervalType
        });
      }
    });
  }

  patchModelInForms() {
    this.invitationForm.patchValue(this.model);
  }

  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }
}
