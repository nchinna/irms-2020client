import { Component, OnInit } from '@angular/core';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CampaignViewConfigurationsDataService } from '../campaign-view-configurations/campaign-view-configurations-data.service';
import { EventCampaignViewService } from '../event-campaign-view.service';
import { CampaignInvitationAcceptedService } from '../../event-campaign-edit/campaign-invitation-accepted/campaign-invitation-accepted.service';
import { CampaignInvitationAcceptedDataService } from '../../event-campaign-edit/campaign-invitation-accepted/campaign-invitation-accepted-data.service';
import { Section } from 'app/constants/constants';
import { CampaignInvitationViewAcceptedDataService } from './campaign-invitation-view-accepted-data.service';
import { CampaignInvitationViewAcceptedService } from './campaign-invitation-view-accepted.service';

@Component({
  selector: 'irms-campaign-view-invitation-accepted',
  templateUrl: './campaign-view-invitation-accepted.component.html',
  styleUrls: ['./campaign-view-invitation-accepted.component.scss']
})
export class CampaignViewInvitationAcceptedComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit {

  nodes = [];
  campaignId: any;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private campaignDataService: CampaignViewConfigurationsDataService,
    private campaignService: EventCampaignViewService,
    public sectionService: CampaignInvitationViewAcceptedService,
    protected dataService: CampaignInvitationViewAcceptedDataService
  ) { 
    super(Section.EventCampaigns, sectionService, dataService, router, route)
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.updateNodes();
      this.reloadCampaignName();
    });
  }

  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }
  
  updateNodes() {
    this.dataService.getList(this.campaignId).subscribe(result => {
      this.nodes = result;
    })
  }
}
