import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CampaignViewConfigurationsDataService } from '../campaign-view-configurations/campaign-view-configurations-data.service';
import { EventCampaignViewService } from '../event-campaign-view.service';
import { CampaignInvitationViewRejectedDataService } from './campaign-invitation-view-rejected-data.service';
import { CampaignInvitationViewRejectedService } from './campaign-invitation-view-rejected.service';
import { Section } from 'app/constants/constants';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';

@Component({
  selector: 'irms-campaign-view-invitation-rejected',
  templateUrl: './campaign-view-invitation-rejected.component.html',
  styleUrls: ['./campaign-view-invitation-rejected.component.scss']
})
export class CampaignViewInvitationRejectedComponent implements OnInit {

  nodes = [];
  campaignId: any;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private campaignDataService: CampaignViewConfigurationsDataService,
    private campaignService: EventCampaignViewService,
    public sectionService: CampaignInvitationViewRejectedService,
    public dataService: CampaignInvitationViewRejectedDataService
  ) { 
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.updateNodes();
      this.reloadCampaignName();
    });
  }

  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }
  
  updateNodes() {
    this.dataService.getList(this.campaignId).subscribe(result => {
      this.nodes = result;
    })
  }
}
