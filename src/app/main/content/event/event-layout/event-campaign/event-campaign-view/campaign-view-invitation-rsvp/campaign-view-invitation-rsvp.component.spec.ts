import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignViewInvitationRsvpComponent } from './campaign-view-invitation-rsvp.component';

describe('CampaignViewInvitationRsvpComponent', () => {
  let component: CampaignViewInvitationRsvpComponent;
  let fixture: ComponentFixture<CampaignViewInvitationRsvpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignViewInvitationRsvpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewInvitationRsvpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
