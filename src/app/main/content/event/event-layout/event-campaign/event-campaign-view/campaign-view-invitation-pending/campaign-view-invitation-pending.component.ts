import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CampaignViewConfigurationsDataService } from '../campaign-view-configurations/campaign-view-configurations-data.service';
import { EventCampaignViewService } from '../event-campaign-view.service';
import { CampaignInvitationPendingService } from '../../event-campaign-edit/campaign-invitation-pending/campaign-invitation-pending.service';
import { CampaignInvitationPendingDataService } from '../../event-campaign-edit/campaign-invitation-pending/campaign-invitation-pending-data.service';
import { Section } from 'app/constants/constants';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';

@Component({
  selector: 'irms-campaign-view-invitation-pending',
  templateUrl: './campaign-view-invitation-pending.component.html',
  styleUrls: ['./campaign-view-invitation-pending.component.scss']
})
export class CampaignViewInvitationPendingComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit {

  nodes = [];
  campaignId: any;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private campaignDataService: CampaignViewConfigurationsDataService,
    private campaignService: EventCampaignViewService,
    public sectionService: CampaignInvitationPendingService,
    protected dataService: CampaignInvitationPendingDataService
  ) { 
    super(Section.EventCampaigns, sectionService, dataService, router, route)
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.sectionService.setFilter({ campaignId: this.campaignId });
      this.updateNodes();
      this.reloadCampaignName();
    });
  }

  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }
  
  updateNodes() {
    this.dataService.getList(this.campaignId).subscribe(result => {
      this.nodes = result;
    })
  }
}
