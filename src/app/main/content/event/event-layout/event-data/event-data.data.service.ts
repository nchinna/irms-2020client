import { Injectable } from '@angular/core';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { IEventList, IResponseModel } from 'types';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { EventDataModel } from './event-data.model';

@Injectable({
  providedIn: 'root'
})
export class EventDataDataService implements SectionDataService<EventDataModel> {
  private url = `${BASE_URL}api/campaign`;
  private dataUrl = `${BASE_URL}api/dataModule`;
  constructor(private http: HttpClient) { }

  @Loader()
  getList(filterParam: IEventList): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/list`, filterParam);
  }

  getFormFields(id: any): Observable<any> {
    return this.http.get<any>(`${this.dataUrl}/rfiform-questions/${id}`);
  }

  get(id): Observable<EventDataModel> {
    return this.http.get<EventDataModel>(`${this.url}/${id}/info`);
  }

  @Toast('Successfully created')
  create(model: any): Observable<any> {
    return this.http.post<any>(`${this.url}`, model);
  }

  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  @Toast('Successfully deleted')
  delete(model) {
    return this.http.request('delete', `${this.url}`, { body: model });
  }

  getCampaignName(data): Observable<any> {
    return this.http.post(`${this.url}/campaign-name`, data, { responseType: 'text' });
  }
  getRfiFormName(data): Observable<any> {
    return this.http.post(`${this.dataUrl}/rfi-name`, data);
  }
  getRfiList(filterParam: any): Observable<IResponseModel> {
    // return this.http.post<IResponseModel>(`${this.url}/rfiList`, filterParam);
    return of(
      {
        items: [
          {
            id: '8d46c05b-549a-4820-bc96-2412c8757175',
            responses: 20,
            uniqueVisits: 139,
            target: true,
            title: 'DEEPENDS'
          },
          {
            id: 'fa964367-66ba-41ae-8b77-80e10ff0de71',
            responses: 312,
            uniqueVisits: 337,
            target: false,
            title: 'CEPRENE'
          },
          {
            id: '53769302-e532-473c-8052-c8eee62b05bc',
            responses: 99,
            uniqueVisits: 443,
            target: false,
            title: 'EXOVENT'
          },
          {
            id: '7e7f3189-bb32-430d-b2f6-f8e29c84782b',
            responses: 230,
            uniqueVisits: 360,
            target: false,
            title: 'SKINSERVE'
          },
          {
            id: '1666b18c-dfcb-49af-be2c-07cb6b4ba53f',
            responses: 229,
            uniqueVisits: 912,
            target: true,
            title: 'QUILK'
          },
          {
            id: 'c90ea56f-a14b-4cd3-944b-29b659ed9e25',
            responses: 225,
            uniqueVisits: 245,
            target: false,
            title: 'KATAKANA'
          },
          {
            id: 'faaf56b4-c539-4080-bef0-86c602b6cf2e',
            responses: 41,
            uniqueVisits: 753,
            target: true,
            title: 'ZIPAK'
          }
        ], totalCount: 12
      }
    );
  }

  getRfiFormSummary(id): Observable<any> {
    return this.http.get<any>(`${this.dataUrl}/rfiform-summary/${id}`);
    // return this.http.get<EventDataModel>(`${this.url}/${id}/summary`);
    // return of({
    //   responseCount: 32,
    //   questions: [
    //     {
    //       id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //       mappedField: 'Full name',
    //       questionMappingType: 1,
    //       sortOrder: 1,
    //       answeredCount: 19,
    //       question: {
    //         type: 'singleLineText',
    //         id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //         headlineArabic: '',
    //         headline: `What's your name?`,
    //       },
    //       responses: [
    //         {
    //           id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //           response: 'response 1 lorem ipsum dolor sit amet jfrijif rf r iofj roij fiojriof ori fio rioj fio riojiojiojio oi oi oijiojio rjoif jrfi rfi uriuf huir hfuihr  fhiur hfui ruihfiuhr fiu riuf iuruihf uirfiur  fiuruifuiriufhruifiu ruif uir uif uir fui rui fui ruif irf r f ri fi hrih ihifhuirhifhihihrihfirirfr'
    //         },
    //         {
    //           id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //           response: 'response 2 lorem ipsum dolor sit amet'
    //         },
    //         {
    //           id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //           response: 'response 4 lorem ipsum dolor sit amet'
    //         },
    //         {
    //           id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //           response: 'response 5 lorem ipsum dolor sit amet'
    //         },
    //         {
    //           id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //           response: 'response 3 lorem ipsum dolor sit amet'
    //         }
    //       ],
    //     },
    //     {
    //       id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //       mappedField: 'Food Preference',
    //       questionMappingType: 1,
    //       sortOrder: 2,
    //       answeredCount: 29,
    //       question: {
    //         type: 'selectSearch',
    //         id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //         headlineArabic: 'وش اكلك المفضل ؟',
    //         headline: '',
    //       },
    //       responses: [
    //         {
    //           id: 'jeieejeoddewjeoje',
    //           option: 'Option Lorem',
    //           percent: 12,
    //         },
    //         {
    //           id: 'jeweweieejeojweeoje',
    //           option: 'Option Ipsum',
    //           percent: 15,
    //         },
    //         {
    //           id: 'jeieejeojeorr34je',
    //           option: 'Option Dolor',
    //           percent: 25,
    //         },
    //         {
    //           id: 'jeieejeoj2e434oje',
    //           option: 'Option Sit',
    //           percent: 60,
    //         }
    //       ]
    //     },
    //     {
    //       id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //       mappedField: '',
    //       questionMappingType: 0,
    //       sortOrder: 3,
    //       answeredCount: 29,
    //       question: {
    //         type: 'rating',
    //         id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
    //         headlineArabic: 'Rate your experience..',
    //         headline: 'Rate your experience..',
    //       },
    //       responses: [
    //         {
    //           id: 'jeieejeoddewjeoje',
    //           option: '1',
    //           percent: 12,
    //         },
    //         {
    //           id: 'jeweweieejeojweeoje',
    //           option: '2',
    //           percent: 15,
    //         },
    //         {
    //           id: 'jeieejeojeorr34je',
    //           option: '3',
    //           percent: 0,
    //         },
    //         {
    //           id: 'jeieejssseoj2e434oje',
    //           option: '4',
    //           percent: 30,
    //         },
    //         {
    //           id: 'jeieejeoj2e434oje',
    //           option: '5',
    //           percent: 5,
    //         }
    //       ]
    //     },
    //   ]
    // }) ;
  }
  getRfiFormQuestionResponses(filterParam: any): Observable<IResponseModel> {
    return of(
      {
        items: [
          {
            id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
            response: 'response 11 lorem ipsum dolor sit amet'
          },
          {
            id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
            response: 'response 12 lorem ipsum dolor sit amet'
          },
          {
            id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
            response: 'response 14 lorem ipsum dolor sit amet'
          },
          {
            id: 'ba24764a-0ac7-4742-9314-c53c750f5781',
            response: 'response 15 lorem ipsum dolor sit amet'
          },
        ],
        totalCount: 12
      });
  }
  getResponsesList(filterParam: any): Observable<IResponseModel> {
    return this.http.post<any>(`${this.dataUrl}/rfiform-responses`, filterParam);
  }

  getGuestResponse(model: any): Observable<any> {
    return this.http.post<any>(`${this.dataUrl}/rfiform-guest-response`, model);
  }
}
