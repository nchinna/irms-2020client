import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-event-data-campaign-rfi-list-item',
  templateUrl: './event-data-campaign-rfi-list-item.component.html',
  styleUrls: ['./event-data-campaign-rfi-list-item.component.scss']
})
export class EventDataCampaignRfiListItemComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
    this.data.title = JSON.parse(this.data.title).title;
  }

}
