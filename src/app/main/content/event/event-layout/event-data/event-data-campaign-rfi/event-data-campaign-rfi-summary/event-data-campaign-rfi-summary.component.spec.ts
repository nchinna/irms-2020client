import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiSummaryComponent } from './event-data-campaign-rfi-summary.component';

describe('EventDataCampaignRfiSummaryComponent', () => {
  let component: EventDataCampaignRfiSummaryComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
