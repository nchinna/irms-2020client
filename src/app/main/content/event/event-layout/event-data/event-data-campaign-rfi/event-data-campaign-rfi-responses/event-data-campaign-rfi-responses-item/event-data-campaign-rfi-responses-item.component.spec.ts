import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiResponsesItemComponent } from './event-data-campaign-rfi-responses-item.component';

describe('EventDataCampaignRfiResponsesItemComponent', () => {
  let component: EventDataCampaignRfiResponsesItemComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiResponsesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiResponsesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiResponsesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
