import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiResponsesComponent } from './event-data-campaign-rfi-responses.component';

describe('EventDataCampaignRfiResponsesComponent', () => {
  let component: EventDataCampaignRfiResponsesComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiResponsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiResponsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiResponsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
