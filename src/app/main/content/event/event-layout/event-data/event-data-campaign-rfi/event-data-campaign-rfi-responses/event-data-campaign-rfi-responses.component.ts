import { Component, OnInit } from '@angular/core';
import { EventDataModService } from '../../event-data.service';
import { EventDataDataService } from '../../event-data.data.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { IActionButtons, IRFIResponsesPage } from 'types';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section, sideDialogConfig } from 'app/constants/constants';
import { EventDataCampaignRfiResponsesItemComponent } from './event-data-campaign-rfi-responses-item/event-data-campaign-rfi-responses-item.component';
import { of } from 'rxjs';
import { RfiModel } from '../rfi.model';
import { EventDataCampaignRfiResponsesExportModalComponent } from './event-data-campaign-rfi-responses-export-modal/event-data-campaign-rfi-responses-export-modal.component';
import { EventDataCampaignRfiResponsesDataService } from './event-data-campaign-rfi-responses-data.service';
import { EventDataCampaignRfiResponsesService } from './event-data-campaign-rfi-responses.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';

@Component({
  selector: 'irms-event-data-campaign-rfi-responses',
  templateUrl: './event-data-campaign-rfi-responses.component.html',
  styleUrls: ['./event-data-campaign-rfi-responses.component.scss'] ,
  animations: fuseAnimations,
})
export class EventDataCampaignRfiResponsesComponent extends SectionListComponent<RfiModel> implements OnInit {
public listName = 'Back';
selectedGuests = [];
public actionButtons: IActionButtons = {
  leftSide: [
    {
      title: 'Export Selected',
      icon: 'launch',
      handler: this.exportResponses.bind(this)
    }
  ],
};
isLocked;
listId: number;
isUpdating = false;

eventId: any;

public listItemComponent: Object = EventDataCampaignRfiResponsesItemComponent;
listHeaders =  ['Guest Name', 'Submission Date'];
  rfiType: any;
constructor(public sectionService: EventDataCampaignRfiResponsesService,
            protected dataService: EventDataCampaignRfiResponsesDataService,
            private dialog: MatDialog,
            private service: EventDataModService,
            router: Router, public route: ActivatedRoute,
            public animDialog: DialogAnimationService) {
  super(Section.EventDataRfiResponses, sectionService, dataService, router);
}

guestPage: IRFIResponsesPage;

ngOnInit() {
  this.route.params.subscribe((params: Params) => {
    this.rfiType = params.rfiType;
    this.sectionService.targetRFIFormId = params['fid'];
  });

  this.sectionService.subscriptions = [];
  this.guestPage = {
    formId: this.sectionService.targetRFIFormId,
    pageSize: 5,
    pageNo: 1,
    searchText: ''
  };
  this.sectionService.setFilter({ formId: this.sectionService.targetRFIFormId });
  super.ngOnInit();

  this.reloadRecords();
}
/// Handle List Selection Change
public selectionChange(event): void{
  if (this.maxSelectable === 0){
    this.setMaxSelectable();
  }
  this.selectedGuests = event;
  if (this.selectedGuests.length !== this.maxSelectable && this.selectedGuests.length > 0){
    this.indeterminate = true;
    this.checked = false;
  } else if (this.selectedGuests.length === this.maxSelectable){
    this.indeterminate = false;
    this.checked = true;
  } else {
    this.checked = false;
    this.indeterminate = false;
  }
}

/// handle select all
public selectAll(event): void{
  let ids = [];
  this.data.subscribe(records => ids = records.items.map(i => i.id));
  if (event) {
    this.selectionChange(ids);
  } else {
    this.selectionChange([]);
  }
}

public search(text): void {
  if (!text) {
    this.guestPage.searchText = null;
  } else {
    this.guestPage.searchText = text;
  }
  this.guestPage.pageNo = 1;
  this.sectionService.pageIndex = 0;
  this.sectionService.pageSize = this.guestPage.pageSize;
  this.reloadRecords();
}

public changePagination(event): void {
  this.guestPage.pageNo = event.pageIndex + 1;
  this.guestPage.pageSize = event.pageSize;
  this.reloadRecords();
}

private reloadRecords(): void {
  if (!this.isUpdating) {
    this.isUpdating = true;
    this.dataService.getResponsesList(this.guestPage)
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(response => {
        this.service.setResponsesCount(response.totalCount);
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
        this.isUpdating = false;
      });
  }
}

/// Export Guests 
exportResponses(): void {
  const dialogConfig: any = sideDialogConfig;
  dialogConfig.autoFocus = true;
  dialogConfig.minWidth = '40vw';
  // dialogConfig.maxHeight = '55vh';
  dialogConfig.data = {
    id: this.sectionService.targetRFIFormId
  };

  const dialogRef = this.animDialog.open(EventDataCampaignRfiResponsesExportModalComponent, dialogConfig);

  dialogRef.afterClosed()
    .subscribe(result => {
      if (result) {
        const data = {
          ...result,
          formId: this.sectionService.targetRFIFormId,
          users: this.selectedGuests
        };

        this.dataService
          .exportRfiResponses(data)
          .subscribe(resp => {
            this.saveExportedContacts(resp);
          });
      }
    });
}

private saveExportedContacts(resp: any) {
  const element = document.createElement('a');
  element.href = URL.createObjectURL(resp.body);
  element.download = `Rfi.xlsx`;
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

}
