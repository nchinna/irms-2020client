import { Injectable } from '@angular/core';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventDataModel } from '../../event-data.model';
import { IPage, IResponseModel } from 'app/core/types/types';
import { Observable, of } from 'rxjs';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';

@Injectable({
    providedIn: 'root'
  })
  export class EventDataCampaignRfiResponsesDataService implements SectionDataService<EventDataModel> {
    private dataUrl = `${BASE_URL}api/dataModule`;

    constructor(private http: HttpClient) {}

    @Loader()
    getList(filterParam: any): Observable<IResponseModel> {
        return this.http.post<any>(`${this.dataUrl}/rfiform-responses`, filterParam);
    }

    get(id: any): Observable<EventDataModel> {
        throw new Error("Method not implemented.");
    }
    create(model: any): Observable<string> {
        throw new Error("Method not implemented.");
    }
    update(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }
    delete(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }

    getResponsesList(filterParam: any): Observable<IResponseModel> {
        return this.http.post<any>(`${this.dataUrl}/rfiform-responses`, filterParam);
    }

    exportRfiResponses(data): Observable<any> {
        return this.http.post(`${this.dataUrl}/rfiform-export`, data, {
        responseType: 'blob' as 'json',
        observe: 'response' as 'body'
        })
    }
  }