import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { EventDataModel } from '../../event-data.model';
import { Injectable } from '@angular/core';
import { EventDataCampaignRfiResponsesDataService } from './event-data-campaign-rfi-responses-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
  })
  export class EventDataCampaignRfiResponsesService extends SectionService<EventDataModel> {
    targetRFIFormId;
    constructor(
        protected dataService: EventDataCampaignRfiResponsesDataService,
        queryService: QueryService,
        protected router: Router
    ) {
        super(dataService, queryService, router);
    }
    
  }