import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiComponent } from './event-data-campaign-rfi.component';

describe('EventDataCampaignRfiComponent', () => {
  let component: EventDataCampaignRfiComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
