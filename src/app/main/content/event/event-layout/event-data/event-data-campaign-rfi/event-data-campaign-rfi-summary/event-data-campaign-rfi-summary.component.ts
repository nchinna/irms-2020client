import { Component, OnInit } from '@angular/core';
import { EventDataDataService } from '../../event-data.data.service';
import { EventDataModService } from '../../event-data.service';
import { fuseAnimations } from 'app/core/animations';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'irms-event-data-campaign-rfi-summary',
  templateUrl: './event-data-campaign-rfi-summary.component.html',
  styleUrls: ['./event-data-campaign-rfi-summary.component.scss'],
  animations: fuseAnimations
})
export class EventDataCampaignRfiSummaryComponent implements OnInit {
  pollTypes = ['select', 'selectSearch'];
  listTypes = ['singleLineText', 'multilineText', 'date', 'phone', 'email'];
  loadButton = [];
  responseSummary;
  pageSize = 5;
  isDataLoaded = false;
  options = [];
  constructor(private dataService: EventDataDataService, private service: EventDataModService,
              public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.dataService.getRfiFormSummary(this.service.targetRFIFormId).subscribe(data => {
      data.questions = data.questions.map(x => {
        const question = JSON.parse(x.question);
        const responses = JSON.parse(x.responses);
        let modResponses = [];
        data.questions.forEach(element => {
          this.options.push(5);
        });
        if (question.type === 'rating') {
          for (let i = 1; i <= 5; ++i) {
            const existingValue = responses.filter(y => y.option === i)[0];
            modResponses.push({
              id: i,
              option: `${i}`,
              percent: existingValue ? existingValue.percent : 0
            });
          }
        }
        else if (question.type === 'select' || question.type === 'selectSearch') {
          modResponses = responses;
          responses.sort((a, b) => (a.percent < b.percent) ? 1 : (a.percent === b.percent) ? ((a.option.localeCompare(b.option) ? 1 : -1)) : -1 );

        }
        else {
          responses.forEach(resp => {
            modResponses.push({
              ...resp,
              // response: resp.response.substring(1, resp.response.length - 1)
              response: question.type === 'date' ?
                this.datepipe.transform(new Date(resp.response).toString() === 'Invalid Date' ?
                  null : new Date(resp.response), 'dd/MM/yyyy') 
              : resp.response
            });
          });
        }
        

        return {
          ...x,
          question,
          responses: modResponses
        };
      });

      this.isDataLoaded = true;
      this.responseSummary = data;
    });
  }

  emptyArray(num): any {
    return new Array(parseInt(num, 10));
  }
  loadMoreResponses(qIndex): void {
    const index = this.loadButton.indexOf(this.responseSummary.questions[qIndex].id);
    const respCount = this.responseSummary.questions[qIndex].responses.length;
    const addRespCount = this.responseSummary.questions[qIndex].additionalResponses ? this.responseSummary.questions[qIndex].additionalResponses.length : 0;

    if (index === -1 && (respCount + addRespCount < this.responseSummary.questions[qIndex].answeredCount)) {
      this.loadButton.push(this.responseSummary.questions[qIndex].id);
      const filter = {
        formId: this.service.targetRFIFormId,
        questionId: this.responseSummary.questions[qIndex].id,
        pageNo: Math.floor((respCount + addRespCount) / this.pageSize) + 1,
        pageSize: this.pageSize,
        searchText: ''
      };
      this.dataService.getRfiFormQuestionResponses(filter).subscribe(data => {
        if (this.responseSummary.questions[qIndex].additionalResponses) {
          this.responseSummary.questions[qIndex].additionalResponses = this.responseSummary.questions[qIndex].additionalResponses.concat(data.items);
        } else {
          this.responseSummary.questions[qIndex].additionalResponses = data.items;
        }
        this.loadButton.splice(index, 1);
      });
    }
    // this.loadButton.splice(index, 1);  
  }
  collapseResponses(qIndex): void {
    this.responseSummary.questions[qIndex].additionalResponses = null;
  }

}
