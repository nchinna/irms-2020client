import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EventDataComponent } from './event-data.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { RouterModule } from '@angular/router';
import { EventDataCampaignListComponent } from './event-data-campaign-list/event-data-campaign-list.component';
import { MainModule } from 'app/main/main.module';
import { EventDataCampaignListItemComponent } from './event-data-campaign-list/event-data-campaign-list-item/event-data-campaign-list-item.component';
import { EventDataCampaignRfiListComponent } from './event-data-campaign-rfi-list/event-data-campaign-rfi-list.component';
import { EventDataCampaignRfiListItemComponent } from './event-data-campaign-rfi-list/event-data-campaign-rfi-list-item/event-data-campaign-rfi-list-item.component';
import { EventDataCampaignRfiComponent } from './event-data-campaign-rfi/event-data-campaign-rfi.component';
import { EventDataCampaignRfiSummaryComponent } from './event-data-campaign-rfi/event-data-campaign-rfi-summary/event-data-campaign-rfi-summary.component';
import { EventDataCampaignRfiResponsesComponent } from './event-data-campaign-rfi/event-data-campaign-rfi-responses/event-data-campaign-rfi-responses.component';
import { EventDataCampaignRfiResponsesItemComponent } from './event-data-campaign-rfi/event-data-campaign-rfi-responses/event-data-campaign-rfi-responses-item/event-data-campaign-rfi-responses-item.component';
import { EventDataCampaignRfiResponseDetailsComponent } from './event-data-campaign-rfi-response-details/event-data-campaign-rfi-response-details.component';
import { EventDataCampaignRfiResponsesExportModalComponent } from './event-data-campaign-rfi/event-data-campaign-rfi-responses/event-data-campaign-rfi-responses-export-modal/event-data-campaign-rfi-responses-export-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { EventDataListAnalysisCampaignListComponent } from './event-data-list-analysis-campaign-list/event-data-list-analysis-campaign-list.component';
import { EventDataListAnalysisCampaignListItemComponent } from './event-data-list-analysis-campaign-list/event-data-list-analysis-campaign-list-item/event-data-list-analysis-campaign-list-item.component';
import { EventDataTabLayoutComponent } from './event-data-tab-layout/event-data-tab-layout.component';
import { EventDataListAnalysisListItemComponent } from './event-data-campaign-list/event-data-list-analysis-list-item/event-data-list-analysis-list-item.component';
import { EventDataListAnalysisComponent } from './event-data-campaign-list/event-data-list-analysis/event-data-list-analysis.component';

const routes = [
  {
    path: '',
    component: EventDataComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataTabLayoutComponent,
        children: [
          { path: '', redirectTo: 'campaigns', pathMatch: 'full' },

          {
            path: 'campaigns',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignListComponent,
          },
          {
            path: 'list-analysis',
            canActivateChild: [AuthGuard],
            component: EventDataListAnalysisCampaignListComponent,
          },
        ]
      },
      
      {
        path: ':rfiType/:cid/RFI',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiListComponent,
      },
      {
        path: ':rfiType/:cid/RFI',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiListComponent,
      },
      {
        path: ':rfiType/:cid/RFI/:fid',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiComponent,
        children: [
          {
            path: '',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiSummaryComponent,
          },
          {
            path: 'summary',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiSummaryComponent,
          },
          {
            path: 'responses',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiResponsesComponent,
          }
        ]
      },
      {
        path: ':rfiType/:fid',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiComponent,
        children: [
          {
            path: '',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiSummaryComponent,
          },
          {
            path: 'summary',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiSummaryComponent,
          },
          {
            path: 'responses',
            canActivateChild: [AuthGuard],
            component: EventDataCampaignRfiResponsesComponent,
          }
        ]
      },
      {
        path: ':rfiType/:fid/responses/:rid',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiResponseDetailsComponent,
      },
      {
        path: ':rfiType/:cid/RFI/:fid/responses/:rid',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventDataCampaignRfiResponseDetailsComponent,
      }
    ]
  }
];

@NgModule({
  declarations: [
    EventDataComponent,
    EventDataCampaignListComponent,
    EventDataCampaignListItemComponent,
    EventDataCampaignRfiListComponent,
    EventDataCampaignRfiListItemComponent,
    EventDataCampaignRfiComponent,
    EventDataCampaignRfiSummaryComponent,
    EventDataCampaignRfiResponsesComponent,
    EventDataCampaignRfiResponsesItemComponent,
    EventDataCampaignRfiResponseDetailsComponent,
    EventDataCampaignRfiResponsesExportModalComponent,
    EventDataListAnalysisCampaignListComponent,
    EventDataListAnalysisCampaignListItemComponent,
    EventDataListAnalysisComponent,
    EventDataListAnalysisListItemComponent,
    EventDataTabLayoutComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forChild(routes)
  ],
  providers: [DatePipe, DialogAnimationService],
  entryComponents: [
    EventDataCampaignListItemComponent,
    EventDataCampaignRfiListItemComponent, 
    EventDataCampaignRfiResponsesItemComponent, 
    EventDataCampaignRfiResponsesExportModalComponent,
    EventDataListAnalysisCampaignListItemComponent,
    EventDataListAnalysisListItemComponent
  ],
  exports: [RouterModule]
})
export class EventDataModule { }
