import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataTabLayoutComponent } from './event-data-tab-layout.component';

describe('EventDataTabLayoutComponent', () => {
  let component: EventDataTabLayoutComponent;
  let fixture: ComponentFixture<EventDataTabLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataTabLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataTabLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
