import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-event-data-tab-layout',
  templateUrl: './event-data-tab-layout.component.html',
  styleUrls: ['./event-data-tab-layout.component.scss'],
  animations: fuseAnimations
})
export class EventDataTabLayoutComponent implements OnInit {
  public tabGroup = [
      {
        label: 'Campaigns',
        link: './campaigns'
      },
      {
        label: 'List Analysis',
        link: './list-analysis'
      }
    ];
    constructor( protected route: ActivatedRoute) { }
  
    ngOnInit(): void {
    }
  
}
