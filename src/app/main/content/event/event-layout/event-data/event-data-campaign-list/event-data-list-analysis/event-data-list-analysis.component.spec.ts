import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataListAnalysisComponent } from './event-data-list-analysis.component';

describe('EventDataListAnalysisComponent', () => {
  let component: EventDataListAnalysisComponent;
  let fixture: ComponentFixture<EventDataListAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataListAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataListAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
