import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { IEventList, IResponseModel } from 'types';
import { Observable, of } from 'rxjs';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { EventDataModel } from './event-layout/event-data/event-data.model';

@Injectable({
    providedIn: 'root'
})
export class DataModuleRfiDataService implements SectionDataService<EventDataModel> {
    
    private url = `${BASE_URL}api/dataModule`
    private campaignUrl = `${BASE_URL}api/campaign`
    constructor(private http: HttpClient) {}

    @Loader()
    getList(filterParam: any): Observable<IResponseModel> { 
        return this.http.post<any>(`${this.url}/rfiform-info`, filterParam)
    }

    @Loader()
    getListAnalysisRFIList(filterParam: any): Observable<any> {
        return this.http.post<any>(`${this.url}/list-analysis-rfiform-info`, filterParam)
      }

    getCampaignName(data): Observable<any> {
        return this.http.post(`${this.campaignUrl}/campaign-name`, data, { responseType: 'text' });
    }

    getRfiFormName(data): Observable<any> {
        return this.http.post(`${this.url}/rfi-name`, data, { responseType: 'text' });
    }

    get(id): Observable<EventDataModel> {
        throw new Error('Method not implemented');
    }    

    create(model: any): Observable<string> {
        throw new Error('Method not implemented');
    }

    update(model): Observable<any> {
        throw new Error('Method not implemented');
    }

    delete(model): Observable<any> {
        throw new Error('Method not implemented');
    }
}