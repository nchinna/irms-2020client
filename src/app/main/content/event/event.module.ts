import { NgModule } from '@angular/core';
import { EventComponent } from './event.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { RouterModule } from '@angular/router';
import { EventCreateComponent } from './event-create/event-create.component';
import { EventService } from './event.service';
import { EventDataService } from './event-data.service';
import { AgmCoreModule } from '@agm/core';
import { EventListItemComponent } from './event-list/event-list-item/event-list-item.component';
import { FuseSearchBarModule, FuseNavigationModule } from '@fuse/components';
import { EventListComponent } from './event-list/event-list.component';
import { EventLayoutComponent } from './event-layout/event-layout.component';
import { EventNavbarComponent } from './event-layout/event-navbar/event-navbar.component';
import { MatTableModule } from '@angular/material';
import { MediaTemplateGroupComponent } from '../components/shared/media-template-group/media-template-group.component';
import { FormBuilderModule } from '../components/shared/form-builder/form-builder.module';
import { CampaignRfiDesignModalComponent } from './event-layout/event-campaign/event-campaign-edit/campaign-invitation/campaign-rfi-design-modal/campaign-rfi-design-modal.component';
import { EventCampaignGoLiveModalComponent } from './event-layout/event-campaign/event-campaign-summary/event-campaign-go-live-modal/event-campaign-go-live-modal.component';
import { RfiDataReviewModalComponent } from './event-layout/event-guest/event-guest-list-analysis/collect-important-fields/rfi-data-review-modal/rfi-data-review-modal.component';

const routes = [
  {
    path: '',
    component: EventComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: 'create',
        canActivateChild: [AuthGuard],
        component: EventCreateComponent,
      },
      {
        path: ':id',
        canActivateChild: [AuthGuard],
        component: EventLayoutComponent,
        children: [
          {
            path: 'dashboard',
            canLoad: [AuthGuard],
            loadChildren: () => import('app/main/content/event/event-layout/event-dashboard/event-dashboard.module').then(m => m.EventDashboardModule),
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: 'campaigns',
            canLoad: [AuthGuard],
            loadChildren: () => import('app/main/content/event/event-layout/event-campaign/event-campaign.module').then(m => m.EventCampaignModule),
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: 'data',
            canLoad: [AuthGuard],
            loadChildren: () => import('app/main/content/event/event-layout/event-data/event-data.module').then(m => m.EventDataModule),
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: 'guests',
            canLoad: [AuthGuard],
            loadChildren: () => import('app/main/content/event/event-layout/event-guest/event-guest.module').then(m => m.EventGuestModule),
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: 'settings',
            canLoad: [AuthGuard],
            loadChildren: () => import('app/main/content/event/event-layout/event-settings/event-settings.module').then(m => m.EventSettingsModule),
            data: { expectedRoles: [RoleType.TenantAdmin] },
          }
        ]
      },
      {
        path: '',
        canActivateChild: [AuthGuard],
        component: EventListComponent,
      }]
  }];

@NgModule({
  declarations: [
    EventCreateComponent,
    EventListItemComponent,
    EventListComponent,
    EventLayoutComponent,
    EventNavbarComponent,


    ],
  imports: [
    MainModule,
    AgmCoreModule,
    ContentModule,
    FuseSearchBarModule,
    FuseNavigationModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  entryComponents: [
    EventListItemComponent,
  ],
  providers: [
    EventService,
    EventDataService
  ]
})
export class EventModule { }
