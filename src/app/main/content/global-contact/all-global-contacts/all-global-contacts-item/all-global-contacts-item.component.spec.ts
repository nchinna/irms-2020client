import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGlobalContactsItemComponent } from './global-contact-list-contacts-item.component';

describe('AllGlobalContactsItemComponent', () => {
  let component: AllGlobalContactsItemComponent;
  let fixture: ComponentFixture<AllGlobalContactsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGlobalContactsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGlobalContactsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
