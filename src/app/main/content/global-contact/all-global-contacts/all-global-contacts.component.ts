import { Component, OnInit } from '@angular/core';
import { SectionListComponent } from '../../components/shared/section/section-list/section-list.component';
import { GlobalContactModel } from '../global-contact.model';
import { GlobalContactService } from '../global-contact.service';
import { GlobalContactDataService } from '../global-contact-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { AllGlobalContactsItemComponent } from './all-global-contacts-item/all-global-contacts-item.component';
import { ToasterService } from 'angular5-toaster/dist';
import { Location } from '@angular/common';
import { fuseAnimations } from '@fuse/animations';
import { IActionButtons } from 'types';
import { Section } from 'app/constants/constants';
import { appInjector } from 'app/main/app-injector';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';


@Component({
  selector: 'irms-all-global-contacts',
  templateUrl: './all-global-contacts.component.html',
  styleUrls: ['./all-global-contacts.component.scss'],
  animations: fuseAnimations
})
export class AllGlobalContactsComponent extends SectionListComponent<GlobalContactModel> implements OnInit {
  public listItemComponent: Object = AllGlobalContactsItemComponent;
  listHeaders = ['Contact Name', 'Email', 'Phone', 'List Count', 'Date Added', 'Date Updated'];
  isLocked = true;
  selectedContacts = [];
  public contactsListContact;
  public actionButtons: IActionButtons = {
    leftSide: [
      {
        title: 'Delete Contacts',
        icon: 'delete',
        handler: this.deleteContacts.bind(this),
      },
      {
        title: 'Export Selected Contacts',
        icon: 'launch',
        handler: this.exportSelectedContacts.bind(this),
      }
    ],
  };
  listName = 'List Name';
  contactListPage;
  listId: any;
  isLoading: boolean;
  allCount: number;
  getListSubscription: Subscription;
  constructor(public sectionService: GlobalContactService,
    protected dataService: GlobalContactDataService,
    private dialog: MatDialog,
    protected location: Location,
    router: Router, public route: ActivatedRoute) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  ngOnInit(): void {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.listId = params.lid;
      this.isLoading = true;
      this.contactListPage = {
        listId: params.lid,
        pageSize: 5,
        pageNo: 1
      };

      this.sectionService.setFilter({});
      // super.ngOnInit();
    });
    this.reloadContacts(true);
  }

  private reloadContacts(firstInit = false): void {
    // this.isLocked = true;
    if ( this.getListSubscription ) {
      this.getListSubscription.unsubscribe();
    }
    this.getListSubscription = this.dataService.getAllGlobalContacts(this.contactListPage)
      .subscribe(response => {
        if (firstInit) {
          this.allCount = response.totalCount;
        }
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
        this.isLoading = false;
      });
  }

  public search(text): void {
    if (!text) {
      this.contactListPage.searchText = null;
    } else {
      this.contactListPage.searchText = text;
    }
    this.contactListPage.pageNo = 1;
    this.sectionService.pageIndex = 0;
    this.sectionService.pageSize = this.contactListPage.pageSize;
    this.reloadContacts();
  }

  public changePagination(event): void {
    this.contactListPage.pageNo = event.pageIndex + 1;
    this.contactListPage.pageSize = event.pageSize;
    this.reloadContacts();
  }

  goToImportContacts(): any {
    this.router.navigateByUrl(`/global-contacts/upload-contact-list`, {
      state: {
        data: {
          listId: this.listId
        }
      }
    });
  }
  goToCreateContact(): any {
    this.router.navigateByUrl(`/global-contacts/add-contact`, {
      state: {
        data: {
          listId: this.listId
        }
      }
    });
  }

  exportSelectedContacts(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    dialogConfig.height = '55vh';
    const dialogRef = this.dialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.dataService
            .exportContacts({
              contactIds: this.selectedContacts,
              contactListId: this.listId,
              filename: this.listName,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  exportContacts(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    dialogConfig.height = '55vh';
    const dialogRef = this.dialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.dataService
            .exportContacts({
              contactListId: this.listId,
              filename: this.listName,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  private saveExportedContacts(resp: any): void {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `All Contacts.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  @PopUp('Are you sure want to delete selected contacts from the system?')
  public deleteContacts(): void {
    this.dataService.deleteContacts({
      ids: this.selectedContacts
    })
      .subscribe(result => {
        this.reloadContacts(true);
      });
  }

  @PopUp(`Are you sure want to delete the entire contact list?`)
  public deleteList(): void {
    const toasterService = appInjector().get(ToasterService);


    this.dataService.deleteContactList({
      id: this.listId
    })
      .subscribe(result => {
        if (result) {
          toasterService.pop('success', null, 'Successfully deleted.');
          this.location.back();
        } else {
          toasterService.pop('error', 'This list can not be deleted!', 'This list can’t be deleted because it is in use with a campaign.\n'
            + 'Please disassociate this list from this association before attempting to delete.');
        }
        this.sectionService.doRefreshList();
      });
  }

  public selectionChange(event): void {
    this.selectedContacts = event;
    console.log(this.selectedContacts)
  }

  /// handle select all
  public selectAll(event): void {
    const data = this.toggleSelectAll();
    if (event) {
      this.selectedContacts = this.contactsListContact;
      this.selectionChange(data);
    } else {
      this.selectionChange([]);
    }

  }

}
