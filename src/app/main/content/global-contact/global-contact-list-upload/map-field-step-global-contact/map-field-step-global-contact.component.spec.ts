import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapFieldStepGlobalContactComponent } from './map-field-step-global-contact.componentnt';

describe('MapFieldStepGlobalContactComponent', () => {
  let component: MapFieldStepGlobalContactComponent;
  let fixture: ComponentFixture<MapFieldStepGlobalContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapFieldStepGlobalContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapFieldStepGlobalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
