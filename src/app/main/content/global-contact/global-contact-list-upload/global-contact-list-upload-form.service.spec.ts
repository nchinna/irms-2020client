import { TestBed } from '@angular/core/testing';

import { GlobalContactListUploadFormService } from './global-contact-list-upload-form.service';

describe('GlobalContactListUploadFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalContactListUploadFormService = TestBed.get(GlobalContactListUploadFormService);
    expect(service).toBeTruthy();
  });
});
