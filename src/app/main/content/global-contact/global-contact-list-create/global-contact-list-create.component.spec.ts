import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactCreateComponent } from './global-contact-list-create.component';

describe('GlobalContactCreateComponent', () => {
  let component: GlobalContactCreateComponent;
  let fixture: ComponentFixture<GlobalContactCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
