import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactExportToGuestModalComponent } from './global-contact-export-to-guest-modal.component';

describe('GlobalContactExportToGuestModalComponent', () => {
  let component: GlobalContactExportToGuestModalComponent;
  let fixture: ComponentFixture<GlobalContactExportToGuestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactExportToGuestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactExportToGuestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
