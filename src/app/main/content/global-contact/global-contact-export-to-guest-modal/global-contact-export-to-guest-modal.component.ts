import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalContactDataService } from '../global-contact-data.service';

@Component({
  selector: 'irms-global-contact-export-to-guest-modal',
  templateUrl: './global-contact-export-to-guest-modal.component.html',
  styleUrls: ['./global-contact-export-to-guest-modal.component.scss']
})
export class GlobalContactExportToGuestModalComponent implements OnInit {
  currentEvents = [];
  eventGuestList = [];
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<GlobalContactExportToGuestModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dataService: GlobalContactDataService
  ) { 
    this.form = this.formBuilder.group({
      eventId: ["", [Validators.required]],
      guestListId: ["", [Validators.required]]
    })
  }

  ngOnInit() {
    this.dataService.getEvents().subscribe(data => {
      this.currentEvents = data.items;
    });
    this.form.controls.eventId.valueChanges.subscribe(value => {
      this.dataService.getUnlockedLists(value).subscribe(data =>{
        this.eventGuestList = data;
      })
    });
  }

  close() {
    this.dialogRef.close();
  }
  
  add(){
    if(this.form.valid)
    this.dialogRef.close(this.form.value);
  }
  
}
