import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactAddComponent } from './global-contact-add.component';

describe('GlobalContactAddComponent', () => {
  let component: GlobalContactAddComponent;
  let fixture: ComponentFixture<GlobalContactAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
