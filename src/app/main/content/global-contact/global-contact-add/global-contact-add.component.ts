import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegexpPattern, Section } from 'app/constants/constants';
import { GlobalContactModel } from '../global-contact.model';
import { SectionCreateComponent } from '../../components/shared/section/section-create/section-create.component';
import { GlobalContactService } from '../global-contact.service';
import { GlobalContactDataService } from '../global-contact-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppStateService } from '../../services/app-state.service';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';

@Component({
  selector: 'irms-global-contact-add',
  templateUrl: './global-contact-add.component.html',
  styleUrls: ['./global-contact-add.component.scss']
})
export class GlobalContactAddComponent extends SectionCreateComponent<GlobalContactModel> implements OnInit {


  public addSuccess = false; /// set to false after creation successful, set back to false on reset form
  public loading = false; /// set true while subscribing to form submit 

  /// dynamic lists populated by api
  public contactLists = [];
  public titles = [];
  public nationalities = [];
  public documentTypes = [];
  public issuingCountries = [];
  public expirationDate;
  selectedTabIndex: number;
  listId: any;
  eventId;
  eventGuestId;
  isListPreset: boolean;
  eventList: any;
  state: number;
  eventGuestLists: any[];
  selectListForm: any;
  contactPersonalForm: any;
  contactContactDetailForm: any;
  ///////////////////////////////////

  constructor(private fb: FormBuilder,
    public sectionService: GlobalContactService,
    protected dataService: GlobalContactDataService,
    private appStateService: AppStateService,
    protected router: Router, protected route: ActivatedRoute) {
    super(Section.GlobalContact, sectionService, dataService, router);
    this.route.params.subscribe(() => {
      // check if activated route params haev list id
      if (history.state.data && history.state.data.listId) {
        this.listId = history.state.data.listId;
        this.isListPreset = true;
        this.state = 1;
      } else if (history.state.data && history.state.data.eventId && !history.state.data.eventGuestId) {
        this.eventId = history.state.data.eventId;
        this.isListPreset = true;
        this.state = 2;
      } else if (history.state.data && history.state.data.eventId && history.state.data.eventGuestId) {
        this.eventId = history.state.data.eventId;
        this.eventGuestId = history.state.data.eventGuestId;
        this.isListPreset = true;
        this.state = 3;
      }
    });
  }

  ngOnInit(): void {

    this.selectListForm = this.fb.group({
      list: ['', [Validators.required]],
      listName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
      listId: [''],
      eventId: [''],
      eventGuestId: [''],
    });
    this.contactPersonalForm = this.fb.group({
      title: [''],
      fullName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      preferredName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
      gender: [''],
      nationalityId: [''],
      documentTypeId: [''],
      issuingCountryId: [''],
      documentNumber: [''],
      expirationDateControl: [''],
      expirationDate: [''],
      organization: ['', [Validators.minLength(2), Validators.maxLength(50)]],
      position: ['', [Validators.minLength(2), Validators.maxLength(50)]],
    });
    this.contactContactDetailForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.email))]],
      alternativeEmail: ['', [Validators.pattern(RegExp(RegexpPattern.email))]],
      mobileNumber: ['', [Validators.required]],
      secondaryMobileNumber: [''],
      workNumber: [''],
    });

    // Select list form custom validator
    this.selectListForm.get('list').valueChanges
      .subscribe((value: string) => {
        if (value === 'new') {
          this.setValidators('selectListForm', 'listName', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]);
          this.clearValidators('selectListForm', ['listId']);
        } else if (value === 'existing') {
          this.setValidators('selectListForm', 'listId', [Validators.required]);
          this.clearValidators('selectListForm', ['listName']);
        } else if (value === 'eventGuest') {
          this.setValidators('selectListForm', 'eventId', [Validators.required]);
          this.setValidators('selectListForm', 'eventGuestId', [Validators.required]);
          this.clearValidators('selectListForm', ['listName', 'listId']);
          this.selectListForm.controls['listName'].setValue('');
        } else {
          this.clearValidators('selectListForm', ['listName', 'listId']);
        }
      });

    this.selectListForm.get('eventId').valueChanges.subscribe(val => {
      if (val && val.id) {
        this.dataService.getGuestList({ pageNo: 1, pageSize: 100, eventId: val.id }).subscribe(lists => {
          this.eventGuestLists = lists.items;
          if (this.state === 3) {
            const index = this.eventGuestLists.map(e => e.id).indexOf(this.eventGuestId); // get index of preset list from routing
            const list = { id: this.eventGuestLists[index].id, name: this.eventGuestLists[index].name };
            this.selectListForm.controls['eventGuestId'].setValue(list);
            if (index > -1) {
              this.selectListForm.controls['eventGuestId'].setValue(this.eventGuestLists[index]);
            }
          }
        });
      }
    });

    this.loadLookups();
  }

  /// Method to load all lookup lists form apis
  loadLookups(): void {
    this.dataService.getGlobalContactList().subscribe((result) => {
      this.loading = false;
      this.contactLists = result;
      if (this.state === 1) {
        this.selectListForm.controls.list.setValue('existing');
        const index = this.contactLists.map(e => e.id).indexOf(this.listId); // get index of preset list from routing
        if (index > -1) {
          const list = { id: this.contactLists[index].id, name: this.contactLists[index].name };
          this.selectListForm.controls['listId'].setValue(list);
        }
      }
    }, error => {
      this.loading = false;
    });
    this.dataService.getEvents().subscribe(res => {
      this.eventList = res.items;
      if (this.isListPreset && (this.state === 2 || this.state === 3)) {
        this.selectListForm.controls.list.setValue('eventGuest');
        const index = this.eventList.map(e => e.id).indexOf(this.eventId); // get index of preset list from routing
        const list = { id: this.eventList[index].id, name: this.eventList[index].name };
        this.selectListForm.controls['eventId'].setValue(list);
      }
    });
    this.appStateService.GetNationalities().subscribe((result) => {
      this.loading = false;
      this.nationalities = result;
    }, error => {
      this.loading = false;
    });
    this.appStateService.GetDocumentTypes().subscribe((result) => {
      this.loading = false;
      this.documentTypes = result;
    }, error => {
      this.loading = false;
    });
    this.appStateService.getCountries().subscribe((result) => {
      this.loading = false;
      this.issuingCountries = result;
    }, error => {
      this.loading = false;
    });
  }

  /// Toggle Form Saved Status
  public resetSaved(): void {

    this.addSuccess = false;
    this.ngOnInit();
    this.selectedTabIndex = 0;
    
    
  }

  expirationDateChanged(event): void {
    this.contactPersonalForm.controls['expirationDate'].setValue(event.value.toISOString());
  }

  /// Create Contact Method 
  createContact(): void {
    this.loading = true;
    const data = {
      ...this.contactPersonalForm.value,
      ...this.contactContactDetailForm.value,
      contactListId: this.selectListForm.value.listId ? this.selectListForm.value.listId.id : this.selectListForm.value.eventGuestId.id
    };
    data.contactList = this.selectListForm.controls.listName.value;
    // data.eventGuestList = this.selectListForm.controls.eventGuestId.value.id;
    /// TO DO  [ITP-1882] update create-contact API to handle passed eventGuestList; 
    /// if eventGuestList is not null then create contact and add to event guest list with id = eventGuestList
    this.dataService.addContact(data).subscribe(resp => {
      this.selectedTabIndex = 2;
      this.addSuccess = true;
      const toasterService = appInjector().get(ToasterService);
      if (!resp) {
        toasterService.pop('error', null, 'List name must be unique.');
        this.selectedTabIndex = 0;
        this.addSuccess = false;
        this.router.navigate(['global-contacts']);
      } else {
        toasterService.pop('success', null, 'Successfully created');
      }
      this.loading = false;
    }, error => {
      this.loading = false;
    }
    );
  }

  checkPhone(control): void {
    if (control.valid && control.dirty) {
      this.dataService.checkPhoneUniqueness({ mobileNumber: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.contactContactDetailForm.updateValueAndValidity();
        }
      });
    }
  }

  checkEmail(control): void {
    if (control.valid && control.dirty) {
      this.dataService.checkEmailUniqueness({ email: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.contactContactDetailForm.updateValueAndValidity();
        }
      });
    }
  }

}
