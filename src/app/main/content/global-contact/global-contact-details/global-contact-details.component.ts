import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subscription, Subject } from 'rxjs';
import { GlobalContactService } from '../global-contact.service';
import { GlobalContactDataService } from '../global-contact-data.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EventGuestService } from '../../event/event-layout/event-guest/event-guest.service';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { MatDialog } from '@angular/material';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';

@Component({
  selector: 'irms-global-contact-details',
  templateUrl: './global-contact-details.component.html',
  styleUrls: ['./global-contact-details.component.scss'],
  animations: fuseAnimations
})
export class GlobalContactDetailsComponent implements OnInit {
  contactName = 'Guest Details';
  contact;
  contactId;
  guest;
  public tabGroup = [
    {
      label: 'Personal Information',
      link: './personal-info'
    },
    {
      label: 'Organization Info',
      link: './organization-info'
    },
    {
      label: 'Custom Fields',
      link: './custom-fields'
    },
    {
      label: 'Associated Lists',
      link: './associated-lists'
    }
  ];
  subscription: Subscription;
  private unsubscribeAll: Subject<any>;

  sectionService: any;
  constructor(protected service: GlobalContactService,
    protected headerService: EventGuestService,
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    protected dataService: GlobalContactDataService) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.headerService.getGuestInfo()
      .subscribe(guest => {
        this.guest = guest;
      })
    this.service.getSelectedContactName()
      .subscribe(x => {
        this.contactName = x;


      })
    this.loadBasicInfo();
  }

  private loadBasicInfo(): void {
    this.route.params.subscribe((params: Params) => {
      this.contactId = params['id'];
      this.dataService.getGlobalContactBasicInfo(params['id'], params['lid']).subscribe(data => {
        this.contact = data;
        this.headerService.setGuestInfo(data)
        this.service.setSelectedContactName(data.fullName);
      });
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    this.service.resetFilter();
  }

  @PopUp('Are you sure want to delete this contact from the system?')
  public delete(): void {
    var ids = [];
    ids.push(this.contactId);
    this.dataService.deleteContacts({
      ids: ids
    })
      .subscribe((x) => {
        this.router.navigate(['../../'], { relativeTo: this.route });
        const toasterService = appInjector().get(ToasterService);
        if(x){
          toasterService.pop('success', null, 'Successfully deleted.');
          
        } else {
          toasterService.pop('error', 'This contact can not be deleted!', 'This contact can’t be deleted because it is in use with a campaign.\n'
            + 'Please disassociate this contact from this association before attempting to delete.');
        }
        this.sectionService.doRefreshList();
      });
      
      
  }

}
