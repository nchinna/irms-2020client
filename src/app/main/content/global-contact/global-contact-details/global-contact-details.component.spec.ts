import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactDetailsComponent } from './global-contact-details.component';

describe('GlobalContactDetailsComponent', () => {
  let component: GlobalContactDetailsComponent;
  let fixture: ComponentFixture<GlobalContactDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
