import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListComponent } from './global-contact-list.component';

describe('GlobalContactListComponent', () => {
  let component: GlobalContactListComponent;
  let fixture: ComponentFixture<GlobalContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
