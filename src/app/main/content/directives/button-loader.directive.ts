import { Directive, Input, ComponentFactoryResolver, ViewContainerRef, Renderer2, ElementRef, SimpleChanges, OnInit, OnChanges } from '@angular/core';
import { MatSpinner } from '@angular/material';

@Directive({
  selector: 'button[irmsButtonLoader]'
})
export class ButtonLoaderDirective implements OnInit, OnChanges {
  @Input('irmsButtonLoader') showSpinner: boolean;
  spinner: MatSpinner;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {

    // Set the button to maintain the same dimensions, even with the spinner inside.
    // this.el.nativeElement.style.width = `${(this.el.nativeElement as HTMLElement).offsetWidth}px`;
    this.el.nativeElement.style.height = `${(this.el.nativeElement as HTMLElement).offsetHeight}px`;

    // Create the spinner
    const factory = this.componentFactoryResolver.resolveComponentFactory(MatSpinner);
    const componentRef = this.viewContainerRef.createComponent(factory);
    this.spinner = componentRef.instance;

    // Spinner size
    this.spinner.strokeWidth = 1;
    this.spinner.diameter = 15;

    // Hide the spinner
    this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof(changes.showSpinner) === 'object' && !changes.showSpinner.isFirstChange()) {
      if (changes.showSpinner.currentValue === true) {
        // Add the loader class
        this.renderer.addClass(this.el.nativeElement, 'mat-load-button');

        // Append the spinner
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);

        // Show the spinner
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'block');
      }

      if (changes.showSpinner.currentValue === false) {
        // Hide the spinner
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');

        // Remove the spinner
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);

        // Remove the loader class
        this.renderer.removeClass(this.el.nativeElement, 'mat-load-button');
      }

      this.el.nativeElement.disabled = changes.showSpinner.currentValue;
    }
  }
}
