import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataReviewFormDesignerService {

  private save = new Subject<any>();
  constructor() {
  }

  triggerSave(data: any): void {
    this.save.next(data);
  }

  saveObs(): Observable<any> {
    return this.save.asObservable();
  }
}
