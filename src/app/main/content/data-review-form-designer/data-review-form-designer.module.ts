import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataReviewFormDesignerComponent } from './data-review-form-designer.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { DataReviewFormDesignerNavBarComponent } from './data-review-form-designer-nav-bar/data-review-form-designer-nav-bar.component';
import { MainModule } from 'app/main/main.module';
import { RouterModule } from '@angular/router';
import { DataReviewFormBuilderModule } from '../components/shared/data-review-form-builder/data-review-form-builder.module';


const routes = [
  {
    path: '',
    redirectTo: `:iid`,
    pathMatch: 'full'
  },
  {
    path: ':iid',
    component: DataReviewFormDesignerComponent, 
    canLoad: [AuthGuard], 
    data: { expectedRoles: [RoleType.TenantAdmin] }
  }];

@NgModule({
  declarations: [DataReviewFormDesignerComponent, DataReviewFormDesignerNavBarComponent],
  imports: [
    MainModule,
    CommonModule,
    DataReviewFormBuilderModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DataReviewFormDesignerModule { }
