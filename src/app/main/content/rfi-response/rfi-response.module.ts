import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RfiResponseComponent } from './rfi-response.component';
import { RouterModule } from '@angular/router';
import { RfiResponseService } from './rfi-response.service';
import { MainModule } from 'app/main/main.module';
import { FormModule } from '../components/shared/form/form.module';
import { FormService } from '../components/shared/form/form.service';

const routes = [
  {
    path: '',
    component: RfiResponseComponent
  },
  {
    path: ':tempId',
    component: RfiResponseComponent
  }
];

@NgModule({
  declarations: [
    RfiResponseComponent,
    // RfiPageComponent,
    // RfiPageQuestionsComponent,
    // RfiTextAnswerComponent,
    // RfiIconRatingComponent,
    // RfiDateAnswerComponent,
    // RfiPhoneAnswerComponent,
    // RfiSelectAnswerComponent,
    // RfiSelectSearchAnswerComponent
  ],
  imports: [
    CommonModule,
    MainModule,
    FormModule,
    // TextMaskModule,
    // RatingModule,
    RouterModule.forChild(routes)
  ],
  providers: [RfiResponseService, FormService],
  exports: [FormModule, RouterModule] 
})
export class RfiResponseModule { }
