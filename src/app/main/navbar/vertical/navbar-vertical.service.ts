import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BASE_URL} from '../../../constants/constants';

@Injectable({ providedIn: 'root'})
export class FuseNavbarVerticalService
{
    navBarRef;
  private url  = `${BASE_URL}api/system`;

  constructor(private http: HttpClient ) { }

  getVersion(){
    return this.http.get( `${this.url}/version`, {responseType: 'text'});
  }

    setNavBar(ref)
    {
        this.navBarRef = ref;
    }

    getNavBar()
    {
        return this.navBarRef;
    }
}
