import { NgModule, ModuleWithProviders } from '@angular/core';
import { MainComponent } from './main.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/material.module';
import { RouterModule } from '@angular/router';
import { NavbarHorizontalStyle1Module } from 'app/layout/components/navbar/horizontal/style-1/style-1.module';
import { FuseWidgetModule} from '@fuse/components';
import { ToolbarModule } from 'app/layout/components/toolbar/toolbar.module';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { LayoutModule } from 'app/layout/layout.module';
import { FuseNavbarVerticalComponent } from './navbar/vertical/navbar-vertical.component';
import { GenericListComponent } from './content/components/shared/generic-list/generic-list.component';
import { GenericListContainerComponent } from './content/components/shared/generic-list-container/generic-list-container.component';
import { ListDirectiveDirective } from './content/directives/list-directive.directive';
import { ActionsButtonGroupComponent } from './content/components/shared/actions-button-group/actions-button-group.component';
import { GenericListItemComponent } from './content/components/shared/generic-list/generic-list-item/generic-list-item.component';
import { GenericListHeaderComponent } from './content/components/shared/generic-list/generic-list-header/generic-list-header.component';
import { AutocompleteComponent } from './content/components/shared/autocomplete/autocomplete.component';
import { AutocompleteServerSideComponent } from './content/components/shared/autocomplete-server-side/autocomplete-server-side.component';
import { AutocompleteServerSideMultipleComponent } from './content/components/shared/autocomplete-server-side-multiple/autocomplete-server-side-multiple.component';
import { DynamicHeightDirective } from './content/directives/dynamic-height.directive';
import { GenericLoaderComponent } from './content/components/shared/generic-loader/generic-loader.component';
import { FuseNavigationModule } from 'app/core/components/navigation/navigation.module';
import { PipesModule } from 'app/core/pipes/pipes.module';
import { GenericLoaderService } from './content/components/shared/generic-loader/generic-loader.service';
import { GenericListService } from './content/components/shared/generic-list/generic-list.service';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { EventComponent } from './content/event/event.component';
import { IrmsNgxCroppieComponent } from './content/components/shared/croppie/irms-croppie-btn/irms-ngx-croppie/irms-ngx-croppie.component';
import { CroppieComponent } from './content/components/shared/croppie/irms-croppie-btn/croppie/croppie.component';
import { IrmsCroppieBtnComponent } from './content/components/shared/croppie/irms-croppie-btn/irms-croppie-btn.component';
import { ImageComponent } from './content/components/shared/image/image.component';
import { CurrentUserService } from './content/services/current-user.service';
import { AppStateService } from './content/services/app-state.service';
import { QueryService } from './content/services/query.service';
import { InputErrorComponent } from './content/components/shared/input-error/input-error.component';
import { TextareaErrorComponent } from './content/components/shared/textarea-error/textarea-error.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FeatureListComponent } from './content/components/shared/feature-list-component/feature-list-component.component';
import { FeatureListItemComponent } from './content/components/shared/feature-list-component/feature-list-item/feature-list-item.component';
import { CheckboxFilterComponent } from './content/components/shared/checkbox-filter/checkbox-filter.component';
import { GenericSearchBarComponent } from './content/components/shared/generic-search-bar/generic-search-bar.component';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { PhoneInputComponent } from './content/components/shared/phone-input/phone-input.component';
import { EmptyListMessageComponent } from './content/components/shared/empty-list-message/empty-list-message.component';
import { TextOverflowDirective } from './content/directives/text-overflow.directive';
import { ContactGuestListSearchComponent } from './content/components/shared/contact-guest-list-search/contact-guest-list-search.component';
import { ContactsListItemComponent } from './content/components/shared/contact-guest-list-search/contacts-list-item/contacts-list-item.component';
import { ContactsGroupedListComponent } from './content/components/shared/contacts-grouped-list/contacts-grouped-list.component';
import { GenericContactListItemComponent } from './content/components/shared/generic-contact-list-item/generic-contact-list-item.component';
import { TemplateThumbnailComponent } from './content/components/shared/template-thumbnail/template-thumbnail.component';
import { MessengerPreviewComponent } from './content/components/shared/messenger-preview/messenger-preview.component';
import { GenericContentLoaderComponent } from './content/components/shared/generic-content-loader/generic-content-loader.component';
import { DurationInputComponent } from './content/components/shared/duration-input/duration-input.component';
import { RefreshFormButtonComponent } from './content/components/shared/swipper-form-components/refresh-form-button/refresh-form-button.component';
import { ButtonLoaderDirective } from './content/directives/button-loader.directive';
import { MatSpinner } from '@angular/material';
import { ResponseDrawerComponent } from './content/components/shared/response-drawer/response-drawer.component';
import { FileUploadBlockComponent } from './content/components/shared/file-upload-block/file-upload-block.component';
import { DragDropDirective } from './content/directives/drag-drop.directive';
import { AddCustomFieldModalComponent } from './content/components/shared/add-custom-field-modal/add-custom-field-modal.component';
import { GlobalContactListExportModalComponent } from './global-contact-list-export-modal/global-contact-list-export-modal.component';
import { EventGuestListExportModalComponent } from './content/event/event-layout/event-guest/event-guest-list/event-guest-list-export-modal/event-guest-list-export-modal.component';
import { WhatsappPreviewComponent } from './content/components/shared/whatsapp-preview/whatsapp-preview.component';
import { CredentialConfirmComponent } from './content/components/shared/credential-confirm/credential-confirm.component';
import { RfiMediaGroupComponent } from './content/components/shared/rfi-media-group/rfi-media-group.component';
import { DialogAnimationService } from './content/services/dialog-animation.service';
import { SmsPreviewComponent } from './content/components/shared/media-template-group/sms-preview/sms-preview.component';
import { SmsDesignerSendTestSmsComponent } from './content/sms-designer/sms-designer-send-test-sms/sms-designer-send-test-sms.component';
import { EmailDesignerPreviewComponent } from './content/email-designer/email-designer-preview/email-designer-preview.component';
import { EmailDesignerSendTestEmailComponent } from './content/email-designer/email-designer-send-test-email/email-designer-send-test-email.component';
import { EmailPreviewComponent } from './content/components/shared/media-template-group/email-preview/email-preview.component';
import { WhatsappPreviewModalComponent } from './content/components/shared/media-template-group/whatsapp-preview-modal/whatsapp-preview-modal.component';
import { WhatsappDesignerSendTestMessageComponent } from './content/whatsapp-designer/whatsapp-designer-send-test-message/whatsapp-designer-send-test-message.component';
import { WhatsappTextEditorComponent } from './content/components/shared/whatsapp-text-editor/whatsapp-text-editor.component';
import { WhatsappTemplateCardComponent } from './content/components/shared/media-template-group/media-template-card/whatsapp-template-card/whatsapp-template-card.component';
import { MatSelectSearchExtComponent } from './content/components/shared/mat-select-search-ext/mat-select-search-ext.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
    MainComponent,
    GenericLoaderComponent,
    FuseNavbarVerticalComponent,
    ActionsButtonGroupComponent,
    InputErrorComponent,
    TextareaErrorComponent,
    GenericListItemComponent,
    GenericListHeaderComponent,
    GenericListComponent,
    ListDirectiveDirective,
    GenericListContainerComponent,
    RefreshFormButtonComponent,
    AutocompleteComponent,
    AutocompleteServerSideComponent,
    AutocompleteServerSideMultipleComponent,
    DynamicHeightDirective, 
    DashboardComponent, EventComponent,
    IrmsNgxCroppieComponent, CroppieComponent, IrmsCroppieBtnComponent,
    ImageComponent,
    FeatureListComponent,
    FeatureListItemComponent,
    CheckboxFilterComponent,
    GenericSearchBarComponent,
    PhoneInputComponent,
    EmptyListMessageComponent,
    EventGuestListExportModalComponent,
    TextOverflowDirective,
    ContactGuestListSearchComponent,
    ContactsListItemComponent,
    ContactsGroupedListComponent,
    GenericContactListItemComponent,
    TemplateThumbnailComponent,
    MessengerPreviewComponent,
    GenericContentLoaderComponent,
    DurationInputComponent,
    GlobalContactListExportModalComponent,
    ButtonLoaderDirective,
    ResponseDrawerComponent,
    DragDropDirective,
    FileUploadBlockComponent,
    AddCustomFieldModalComponent,
    GlobalContactListExportModalComponent,
    CredentialConfirmComponent,
    // MediaTemplateGroupComponent,
    GlobalContactListExportModalComponent,
    RfiMediaGroupComponent,
    SmsPreviewComponent,
    SmsDesignerSendTestSmsComponent,
    EmailPreviewComponent,
    EmailDesignerSendTestEmailComponent,
    EmailDesignerPreviewComponent,
    WhatsappPreviewComponent,
    WhatsappPreviewModalComponent,
    WhatsappDesignerSendTestMessageComponent,
    WhatsappTextEditorComponent,
    MatSelectSearchExtComponent,
  ],
imports: [
    FuseSharedModule,
    MaterialModule,
    RouterModule,
    NavbarHorizontalStyle1Module,
    FuseNavigationModule,
    ToolbarModule,
    LayoutModule,
    PipesModule,
    FuseWidgetModule,
    Ng2TelInputModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,ScrollingModule,

],
entryComponents: [CroppieComponent, ContactsListItemComponent, AddCustomFieldModalComponent, GenericContactListItemComponent,
  GlobalContactListExportModalComponent, MatSpinner,
  EventGuestListExportModalComponent,SmsPreviewComponent,EmailPreviewComponent,WhatsappPreviewModalComponent],
providers: [GenericLoaderService, GenericListService, ToasterService],
exports: [
    FuseSharedModule,
    MainComponent,
    ActionsButtonGroupComponent,
    MaterialModule,
    InputErrorComponent,
    TextareaErrorComponent,    
    FeatureListComponent,
    FeatureListItemComponent,
    GenericListItemComponent,
    GenericListHeaderComponent,
    GenericListComponent,
    GenericListContainerComponent,
    PipesModule,
    FuseWidgetModule,
    AutocompleteComponent,
    AutocompleteServerSideComponent,
    AutocompleteServerSideMultipleComponent,
    DynamicHeightDirective,
    DragDropDirective,
    ButtonLoaderDirective,
    IrmsNgxCroppieComponent, CroppieComponent, IrmsCroppieBtnComponent, ImageComponent,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    CheckboxFilterComponent,
    GenericSearchBarComponent,
    Ng2TelInputModule,
    PhoneInputComponent,
    EmptyListMessageComponent,
    TextOverflowDirective,
    ContactGuestListSearchComponent,
    ContactsGroupedListComponent,
    TemplateThumbnailComponent,
    MessengerPreviewComponent,
    GenericContentLoaderComponent,
    DurationInputComponent,
    RefreshFormButtonComponent,
    ResponseDrawerComponent,
    FileUploadBlockComponent,
    RfiMediaGroupComponent,
    SmsPreviewComponent,
    WhatsappPreviewComponent,
    WhatsappTextEditorComponent,
    MatSelectSearchExtComponent,
]
})
export class MainModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MainModule,
      providers: [CurrentUserService, AppStateService, QueryService, DialogAnimationService]
    };
  }
}
