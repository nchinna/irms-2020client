import { environment } from '../../environments/environment';

// SWITHC IT FOR LOCAL DEBUGGING:
export const BASE_URL = environment.apiUrl; // 'https://localhost:5001'; // document.getElementsByTagName('base')[0].href; // 
export const BASE_PAGE_SIZE = 5;
export const BASE_PAGE_SIZE_OPTIONS = [5, 10, 25];
export const DATE_FMT = 'MMM dd, yyyy';
export const DATE_TIME_FMT = `${DATE_FMT} hh:mm:ss a`;
export const MIN_EVENT_DURATION = 1000 * 60 * 60;

export const ForbiddenPhoneCodes = ['+972', '+98', '+850'];

export enum RegexpPattern {
  youTubeLinkValidators = '^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+$',
  password = '^(?=.*[\\d])(?=.*[a-z])(?=.*[A-Z])(?!.*\\<)(?!.*\\>)(?!.*\\s)(?=.*[\'/\\\\\\\\!"#$%&\'()*+,.\\-/:;=?@[\\]^_`{|}~]).*$',
  email = '^[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z-\\.]+$',
  // phone = '^[+\\n0-9\\s\\(\\)-/\\\\\\.]*$',
  phone = '^\\++(965|966|968|973|971)+[0-9]{9}$',
  intlPhone = '^\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{7,15}$',
  link = '^(?:https?:\\/\\/)([\\da-z\\-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$',
  digits = '^[0-9]*$',
  number = '^(\\d+)(?:\\.(\\d+))?$',
  letters = '^[a-zA-Z\u0621-\u064A]*$',
  lettersDigits = '^[0-9a-zA-Z\u0621-\u064A]*$',
  enumeration = '^[a-zA-Z;, \u0621-\u064A]*$',
  tenantUrl = '\\.[a-zA-Z0-9-_]+$',
  fullName = '^[a-zA-Z\u0621-\u064A]+[ a-zA-Z\u0621-\u064A]*[a-zA-Z\u0621-\u064A]+$',
  date = '((^)*([a-zA-Z]{3} [a-zA-z]{3} \\d{2} \\d{4} \\d{2}:\\d{2}:\\d{2} [A-Z]{3}[\\+-]\\d{4})*(\\d{4}-\\d{2}-\\d{2}[a-zA-Z]\\d{2}:\\d{2}:\\d{2}.\\d{3}[a-zA-Z])*)$'
}

// +96647854
export enum RoleType {
  SuperAdmin = 0,
  TenantAdmin = 1
}

export enum MediaType {
  Rfi = 'rfi',
  Email = 0,
  Sms = 1,
  whatsapp = 2
}
export enum timePeriodUnits {
  Days = 0,
  Hours = 1,
  Minutes = 2
}

export enum PeriodDropDownRange {
  Upcoming = 0,
  Current = 1,
  Past = 2,
  All = 3
}

export enum PeriodCheckBoxRange {
  Upcoming = 0,
  Live = 1,
  Past = 2
}

export enum PeriodFilterRange {
  Upcoming = 0,
  Current = 1,
  Past = 2,
  All = 3,
  UpcomingAndCurrent = 4
}

export enum campaignStatus {
  Draft = 0,
  Live = 1,
  Disabled = 2,
  Ended = 3
}

export enum invitationStatus {
  Pending = 0,
  Accepted = 1,
  Rejected = 2
}

export enum invitationWorkflowStep {
  Invitation = 0,
  Pending = 1,
  Accepted = 2,
  Rejected = 3
}

export enum SystemModules {
  Registration = 0,
  Guests = 1,
  Events = 2,
  Content = 3,
  Tasks = 4,
  UserManagement = 5,
  Evaluations = 6,
  Inquires = 7,
}

export enum Section {
  Dashboard = 'dashboard',
  Profile = 'profile',
  GlobalContact = 'global-contacts',
  Events = 'events',
  EventCampaigns = 'campaigns',
  EventGuests = 'event-guests',
  EventDataCampaign = 'event-data-campaign',
  EventDataListAnalysis = 'event-data-list-analysis',
  EventDataRfi = 'event-data-campaign-rfi',
  EventDataRfiResponses = 'event-data-campaign-rfi-responses',
  Settings = 'settings',
  EmailDesigner = 'email-designer',
  SmsDesigner = 'sms-designer',
  FormDesigner = 'form-designer',
  WhatsappDesigner = 'whatsapp-designer',
  Response = 'response',
  RfiResponse = 'rfi',
  GlobalGuest = 'global-guests',
  DataReviewFormDesigner = 'data-review'
}

export enum copyTemplateType {
  SmsToWhatsapp = 0,
  WhatsappToSms = 1,
}

export enum rfiType {
  rsvp = 0,
  pending = 1,
  accepted = 2,
  rejected = 3,
  listAnalysis = 4
}

export enum customFieldType {
  text = 0,
  number = 1,
  date = 2,
  email = 3,
  phone = 4,
}

export enum reservedFields {
  title = 'title',
  fullName = 'fullName',
  preferredName = 'preferredName',
  gender = 'gender',
  nationalityId = 'nationalityId',
  documentTypeId = 'documentTypeId',
  documentNumber = 'documentNumber',
  issuingCountryId = 'issuingCountryId',
  expirationDate = 'expirationDate',
  email = 'email',
  alternativeEmail = 'alternativeEmail',
  mobileNumber = 'mobileNumber',
  secondaryMobileNumber = 'secondaryMobileNumber',
  workNumber = 'workNumber',
  organization = 'organization',
  position = 'position'
}

export const reservedFieldsWithValidation = [
  {
    name: reservedFields.title,
    label: 'Title',
    minLength: 2,
    maxLength: 20,
    type: customFieldType.text
  },
  {
    name: reservedFields.fullName,
    label: 'Full Name',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.preferredName,
    label: 'Preferred Name',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.gender,
    label: 'Gender',
    minLength: 4,
    maxLength: 6,
    type: customFieldType.text
  },
  {
    name: reservedFields.nationalityId,
    label: 'Nationality',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.documentTypeId,
    label: 'Document Type',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.issuingCountryId,
    label: 'Issuing Country',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.documentNumber,
    label: 'Document Number',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.expirationDate,
    label: 'Expiration Date',
    minLength: null,
    maxLength: null,
    type: customFieldType.date
  },
  {
    name: reservedFields.email,
    label: 'Email',
    minLength: 5,
    maxLength: 50,
    type: customFieldType.email
  },
  {
    name: reservedFields.alternativeEmail,
    label: 'Alternative Email',
    minLength: 5,
    maxLength: 50,
    type: customFieldType.email
  },
  {
    name: reservedFields.mobileNumber,
    label: 'Mobile Number',
    minLength: 5,
    maxLength: 20,
    type: customFieldType.phone
  },
  {
    name: reservedFields.secondaryMobileNumber,
    label: 'Secondary Mobile Number',
    minLength: 5,
    maxLength: 20,
    type: customFieldType.phone
  },
  {
    name: reservedFields.workNumber,
    label: 'Work Number',
    minLength: 5,
    maxLength: 20,
    type: customFieldType.phone
  },
  {
    name: reservedFields.organization,
    label: 'Organization',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  },
  {
    name: reservedFields.position,
    label: 'Position',
    minLength: 2,
    maxLength: 50,
    type: customFieldType.text
  }
];
// array of lookups in lowercase
export const lookupReservedFields = ['nationalityid', 'issuingcountryid', 'documenttypeid', 'gender'];

export const transparentSource = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
export const noImagePreview = 'http://unsplash.it/340?random';

export const EditorConfig = {
  toolbarGroups: [
    { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'clipboard', groups: ['clipboard', 'undo'] },
    { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
    { name: 'forms', groups: ['forms'] },
    '/',
    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
    { name: 'links', groups: ['links'] },
    { name: 'insert', groups: ['insert'] },
    '/',
    { name: 'styles', groups: ['styles'] },
    { name: 'colors', groups: ['colors'] },
    { name: 'tools', groups: ['tools'] },
    { name: 'others', groups: ['others'] },
    { name: 'about', groups: ['about'] }
  ],
  removeButtons: `Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,
  Select,Button,ImageButton,HiddenField,CopyFormatting,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Iframe,PageBreak,SpecialChar,Smiley,HorizontalRule,Table,Flash,Save,NewPage,Image,Unlink,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Outdent,Indent,TextColor,BGColor,Maximize,About,ShowBlocks,Subscript,Superscript,RemoveFormat,Replace,Source`
};

/// Tiny MCE configs
export const tinyMceKey = 'xuo1ctz6omrrrvpi51u2ealboku0m36odmdizh0lpk8itptx';

// Sliding dialog config
export const sideDialogConfig = {
  minWidth: '40vw',
  height: '100%',
  position: { right: '0px' },
  animation: { to: 'left' },
};
/// Google Map theme
export const mapsStyle = [
  {
    featureType: 'landscape',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#e0e4ee'
      }
    ]
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#3ccabc'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#353755'
      }
    ]
  }
];
