import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../auth/services/authentication.service';
import {RegexpPattern} from '../../constants/constants';
import { fuseAnimations } from '@fuse/animations';
import { AppStateService } from 'app/main/content/services/app-state.service';

@Component({
    selector     : 'irms-login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    // encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit {

  public loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern(new RegExp(RegexpPattern.email))]],
    password: ['', [Validators.required, Validators.minLength(6),  Validators.pattern(new RegExp(RegexpPattern.password)) ]]
  });

  private returnUrl: string;
  invalidCred: boolean;
  logoLink: any;
  loading = false;
  logoLoad = true;
  constructor(
    public fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private appState: AppStateService,
    private route: ActivatedRoute) {
    authService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }


  ngOnInit() {
    this.logoLoad = true;
    this.appState.getTenantLogo().subscribe(result => {
      this.logoLink = result.logoPath;
      this.logoLoad = false;
    }, error => {
      this.logoLoad = false;
      this.logoLink = null;
    });
  }

  public login(): void {
    if (this.loginForm.valid) {
      this.loading = true;
      const user = this.loginForm.value;
      this.authService.login(user.email, user.password)
        .subscribe(
          data => {
            this.loading = false;
            // by pass two factor authentication
            this.router.navigate([this.returnUrl]);
            // this.router.navigate(['/account/verify-otp']);
          }, error => {
            console.log('error:', error);
            this.invalidCred = true;
            this.loading = false;
          });
    }
  }
}
