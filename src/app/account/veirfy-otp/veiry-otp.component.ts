import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '../../core/animations';
import { AuthenticationService } from '../../auth/services/authentication.service';
import { CurrentUserService } from '../../main/content/services/current-user.service';
import { AccountService } from '../account.service';
import { TokenService } from '../../auth/services/token.service';
import { AppStateService } from 'app/main/content/services/app-state.service';

@Component({
  selector: 'mvms-veiry-otp',
  templateUrl: './veiry-otp.component.html',
  styleUrls: ['./veiry-otp.component.scss'],
  animations: fuseAnimations
})
export class VerifyOtpComponent implements OnInit {
  logoLink: any;
  loading = false;
  logoLoad = true;
  success = false;

  public otpForm = this.fb.group({
    otpCode: ['', [Validators.required]],
  });

  private returnUrl: string;
  user: any;
  phoneNum: any;
  enableResend: boolean;


  constructor(
    public fb: FormBuilder,
    private router: Router,
    private currentUserService: CurrentUserService,
    private accountService: AccountService,
    private tokenService: TokenService,
    private route: ActivatedRoute,
    private  appState: AppStateService) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }

  ngOnInit() {
    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.user = user;
      this.phoneNum = "*".repeat(this.user.phoneNo.length - 3) + '' + this.user.phoneNo.substr(this.user.phoneNo.length - 3);
    });
    this.logoLoad = true;
      this.appState.getTenantLogo().subscribe(result => {
        this.logoLink = result.logoPath;
        this.logoLoad = false;
      }, error => {
        this.logoLoad = false;
        this.logoLink = null;
      });
    // setTimeout(() => {
    //   this.enableResend = true;
    // }, 10000);
    this.startTimer();
  }

  timeLeft: number = 15;
  interval;

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 0;
        this.enableResend = true;
      }
    }, 1000)
  }

  public resendOtp(): void {

    this.accountService.resendOtp().subscribe(
      () => {
      }, () => {
      });
  }

  public veirfyOtp(): void {
    if (this.otpForm.valid) {
      const model = {
        code: this.otpForm.value.otpCode,
      };
      this.accountService.verifyOTP(model).subscribe(
        () => {
          //by pass two factor authentications
          this.tokenService.setOTPStatus(true);
          this.success = true;
        }, () => {
        });
    }
  }

}
