import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { Observable } from 'rxjs/Observable';
import { GenericLoaderService } from 'app/main/content/components/shared/generic-loader/generic-loader.service';
import { tap } from 'rxjs/operators';

export function Toast(value: any) {
  return function (target: any, key: string, descriptor: any) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args: any[]) {
      const loaderService = appInjector().get(GenericLoaderService);
      loaderService.show();
      const result = originalMethod.apply(this, args);
      return (result as Observable<any>).pipe(tap(() => {
        const toasterService = appInjector().get(ToasterService);
        toasterService.pop('success', null, value);
        loaderService.hide();
      }));
    };

    return descriptor;
  };
}