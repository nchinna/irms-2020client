
import {MatDialogRef} from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';


export function PopUp(value: any) {
  return function (target: any, key: string, descriptor: any) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args: any[]) {
      let confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent> = this.dialog.open(FuseConfirmDialogComponent, {
        disableClose: false
      });
      confirmDialogRef.componentInstance.confirmMessage = value;
      confirmDialogRef.afterClosed().subscribe(res => {
        if (res) {
          originalMethod.apply(this, args);
        }
        confirmDialogRef = null;
      });
    };
    return descriptor;
  };
}
