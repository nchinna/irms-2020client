import { NgModule } from '@angular/core';

// import { KeysPipe } from './keys.pipe';
// import { GetByIdPipe } from './getById.pipe';
// import { HtmlToPlaintextPipe } from './htmlToPlaintext.pipe';
// import { FilterPipe } from './filter.pipe';
// import { CamelCaseToDashPipe } from './camelCaseToDash.pipe';
// import { CamelCaseToWordsPipe } from './camelCaseToWords.pipe';
// import { AgeRangePipe } from './ageRange.pipe';
import {DateFormatPipe} from './date.pipe';
import {DateTimeFormatPipe} from './datetime.pipe';
import { EnumKeysPipe } from './enumKeys.pipe';
import { FilterKeywordPipe } from './filter-keyword.pipe';
import { SafehtmlPipe } from './safehtml.pipe';
import { Mins2durationPipe } from './mins2duration.pipe';
import { WhatsappFormatPipe } from './whatsapp-format.pipe';

@NgModule({
    declarations: [
        DateFormatPipe,
        DateTimeFormatPipe,
        EnumKeysPipe,
        FilterKeywordPipe,
        SafehtmlPipe,
        Mins2durationPipe,
        WhatsappFormatPipe,

    ],
    imports     : [],
    exports     : [
        DateFormatPipe,
        DateTimeFormatPipe,
        EnumKeysPipe,
        FilterKeywordPipe,
        SafehtmlPipe,
        Mins2durationPipe,
        WhatsappFormatPipe
    ]
})

export class PipesModule
{

}
