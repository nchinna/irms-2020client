import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mins2duration'
})
export class Mins2durationPipe implements PipeTransform {

  transform(minutes: number): any {
    if (minutes === 0) {
      return '0m';
    }
    else if (minutes < 0){
      return 'invalid';
    }
    else {
      const result: any = {};
      let resString = '';
      const s = {
      w: 10080,
      d: 1440,
      h: 60,
      m: 1
    };

      Object.keys(s).forEach(key => {
      result[key] = Math.floor(minutes / s[key]);
      minutes -= result[key] * s[key];
    });
      Object.keys(result).forEach(key => {
      if (result[key] > 0) {
        resString += result[key] + key + ' ';
      }
    });
      return resString;
    }
    
  }

}
