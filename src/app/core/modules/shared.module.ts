import {ModuleWithProviders, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FuseMatSidenavHelperService } from '@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.service';
import { FusePipesModule } from '@fuse/pipes/pipes.module';
import { FuseMatchMediaService } from '@fuse/services/match-media.service';
import { FuseNavbarVerticalService } from '../../main/navbar/vertical/navbar-vertical.service';
import { FuseConfirmDialogModule } from '@fuse/components';

@NgModule({
  declarations: [
  ],
  exports: [
    FlexLayoutModule,
    MaterialModule,
    CommonModule,
    FormsModule,
    FusePipesModule,
    ReactiveFormsModule,
    FuseConfirmDialogModule
  ],
  entryComponents: [
  ],
  providers: [
    FuseMatchMediaService,
    FuseMatSidenavHelperService,
  ]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [FuseNavbarVerticalService]
    };
  }
}
